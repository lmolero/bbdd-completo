﻿DROP DATABASE IF EXISTS empresa;

CREATE DATABASE IF NOT EXISTS empresa;

USE empresa;

DROP TABLE IF EXISTS empresas;

  CREATE TABLE  IF NOT EXISTS  empresas(
    codigo int AUTO_INCREMENT,
    nombre varchar (15),
    cp int(5),
    trabajadores int,
    poblacion varchar(15),
    PRIMARY KEY (codigo)
    );


DROP TABLE IF EXISTS personas;

  CREATE TABLE  IF NOT EXISTS personas(
    id int AUTO_INCREMENT,
    nombre varchar(15),
    apellidos varchar(15),
    poblacion varchar(15),
    fechapersona date,
    dato1 int,
    dato2 int,
    dato3 int,
    trabajadores int,
    empresa varchar(15),
    fechaempresas date, 
    PRIMARY KEY (id),
    CONSTRAINT FKempresaspersonas FOREIGN KEY (trabajadores)
    REFERENCES empresas(codigo)  ON DELETE CASCADE ON UPDATE CASCADE
    );



