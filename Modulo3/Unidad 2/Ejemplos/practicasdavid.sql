﻿/* (4.07.19) Sacar la facturación que ha tenido la empresa en toda la historia, 
    indicando la base imponible, el IVA y el total facturado.
    NOTA: La base imponible se calcula sumando el coste del producto por el número de unidades vendidas. 
    El IVA es el 21% de la base imponible y, el total, la suma de los dos campos anteriores.*/

SELECT CodigoPedido FROM pedidos WHERE estado<>'rechazado';
SELECT SUM(PrecioUnidad*Cantidad) bi FROM (
    SELECT CodigoPedido FROM pedidos WHERE estado<>'rechazado'  
  ) p JOIN detallepedidos USING(CodigoPedido);

SELECT ROUND(bi,2), ROUND(bi*.21,2),ROUND(bi*1.21,2) FROM (
    SELECT SUM(PrecioUnidad*Cantidad) bi FROM (
        SELECT CodigoPedido FROM pedidos WHERE estado<>'rechazado'  
      ) p JOIN detallepedidos USING(CodigoPedido)
  ) c2;

-- (4.08.03) Obtener los nombres de los clientes cuya línea de crédito sea mayor que los pagos que haya realizado
SELECT p.CodigoCliente,SUM(p.Cantidad) FROM pagos p
GROUP BY p.CodigoCliente;

SELECT * FROM clientes c
  WHERE c.LimiteCredito>(SELECT p.CodigoCliente,SUM(p.Cantidad) n FROM pagos p
GROUP BY p.CodigoCliente);

SELECT c.NombreCliente FROM clientes c
  JOIN (SELECT p.CodigoCliente,SUM(p.Cantidad) n FROM pagos p
GROUP BY p.CodigoCliente)c1 USING(CodigoCliente)
  WHERE c.LimiteCredito>n;

-- (4.10.09) Sacar el nombre, apellidos, oficina (ciudad) y cargo del empleado que no represente a ningún cliente
  SELECT DISTINCT c.CodigoEmpleadoRepVentas
    FROM clientes c;

  SELECT 
         e.Nombre,
         e.Apellido1,
          e.Apellido2,
         o.Ciudad,
         e.Puesto
    FROM empleados e JOIN oficinas o USING(CodigoOficina)
    WHERE e.CodigoEmpleado NOT IN (SELECT DISTINCT c.CodigoEmpleadoRepVentas
    FROM clientes c);

  -- (4.09.02) Sacar el nombre de los clientes que no hayan hecho pagos, junto al nombre de sus representantes (sin apellidos) y la ciudad de la oficina la que pertenece el representante.

 -- clientes que tienen pagos   
SELECT distinct  p.CodigoCliente FROM pagos p;

SELECT 
       c.NombreCliente,
       e.Nombre
       FROM clientes c
  JOIN empleados e ON c.CodigoEmpleadoRepVentas = e.CodigoEmpleado
  WHERE c.CodigoCliente NOT IN(SELECT distinct  p.CodigoCliente FROM pagos p);


SELECT e.CodigoEmpleado,
       e.Nombre,
       o.CodigoOficina,
       o.Ciudad
       FROM empleados e
  JOIN oficinas o USING(CodigoOficina) ;

 SELECT DISTINCT c1.NombreCliente,
        c1.Nombre,
       c2.Ciudad FROM (
        SELECT 
       c.NombreCliente,
       e.Nombre
       FROM clientes c
  JOIN empleados e ON c.CodigoEmpleadoRepVentas = e.CodigoEmpleado
  WHERE c.CodigoCliente NOT IN(SELECT distinct  p.CodigoCliente FROM pagos p))c1 
  JOIN (SELECT e.CodigoEmpleado,
       e.Nombre,
       o.CodigoOficina,
       o.Ciudad
       FROM empleados e
  JOIN oficinas o USING(CodigoOficina) )c2 USING(nombre)
ORDER BY c1.NombreCliente;
  

-- (4.10.18) Sacar los nombres de los clientes que han pedido más de 200 unidades de cualquier producto.

  -- sacando el codigo del cliente que ha comprado cantidad>200

  SELECT  distinct  p.CodigoPedido,
       p.CodigoCliente 
         FROM pedidos p
    JOIN detallepedidos d USING(CodigoPedido)
  WHERE d.Cantidad>200;

  SELECT DISTINCT
         c.NombreCliente
    FROM clientes c
    JOIN ( SELECT  distinct  p.CodigoPedido,
       p.CodigoCliente 
         FROM pedidos p
    JOIN detallepedidos d USING(CodigoPedido)
  WHERE d.Cantidad>200)c1 USING(CodigoCliente);

  -- (4.10.40) Códigos de los frutales no vendidos
SELECT  
       d.CodigoProducto FROM detallepedidos d;

SELECT DISTINCT p.CodigoProducto FROM productos p
  WHERE p.Gama='frutales' 
          AND p.CodigoProducto NOT IN (SELECT d.CodigoProducto FROM detallepedidos d);

-- (4.10.21) Calcula la facturación obtenida con los productos con más margen de beneficio. (Sin excluir los pedidos rechazados)

-- productos con su beneficio
SELECT
  p.CodigoProducto,
  (p.PrecioVenta - p.PrecioProveedor) ganancia
FROM productos p;

SELECT
  MAX(c1.ganancia) maximo
FROM (SELECT
    p.CodigoProducto,
    (p.PrecioVenta - p.PrecioProveedor) ganancia
  FROM productos p)c1;

SELECT
  codigoproducto, Nombre
FROM productos
WHERE  precioventa - precioproveedor = (SELECT
    MAX(beneficio)
  FROM (SELECT
      codigoproducto,
      precioventa - precioproveedor beneficio
    FROM productos)c1);

SELECT 
       
       SUM(d.Cantidad*d.PrecioUnidad)
  FROM detallepedidos d
  JOIN (SELECT
  codigoproducto, Nombre
FROM productos
WHERE  precioventa - precioproveedor = (SELECT
    MAX(beneficio)
  FROM (SELECT
      codigoproducto,
      precioventa - precioproveedor beneficio
    FROM productos)c1))c2 ON c2.CodigoProducto=d.CodigoProducto
GROUP BY c2.CodigoProducto;

-- (4.10.02b) Sacar el listado con los nombres de los clientes, el total pedido y el total pagado por cada uno de ellos.

  -- total pedido
  SELECT d.CodigoPedido ,SUM(d.Cantidad*d.PrecioUnidad) totalpedido FROM detallepedidos d
    GROUP BY d.CodigoPedido;

  -- total pedido por acda cliente
    SELECT
           p.CodigoCliente,
      SUM( c1.totalpedido)totalpe FROM pedidos p
      JOIN ( SELECT d.CodigoPedido ,SUM(d.Cantidad*d.PrecioUnidad) totalpedido FROM detallepedidos d
    GROUP BY d.CodigoPedido)c1 ON c1.CodigoPedido=p.CodigoPedido
    GROUP BY p.CodigoCliente;

-- total pagado por cada cliente
SELECT p.CodigoCliente, SUM(p.Cantidad) FROM pagos p
  GROUP BY p.CodigoCliente;

SELECT 
  c.CodigoCliente,
       c.NombreCliente,
      c2.totalpagado
        FROM clientes c
  JOIN (SELECT p.CodigoCliente, SUM(p.Cantidad) totalpagado FROM pagos p
  GROUP BY p.CodigoCliente)c2 USING(CodigoCliente);

-- uniendo ambas consultas para sacar lo q me piden

  SELECT 
          pagos.NombreCliente,
        pedidos.totalpedido,
         pagos.totalpagado FROM ( SELECT
           p.CodigoCliente,
      SUM( c1.totalpedido)totalpedido FROM pedidos p
      JOIN ( SELECT d.CodigoPedido ,SUM(d.Cantidad*d.PrecioUnidad) totalpedido FROM detallepedidos d
    GROUP BY d.CodigoPedido)c1 ON c1.CodigoPedido=p.CodigoPedido
    GROUP BY p.CodigoCliente) pedidos JOIN (SELECT 
  c.CodigoCliente,
       c.NombreCliente,
      c2.totalpagado
        FROM clientes c
  JOIN (SELECT p.CodigoCliente, SUM(p.Cantidad) totalpagado FROM pagos p
  GROUP BY p.CodigoCliente)c2 USING(CodigoCliente))pagos USING(CodigoCliente);


-- (4.08.09) ¿Qué cliente(s) ha rechazado más pedidos?

  SELECT p.CodigoCliente ,COUNT(*) cuentarechazados FROM pedidos p
  WHERE estado='rechazado'
  GROUP BY p.CodigoCliente;

  SELECT * FROM ( SELECT p.CodigoCliente ,COUNT(*) cuentarechazados FROM pedidos p
  WHERE estado='rechazado'
  GROUP BY p.CodigoCliente)c1 
    HAVING c1.cuentarechazados=2;

  SELECT 
         c.NombreCliente
         FROM clientes c
    JOIN ( SELECT * FROM ( SELECT p.CodigoCliente ,COUNT(*) cuentarechazados FROM pedidos p
  WHERE estado='rechazado'
  GROUP BY p.CodigoCliente)c1 
    HAVING c1.cuentarechazados=2)c2 ON c.CodigoCliente=c2.CodigoCliente;

  -- (4.10.20) Obtener los nombre de clientes que no estén al día con sus pagos. (Sin excluir los pedidos rechazados)

   

  -- cantidad que deben los clientes
    SELECT d.CodigoPedido,
           SUM(d.Cantidad*d.PrecioUnidad) totalapagar
           FROM detallepedidos d
      GROUP BY d.CodigoPedido;

    -- sacando codigo del cliente

    SELECT 
           p.CodigoCliente,
           c1.CodigoPedido,
           c1.totalapagar FROM pedidos p
      JOIN (SELECT d.CodigoPedido,
           SUM(d.Cantidad*d.PrecioUnidad) totalapagar
           FROM detallepedidos d
      GROUP BY d.CodigoPedido)c1 ON c1.CodigoPedido=p.CodigoPedido;

-- sacando total a pagar por cliente
SELECT c2.CodigoCliente ,SUM(c2.totalapagar) deuda FROM ( SELECT 
           p.CodigoCliente,
           c1.CodigoPedido,
           c1.totalapagar FROM pedidos p
      JOIN (SELECT d.CodigoPedido,
           SUM(d.Cantidad*d.PrecioUnidad) totalapagar
           FROM detallepedidos d
      GROUP BY d.CodigoPedido)c1 ON c1.CodigoPedido=p.CodigoPedido)c2
  GROUP BY c2.CodigoCliente;




-- sacando la cantidad pagada por cada cliente
  SELECT p.CodigoCliente,SUM(p.Cantidad) pagorealizado
  FROM pagos p
  GROUP BY p.CodigoCliente;




  SELECT c3.CodigoCliente,
       c3.deuda,
       c4.pagorealizado FROM  (SELECT c2.CodigoCliente ,SUM(c2.totalapagar) deuda FROM ( SELECT 
           p.CodigoCliente,
           c1.CodigoPedido,
           c1.totalapagar FROM pedidos p
      JOIN (SELECT d.CodigoPedido,
           SUM(d.Cantidad*d.PrecioUnidad) totalapagar
           FROM detallepedidos d
      GROUP BY d.CodigoPedido)c1 ON c1.CodigoPedido=p.CodigoPedido)c2
  GROUP BY c2.CodigoCliente)c3 RIGHT JOIN (SELECT p.CodigoCliente,SUM(p.Cantidad) pagorealizado
  FROM pagos p
  GROUP BY p.CodigoCliente)c4 ON c3.CodigoCliente=c4.CodigoCliente
  WHERE c3.deuda> IFNULL(c4.pagorealizado,0);


  SELECT 
         c.NombreCliente
         FROM clientes c
    JOIN ( SELECT c3.CodigoCliente,
       c3.deuda,
       c4.pagorealizado FROM  (SELECT c2.CodigoCliente ,SUM(c2.totalapagar) deuda FROM ( SELECT 
           p.CodigoCliente,
           c1.CodigoPedido,
           c1.totalapagar FROM pedidos p
      JOIN (SELECT d.CodigoPedido,
           SUM(d.Cantidad*d.PrecioUnidad) totalapagar
           FROM detallepedidos d
      GROUP BY d.CodigoPedido)c1 ON c1.CodigoPedido=p.CodigoPedido)c2
  GROUP BY c2.CodigoCliente)c3 RIGHT JOIN (SELECT p.CodigoCliente,SUM(p.Cantidad) pagorealizado
  FROM pagos p
  GROUP BY p.CodigoCliente)c4 ON c3.CodigoCliente=c4.CodigoCliente
  WHERE c3.deuda> IFNULL(c4.pagorealizado,0))c5 ON c.CodigoCliente=c5.CodigoCliente;

-- (03) Obtener el nombre de los países desde donde no se han realizado pedidos.
  SELECT  DISTINCT    c.Pais
         FROM clientes c;


SELECT DISTINCT
       c.Pais,
       p.CodigoCliente FROM clientes c
  JOIN pedidos p USING(CodigoCliente);

SELECT * FROM ( SELECT  DISTINCT    c.Pais
         FROM clientes c)c1
  WHERE pais NOT IN (SELECT DISTINCT
       c.Pais FROM clientes c
  JOIN pedidos p USING(CodigoCliente));

-- (4.08.05) Obtener la cantidad facturada con el producto que más unidades haya vendido

  -- saco productos con sus cantidades vendidas

  SELECT d.CodigoProducto ,SUM(d.Cantidad) sumacantidad FROM detallepedidos d
    GROUP BY d.CodigoProducto;

  -- maxima cantidad vendida y codigo de producto
  SELECT MAX(c1.sumacantidad) maxunidades FROM ( SELECT d.CodigoProducto ,SUM(d.Cantidad) sumacantidad FROM detallepedidos d
    GROUP BY d.CodigoProducto)c1;

  SELECT c2.CodigoProducto FROM ( SELECT d.CodigoProducto ,SUM(d.Cantidad) sumacantidad FROM detallepedidos d
    GROUP BY d.CodigoProducto)c2
    WHERE c2.sumacantidad=(  SELECT MAX(c1.sumacantidad) maxunidades FROM ( SELECT d.CodigoProducto ,SUM(d.Cantidad) sumacantidad FROM detallepedidos d
    GROUP BY d.CodigoProducto)c1);

  SELECT SUM(d.Cantidad*d.PrecioUnidad)total FROM detallepedidos d
    WHERE d.CodigoProducto=( SELECT c2.CodigoProducto FROM ( SELECT d.CodigoProducto ,SUM(d.Cantidad) sumacantidad FROM detallepedidos d
    GROUP BY d.CodigoProducto)c2
    WHERE c2.sumacantidad=(  SELECT MAX(c1.sumacantidad) maxunidades FROM ( SELECT d.CodigoProducto ,SUM(d.Cantidad) sumacantidad FROM detallepedidos d
    GROUP BY d.CodigoProducto)c1));






  













