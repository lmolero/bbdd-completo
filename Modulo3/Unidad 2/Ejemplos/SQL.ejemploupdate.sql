﻿DROP DATABASE IF EXISTS ejemploupdate;

CREATE DATABASE IF NOT EXISTS ejemploupdate;

USE ejemploupdate;

DROP TABLE IF EXISTS productos;

  CREATE TABLE  IF NOT EXISTS  productos(
    idproducto int AUTO_INCREMENT,
    nombre varchar (15),
  precio float,
    cantidad float,
   peso float,
    grupo varchar (15),
    PRIMARY KEY (idproducto)
    );

DROP TABLE IF EXISTS cliente;

  CREATE TABLE  IF NOT EXISTS  cliente(
    idcliente int AUTO_INCREMENT,
    nombre varchar (15),
  apellido varchar (15),
    cantidad float,
    nombrecompleto varchar (15),
  descuento float,
   
    PRIMARY KEY (idcliente)
    );

DROP TABLE IF EXISTS venden;

  CREATE TABLE  IF NOT EXISTS  venden(
     producto int,
    cliente int, 
    cantidad float,
   fecha date,
  total float,
  PRIMARY KEY (producto,cliente),
  CONSTRAINT FKvendeproducto FOREIGN KEY (producto)
   REFERENCES productos(idproducto)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKvendecliente FOREIGN KEY (cliente)
    REFERENCES cliente(idcliente) ON DELETE CASCADE ON UPDATE CASCADE

    );


