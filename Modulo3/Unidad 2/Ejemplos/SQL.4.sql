﻿/* Consulta de combinacion interna 4.1*/
  SELECT DISTINCT nombre, edad
    FROM ciclista 
    INNER JOIN etapa ON ciclista.dorsal= etapa.dorsal;

-- c1 ciclistas que gan ganado etapas

  SELECT DISTINCT dorsal FROM etapa ;

  -- consulta completa con join
  SELECT nombre,edad FROM (SELECT DISTINCT dorsal FROM etapa ) c1
    JOIN ciclista USING (dorsal);

--  consuta completa con in
   
  SELECT c.nombre,c.edad 
  FROM ciclista c 
  
  WHERE c.dorsal IN ( SELECT DISTINCT dorsal FROM etapa);

  /* Consulta de combinacion interna 4.2*/
  SELECT DISTINCT nombre, edad
    FROM ciclista 
    INNER JOIN puerto ON ciclista.dorsal= puerto.dorsal;

  /* Consulta de combinacion interna 4.3*/
  SELECT DISTINCT nombre, edad
   FROM ciclista 
  JOIN etapa ON  ciclista.dorsal= etapa.dorsal 
  JOIN puerto ON ciclista.dorsal= puerto.dorsal;


/* Consulta de combinacion interna 4.4*/
  SELECT DISTINCT director
         FROM ciclista
    JOIN equipo ON ciclista.nomequipo=equipo.nomequipo
    JOIN etapa ON ciclista.dorsal=etapa.dorsal;

/* Consulta de combinacion interna 4.5*/
      SELECT DISTINCT ciclista.dorsal,
             nombre
        FROM ciclista
       INNER JOIN lleva ON lleva.dorsal=ciclista.dorsal;

/* Consulta de combinacion interna 4.6*/
SELECT DISTINCT ciclista.dorsal,
             nombre
 FROM ciclista
  JOIN lleva ON lleva.dorsal=ciclista.dorsal
  JOIN maillot ON lleva.código= maillot.código 
  WHERE color="amarillo";

/* Consulta de combinacion interna 4.7*/
SELECT DISTINCT lleva.dorsal
 FROM lleva
  JOIN  etapa ON lleva.dorsal = etapa.dorsal;

/* Consulta de combinacion interna 4.8*/
SELECT DISTINCT etapa.numetapa
  FROM etapa
  JOIN puerto ON etapa.numetapa = puerto.numetapa;

/* este es el resultado facil*/

SELECT DISTINCT numetapa FROM puerto p;


/* Consulta de combinacion interna 4.9*/
SELECT DISTINCT kms
       FROM ciclista
  JOIN etapa ON ciclista.dorsal = etapa.dorsal
  JOIN puerto ON ciclista.dorsal = puerto.dorsal
  WHERE nomequipo='banesto' ;

/* Consulta de combinacion interna 4.10*/

SELECT DISTINCT COUNT(*)
       FROM ciclista
  JOIN etapa ON ciclista.dorsal = etapa.dorsal
  JOIN puerto ON ciclista.dorsal = puerto.dorsal;

-- consulta corregida

SELECT COUNT(DISTINCT etapa.dorsal) FROM puerto JOIN etapa USING (numetapa);

/* Consulta de combinacion interna 4.11*/
  SELECT nompuerto
    FROM  puerto
    JOIN ciclista ON puerto.dorsal = ciclista.dorsal
    WHERE nomequipo='banesto';


  /* Consulta de combinacion interna 4.12*/
SELECT COUNT(DISTINCT e.numetapa)
       FROM ciclista
  JOIN etapa ON ciclista.dorsal = etapa.dorsal
  JOIN puerto ON ciclista.dorsal = puerto.dorsal
  WHERE nomequipo='banesto' AND kms>200;

-- correcion de ejemplo 12

-- c1 
  CREATE OR REPLACE VIEW c1 AS 

SELECT dorsal FROM ciclista WHERE nomequipo='banesto';
 -- c2

SELECT COUNT(DISTINCT e.numetapa) FROM c1 
  JOIN etapa e USING(dorsal)
  JOIN puerto USING(numetapa)
WHERE e.kms>20;


