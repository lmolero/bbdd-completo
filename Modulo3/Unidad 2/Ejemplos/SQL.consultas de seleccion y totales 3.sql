﻿/* consultas de seleccion y ttales 3.1*/
  SELECT DISTINCT edad
    FROM ciclista
    WHERE nomequipo= 'Banesto'; 

/* consultas de seleccion y ttales 3.2*/
  SELECT DISTINCT edad
    FROM ciclista
    WHERE nomequipo= 'Banesto' OR 'Navigare';

/* consultas de seleccion y ttales 3.3*/
  SELECT dorsal 
    FROM ciclista
    WHERE nomequipo='banesto' AND edad BETWEEN 25 AND 32;

  /* consultas de seleccion y ttales 3.4*/
  SELECT dorsal 
    FROM ciclista
    WHERE nomequipo='banesto' OR edad BETWEEN 25 AND 32;

  /* consultas de seleccion y ttales 3.5*/
  SELECT DISTINCT LEFT(nomequipo,1) inicial
  FROM ciclista 
  where nombre like 'r%' limit 0,30 ;

  /* consultas de seleccion y ttales 3.6*/
SELECT numetapa
  FROM etapa 
  WHERE salida=llegada; 

  /* consultas de seleccion y ttales 3.7*/
  SELECT numetapa,dorsal
  FROM etapa 
  WHERE salida<>llegada AND dorsal IS NOT NULL;

 /* consultas de seleccion y ttales 3.8*/
SELECT nompuerto 
  FROM puerto
  WHERE altura BETWEEN 1000 AND 2000 OR altura>2400;

 /* consultas de seleccion y ttales 3.9*/
SELECT dorsal
  FROM puerto
  WHERE altura BETWEEN 1000 AND 2000 OR altura>2400;

 /* consultas de seleccion y ttales 3.10*/
  SELECT  COUNT(DISTINCT dorsal)
    FROM etapa ;

/* consultas de seleccion y ttales 3.11*/
SELECT COUNT(DISTINCT numetapa)
  FROM puerto;

/* consultas de seleccion y ttales 3.12*/
SELECT COUNT( DISTINCT dorsal)
  FROM puerto;

/* consultas de seleccion y ttales 3.13*/
  SELECT numetapa, COUNT(nompuerto) 
    FROM puerto
    GROUP BY numetapa;  

/* consultas de seleccion y ttales 3.14*/
  SELECT AVG(altura)
    FROM puerto;

  /* consultas de seleccion y ttales 3.15*/
 SELECT  numetapa 
  FROM puerto
  GROUP BY numetapa
  HAVING AVG(altura)>1500;
  

  /* consultas de seleccion y ttales 3.16*/
    SELECT COUNT(numetapa)
      FROM  (SELECT  numetapa 
  FROM puerto
  GROUP BY numetapa
  HAVING AVG(altura)>1500 ) c1;


  /* consultas de seleccion y ttales 3.17*/
    SELECT   dorsal, COUNT(*)
      FROM  lleva 
     GROUP BY dorsal ;


   /* consultas de seleccion y ttales 3.18 sin subconsultas*/
  SELECT  COUNT(*), dorsal, código
         FROM lleva
  GROUP BY dorsal,código;

/* consultas de seleccion y ttales 3.19*/
SELECT DISTINCT  COUNT(numetapa),
                dorsal,
                numetapa
  FROM lleva
  GROUP BY dorsal, numetapa;

/* consulta 3.19, sacando nombre de cicista*/

SELECT ciclista.dorsal,numetapa, COUNT(*) AS numero
  FROM lleva 
  JOIN ciclista
  ON lleva.dorsal=ciclista.dorsal
  GROUP BY  ciclista.dorsal,numetapa;





      

   

