﻿DROP TABLE IF EXISTS importes;
CREATE TABLE IF NOT EXISTS importes (
  tipo varchar(15),
  valor int,
  PRIMARY KEY (tipo)
);

INSERT INTO importes
  VALUES ('jornalero', 25),
  ('efectivo', 250);

-- 3

ALTER TABLE empleados ADD COLUMN IF NOT EXISTS salarioBasico float;

UPDATE empleados e
JOIN importes i
  ON e.Tipo_trabajo = i.tipo
SET e.salarioBasico = IF(e.Tipo_trabajo = 'jornalero', e.Horas_trabajadas, e.Dias_trabajdos) * valor;

-- 4

ALTER TABLE empleados ADD COLUMN IF NOT EXISTS premios float;

UPDATE empleados e
SET premios = e.salarioBasico * (
IF(
  (e.Horas_trabajadas >= 200 OR e.Dias_trabajdos >= 20,
IF ((rubro = 'chofer' OR e.Rubro = 'azafatas'), 0.15, 0.05),0);


-- 5

ALTER TABLE empleados ADD COLUMN IF NOT EXISTS sueldoNominal float;

UPDATE empleados e
SET sueldoNominal = (e.salarioBasico + e.premios);

-- 6

ALTER TABLE empleados ADD COLUMN IF NOT EXISTS BPS float;
ALTER TABLE empleados ADD COLUMN IF NOT EXISTS IRPF float;

UPDATE empleados e
SET e.BPS = (e.sueldoNominal * 0.13);

UPDATE empleados e
  SET e.IRPF=e.sueldoNominal* 
    IF
      (e.sueldoNominal<=(4*1160),0.03,
    IF(e.sueldoNominal>=(10*1160),0.09
        ,0.06));

-- 7

ALTER TABLE empleados ADD COLUMN IF NOT EXISTS subtotaldescuentos float;

UPDATE empleados e
  SET subtotaldescuentos=(e.IRP+e.BPS);

-- 8

ALTER TABLE empleados ADD COLUMN IF NOT EXISTS sueldoliquido float;

UPDATE empleados e
  SET sueldoliquido=( e.sueldoNominal-e.subtotaldescuentos);

-- 9 
  
  ALTER TABLE empleados ADD COLUMN IF NOT EXISTS v_transporte float;
 UPDATE empleados e
  SET e.v_transporte=
  CASE 
  WHEN (DATEDIFF(NOW(),e.`Fecha-nac`)/365>40) AND (e.Barrio='cordon' OR e.Barrio='centro') THEN 200
  WHEN (DATEDIFF(NOW(),e.`Fecha-nac`)/365>40) AND NOT(e.Barrio='cordon' OR e.Barrio='centro') THEN 150
  WHEN (DATEDIFF(NOW(),e.`Fecha-nac`)/365<40) THEN 100

  -- el ltimo when lo puedo sustituir por un else 100 y es lo mismo
END;

-- 10

ALTER TABLE empleados ADD COLUMN IF NOT EXISTS valimentacion float;

UPDATE empleados e
  SET valimentacion=
  CASE 
  WHEN (e.sueldoliquido<5000) THEN 300
  WHEN (e.sueldoliquido BETWEEN 5000 AND 10000) AND (e.Tipo_trabajo='jornalero') THEN 200
  WHEN (e.sueldoliquido BETWEEN 5000 AND 10000) AND (e.Tipo_trabajo='efectivo') THEN 100
  WHEN (e.sueldoliquido>10000) THEN 0
  END;

