﻿SELECT  v.cantidad*p.precio
        FROM venden v
  JOIN productos p ON v.producto = p.idproducto;

CREATE OR REPLACE VIEW c1 AS 
SELECT  p.idproducto,(v.cantidad*p.precio) total 
        FROM venden v
  JOIN productos p ON v.producto = p.idproducto;

-- consulta de actualizacion
 UPDATE venden v JOIN c1 c 
  ON v.producto=c.idproducto
  SET v.total=c.total;

-- 4-b
SELECT CONCAT(c.nombre,' ',c.apellido) nombrecompleto FROM cliente c;

CREATE OR REPLACE VIEW c2 AS 
  SELECT c.idcliente,
    CONCAT(c.nombre,' ',c.apellido) nombrecompleto 
FROM cliente c;

-- consulta de actualizacion
  UPDATE cliente c JOIN c2 c2
    ON c.idcliente=c2.idcliente
    SET c.nombrecompleto=c2.nombrecompleto;

-- 5-a vende total
  SELECT p.idproducto,(p.precio*v.cantidad)*(1-c.descuento/100) vendentotal FROM venden v
  JOIN productos p ON v.producto = p.idproducto
  JOIN cliente c ON v.cliente = c.idcliente;

CREATE OR REPLACE VIEW c3 AS 
SELECT p.idproducto,(p.precio*v.cantidad)*(1-c.descuento/100) vendentotal FROM venden v
  JOIN productos p ON v.producto = p.idproducto
  JOIN cliente c ON v.cliente = c.idcliente;

UPDATE venden v JOIN c3 c3
  ON v.producto=c3.idproducto
  SET v.total=c3.vendentotal;

-- 5-b productos cantidad
SELECT v.producto,sum(DISTINCT total) cantidadtotal
 FROM venden v
GROUP BY v.producto;


CREATE OR REPLACE VIEW c4 AS
SELECT v.producto,sum(DISTINCT total) cantidadtotal
 FROM venden v
GROUP BY v.producto;


UPDATE venden v JOIN c4 c4
  ON v.producto=c4.producto
  SET v.cantidad=c4.cantidadtotal;

-- 5-c
  SELECT v.cliente,SUM(v.cantidad)sumcomprados FROM venden v
  GROUP BY v.cliente;

  CREATE OR REPLACE VIEW c5 AS
  SELECT v.cliente,SUM(v.cantidad)sumcomprados FROM venden v
  GROUP BY v.cliente;

  UPDATE cliente c JOIN c5 c5 
    ON c.idcliente=c5.cliente
    SET c.cantidad=c5.sumcomprados;

  -- creando nuevos campos en diferentes tablas de la base de datos
  ALTER TABLE productos ADD COLUMN IF NOT EXISTS preciototal float;
  ALTER TABLE cliente ADD COLUMN IF NOT EXISTS preciototal float;

  -- 6-a
    SELECT p.idproducto,p.nombre,SUM(p.precio) preciototal
      FROM  productos p
GROUP BY p.nombre;




    CREATE OR REPLACE VIEW c6 AS 
  SELECT p.idproducto,p.nombre,SUM(p.precio) preciototal
      FROM  productos p
GROUP BY p.nombre;

    UPDATE productos p JOIN c6 c6
      ON p.idproducto=c6.idproducto
      SET p.preciototal=c6.preciototal;

    -- 6-b





  
