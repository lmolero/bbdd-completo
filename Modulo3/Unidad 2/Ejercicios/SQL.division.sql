﻿-- dorsal de todos los ciclistas que han llevado todos los maillots

  SELECT DISTINCT l.código FROM lleva l;

  SELECT DISTINCT l.código,l.dorsal FROM lleva l;

   CREATE OR REPLACE VIEW c1 AS
      SELECT DISTINCT l.código FROM lleva l;

  CREATE OR REPLACE VIEW c2 AS
 SELECT DISTINCT l.código,l.dorsal FROM lleva l;

  SELECT c.dorsal, COUNT(*) t FROM c2 c
    GROUP BY c.dorsal 
    HAVING t=(SELECT COUNT(*) FROM c1 c1);
