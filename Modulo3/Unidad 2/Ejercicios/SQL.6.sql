﻿/*consulta de seleccion 6.1*/
 SELECT COUNT(*)
  FROM vendedores v
   WHERE MONTH(v.FechaAlta)= '02';

/*consulta de seleccion 6.2*/
  SELECT COUNT(*), v.`Guap@` 
    FROM vendedores v
   GROUP BY v.`Guap@`;

/*consulta de seleccion 6.3*/
SELECT MAX(p.Precio),p.NomProducto 
FROM productos p;

/*consulta de seleccion 6.4*/
  SELECT AVG(p.Precio), p.IdGrupo FROM productos p
    GROUP BY p.IdGrupo;

/*consulta de seleccion 6.5*/
SELECT DISTINCT 
       p.IdGrupo,
    g.NombreGrupo
  FROM productos p
    JOIN grupos g ON p.IdGrupo = g.IdGrupo
  JOIN ventas v ON p.IdProducto = v.`Cod Producto`
  GROUP BY p.IdGrupo;


/*consulta de seleccion 6.6*/
SELECT p.IdGrupo, g.NombreGrupo     
  FROM productos p
  JOIN grupos g ON p.IdGrupo = g.IdGrupo
  LEFT JOIN ventas v ON p.IdProducto = v.`Cod Producto`
  WHERE v.`Cod Producto`  IS NULL;

/*consulta de seleccion 6.7*/
SELECT COUNT(DISTINCT v.Poblacion) total
  FROM vendedores v
WHERE v.`Guap@`= 1; 

/*consulta de seleccion 6.8*/
  SELECT  MAX(v.Poblacion)
    FROM vendedores v
    WHERE v.EstalCivil='casado';

  /*consulta de seleccion 6.9*/

CREATE OR REPLACE VIEW c1 AS 
SELECT *
  FROM vendedores v
  WHERE v.EstalCivil='casado';

SELECT DISTINCT
       v.Poblacion 
  FROM 
    vendedores v
  LEFT JOIN c1 c USING(EstalCivil)
WHERE v.EstalCivil<> 'casado';

/*consulta de seleccion 6.10*/

SELECT * FROM  vendedores ve 
LEFT JOIN ventas v ON ve.IdVendedor= v.`Cod Vendedor`
WHERE v.`Cod Vendedor` IS null;

/*consulta de seleccion 6.11*/
    SELECT `Cod Vendedor`,MAX(DISTINCT Kilos) FROM ventas;

    CREATE OR REPLACE VIEW c2 AS
       SELECT `Cod Vendedor`,MAX(DISTINCT Kilos) FROM ventas;

 SELECT v.NombreVendedor FROM vendedores v
  JOIN c2 c ON v.IdVendedor=c.Cod Vendedor;


    SELECT
           v.NombreVendedor 
      FROM vendedores v
      JOIN 
    ( SELECT `Cod Vendedor`,MAX(DISTINCT Kilos) FROM ventas) ventas ON v.IdVendedor = ventas.`Cod Vendedor`;
      

      







  

   
  
