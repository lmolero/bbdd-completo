﻿/* Consultas de combinacion externa 5.1*/
  SELECT DISTINCT nombre,
         edad    
    FROM ciclista
    LEFT OUTER JOIN etapa ON etapa.dorsal=ciclista.dorsal
  WHERE etapa.dorsal IS NULL;

  -- optimizada
 CREATE OR REPLACE VIEW e1c1 AS 
      SELECT DISTINCT dorsal FROM etapa;

 CREATE OR REPLACE VIEW e1c2 AS 
      SELECT DISTINCT dorsal FROM ciclista c;

-- ciclistas que NO han ganado etapas con left join

SELECT * FROM e1c2 e LEFT JOIN e1c1 e1 USING (dorsal)
  WHERE e1.dorsal IS NULL ; 

-- ciclistas que NO han ganado etapas con WHERE 
SELECT c.nombre,c.edad FROM ciclista c WHERE dorsal NOT IN (SELECT dorsal FROM e1c1 e1);

/* Consultas de combinacion externa 5.2*/
 SELECT DISTINCT nombre,
         edad    
    FROM ciclista
    LEFT OUTER JOIN puerto ON puerto.dorsal=ciclista.dorsal
  WHERE puerto.dorsal IS NULL;

/* Consultas de combinacion externa 5.3*/
SELECT DISTINCT director
         FROM ciclista
   LEFT JOIN equipo ON ciclista.nomequipo=equipo.nomequipo
   LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal
WHERE etapa.dorsal IS NULL;

/* Consultas de combinacion externa 5.4*/
  SELECT ciclista.dorsal,
         nombre FROM ciclista
    LEFT JOIN lleva ON  ciclista.dorsal = lleva.dorsal
    WHERE lleva.dorsal IS NULL;

/* Consultas de combinacion externa 5.5*/
SELECT *
  FROM lleva 
  LEFT JOIN maillot ON  lleva.código = maillot.código
WHERE color= 'amarillo' ;

 SELECT  ciclista.dorsal,
         nombre
 FROM ciclista
 LEFT JOIN (SELECT lleva.*
  FROM lleva 
  LEFT JOIN maillot ON  lleva.código = maillot.código
WHERE color= 'amarillo') AS c1
  ON c1.dorsal=ciclista.dorsal
WHERE c1.dorsal IS NULL ;

-- c1
SELECT DISTINCT l.dorsal FROM lleva l;

 CREATE OR REPLACE VIEW consulta5c1 AS 
  SELECT DISTINCT l.dorsal FROM lleva l;

-- c2 ciclistas que no tienen maillot 
SELECT * FROM ciclista LEFT JOIN consulta5c1 c1 USING(dorsal)
  WHERE c1.dorsal IS NULL;

SELECT * FROM ciclista
  WHERE dorsal NOT IN (SELECT * FROM consulta5c1);

CREATE OR REPLACE VIEW consulta5c2 AS 
  SELECT dorsal FROM ciclista LEFT JOIN consulta5c1 c1 USING(dorsal)
  WHERE c1.dorsal IS NULL;

-- c3 ciclistas que han ganado etapas 
  SELECT DISTINCT dorsal FROM etapa;

CREATE OR REPLACE VIEW consulta5c3 AS 
 SELECT DISTINCT dorsal FROM etapa;

-- c4 ciclistas que han ganado puerto
  SELECT DISTINCT dorsal FROM puerto;
   
CREATE OR REPLACE VIEW consulta5c4 AS
  SELECT DISTINCT dorsal FROM puerto;

-- c3-c1: ciclistas que han ganado etapa pero no han llevado maillot
  SELECT * FROM consulta5c3 c3 LEFT JOIN consulta5c1 c1 USING(dorsal)
    WHERE c1.dorsal IS NULL;

  SELECT * FROM consulta5c3 c3
    WHERE c3.dorsal NOT IN (SELECT * FROM consulta5c1);

  -- c3 interseccion con c2: hecho de otra forma c3-c1
    SELECT * FROM consulta5c3 NATURAL JOIN consulta5c2; 

 -- c3-c2: ciclistas que han ganado etapa y además han llevado maillot
  SELECT c3.dorsal FROM consulta5c3 c3 LEFT JOIN consulta5c2 c2 USING(dorsal)
    WHERE c2.dorsal IS NULL;

  SELECT * FROM consulta5c3 c3
  WHERE c3.dorsal NOT IN (SELECT * FROM consulta5c2);

-- c3 interseccion con c1 
  SELECT * FROM  consulta5c3 NATURAL JOIN consulta5c1 c1;

  -- c4-c3-c2: ciclistas que han ganado puertos y que no han ganado etapas y llevan dorsal
    SELECT c4.dorsal FROM consulta5c4 c4 
      LEFT JOIN consulta5c3 c3 USING(dorsal)
      LEFT JOIN consulta5c2 c2  USING(dorsal)
      WHERE 
        c2.dorsal IS NULL AND c3.dorsal IS NULL;

-- c1 union c3: ciclistas que han llevado maillot mas los que  han ganado alguna etapa

  SELECT * FROM  consulta5c1 c1 UNION SELECT * FROM  consulta5c3 c3;

  -- c1 union c3 union c4

SELECT * FROM  consulta5c1 c1 
UNION 
SELECT * FROM  consulta5c3 c3 
UNION  
SELECT * FROM consulta5c4;

/* Consultas de combinacion externa 5.6*/
SELECT DISTINCT etapa.numetapa FROM etapa
  LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa 
  WHERE puerto.numetapa IS NULL;

-- c1 etapas que tienen puerto
  SELECT DISTINCT p.numetapa FROM puerto p;

  CREATE OR REPLACE VIEW e6c1 AS 
SELECT DISTINCT p.numetapa FROM puerto p;

  -- consulta completa son c2
    SELECT e.numetapa
      FROM etapa e 
      LEFT JOIN e6c1 e1 USING(numetapa)
      WHERE e1.numetapa IS NULL;


/* Consultas de combinacion externa 5.7*/
SELECT AVG(kms)
        FROM etapa
  LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa 
  WHERE puerto.numetapa IS NULL;

/* Consultas de combinacion externa 5.8*/
  SELECT COUNT(*) FROM ciclista 
    LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal
    WHERE etapa.dorsal IS NULL;

  /* Consultas de combinacion externa 5.9*/
-- c1
SELECT DISTINCT etapa.dorsal,etapa.numetapa
   FROM etapa
  LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa 
  WHERE puerto.numetapa IS NULL;

CREATE OR REPLACE VIEW consulta9c1 AS
SELECT DISTINCT etapa.dorsal,etapa.numetapa
   FROM etapa
  LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa 
  WHERE puerto.numetapa IS NULL;

-- total 

SELECT DISTINCT c.dorsal FROM ciclista c
  JOIN (SELECT DISTINCT etapa.*
   FROM etapa
  LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa 
  WHERE puerto.numetapa IS NULL) c1
WHERE c.dorsal=c1.dorsal;


CREATE OR REPLACE VIEW consulta9total  AS
SELECT DISTINCT c.dorsal FROM ciclista c
  JOIN consulta9c1 c1 ON c.dorsal=c1.dorsal;

SELECT * FROM consulta9total c;

  /* Consultas de combinacion externa 5.10*/
    SELECT DISTINCT c.dorsal FROM ciclista c
  JOIN (SELECT DISTINCT etapa.dorsal,etapa.numetapa
   FROM etapa
  LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa 
  WHERE puerto.numetapa IS NULL) c1  ON  c.dorsal=c1.dorsal;

-- ciclistas que han ganado etapas y no han ganado puertos
  

 SELECT * FROM e1c1 e LEFT JOIN e1c2 e1 USING (dorsal)
  WHERE e2c1.dorsal IS NULL;






  

