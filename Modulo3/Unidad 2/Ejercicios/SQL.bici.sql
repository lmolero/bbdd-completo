﻿-- a
  SELECT a.bici,SUM(DISTINCT a.kms) suma FROM alquileres a
    GROUP BY a.bici;


  UPDATE bicis b
    JOIN (SELECT a.bici,SUM(DISTINCT a.kms) suma FROM alquileres a
    GROUP BY a.bici)c1  ON b.bicis_id=c1.bici
   SET b.kms=c1.suma;

  -- igualar datos a 0 cuando sea null

  UPDATE bicis 
    SET kms=0
    WHERE kms IS NULL;

-- b
   
  SELECT b.bicis_id,TIMESTAMPDIFF(year, b.fechaCompra,NOW()) FROM bicis b;

  UPDATE bicis b
    JOIN
    (SELECT b.bicis_id,TIMESTAMPDIFF(year, b.fechaCompra,NOW()) años FROM bicis b) c1 
  ON b.bicis_id=c1.bicis_id
    SET b.años=c1.años;

  -- consulta de ramon

    UPDATE bicis b
      SET b.años=TIMESTAMPDIFF(year, b.fechaCompra,NOW();

 -- c
  ALTER TABLE bicis ADD COLUMN IF NOT EXISTS numaverias float;
  SELECT a.bici,COUNT(*) s FROM averias a
  GROUP BY a.bici;

  UPDATE bicis b
    JOIN (SELECT a.bici,COUNT(*) s FROM averias a
  GROUP BY a.bici) c1 ON b.bicis_id=c1.bici
    SET b.numaverias= c1.s;

  -- d 
    
  ALTER TABLE bicis ADD COLUMN IF NOT EXISTS alquileres float;

  SELECT a.bici,SUM(a.bici) nalquiler FROM alquileres a
  GROUP BY a.bici;

  UPDATE bicis b
    JOIN (SELECT a.bici,SUM(a.bici) nalquiler FROM alquileres a
  GROUP BY a.bici) c1 ON b.bicis_id=c1.bici
  SET b.alquileres=c1.nalquiler;

  -- e
    ALTER TABLE bicis ADD COLUMN IF NOT EXISTS gastoTotal float;

    SELECT a.bici,(a.coste *b.numaverias) gasto FROM averias a
      JOIN bicis b ON a.bici = b.bicis_id
    GROUP BY a.bici;

    UPDATE bicis b
      JOIN (SELECT a.bici,(a.coste *b.numaverias) gasto FROM averias a
      JOIN bicis b ON a.bici = b.bicis_id
    GROUP BY a.bici)  c ON b.bicis_id=c.bici
      SET b.gastoTotal=c.gasto;

    -- f
       SELECT b.bicis_id,(b.precio*b.alquileres) total FROM bicis b
      GROUP BY b.bicis_id;

      UPDATE bicis b
        JOIN(SELECT b.bicis_id,(b.precio*b.alquileres) total FROM bicis b
      GROUP BY b.bicis_id) cf ON b.bicis_id=cf.bicis_id
        SET b.beneficios=cf.total;


    -- g
       ALTER TABLE alquileres ADD COLUMN IF NOT EXISTS alquileresclientes float;

      SELECT a.cliente,SUM(a.alquileres_id) numa FROM alquileres a 
        GROUP BY a.cliente;

      UPDATE alquileres a
        JOIN ( SELECT a.cliente,SUM(a.alquileres_id) numa FROM alquileres a 
        GROUP BY a.cliente) l ON a.cliente=l.cliente
        SET a.alquileresclientes=l.numa;

      -- h
        

      SELECT c.codigo,
             c.descuento 
        FROM clientes c;

     UPDATE alquileres a
      JOIN (SELECT c.codigo,
             c.descuento 
        FROM clientes c) d ON a.cliente=d.codigo
      SET a.descuento=d.descuento;

  


    -- i

      SELECT b.bicis_id,(b.precio-a.descuento) total FROM bicis b 
        JOIN alquileres a ON b.bicis_id = a.bici;

      UPDATE alquileres a
        JOIN( SELECT  b.bicis_id,(b.precio-a.descuento) total FROM bicis b 
        JOIN alquileres a ON b.bicis_id = a.bici) t ON a.bici=t.bicis_id
        SET a.precio_total=t.total;


  -- consulta de ramon
    -- h
    UPDATE alquileres a
      JOIN clientes c 
      ON a.descuento = c.descuento
      SET a.descuento=c.descuento;

   -- f
    UPDATE alquileres a
      JOIN bicis b ON a.bici = b.bicis_id
      SET a.precio_total=(b.precio*(1-a.descuento));

    UPDATE (SELECT * FROM bicis b,SUM(precio_total) AS suma FROM alquileres GROUP BY bici
      ) c1
 


      

