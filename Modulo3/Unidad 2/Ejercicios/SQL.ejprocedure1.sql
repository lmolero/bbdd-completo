﻿CREATE DATABASE IF NOT EXISTS ejprocedure1;
USE ejprocedure1;

-- procedimientos almacenados 1

-- funcion sql
DELIMITER $$
DROP PROCEDURE IF EXISTS ep1_1 $$
CREATE PROCEDURE ep1_1(num1 int,num2 int)
BEGIN
  DECLARE mayor int;
  SET mayor=GREATEST(num1,num2);

SELECT mayor;


END
$$

DELIMITER ;

-- con if 

  DELIMITER $$
DROP PROCEDURE IF EXISTS ep1_2 $$
CREATE PROCEDURE ep1_2 (num1 int, num2 int)
BEGIN
DECLARE n int DEFAULT 0;
  IF (num1>num2) THEN 
      SELECT n=num1;
    ELSE 
      SELECT n=num2;
  END IF;

  SELECT n;
END
$$
DELIMITER ;

CALL ep1_2(5,9);


-- consulta totales

DELIMITER $$
DROP PROCEDURE IF EXISTS ep1_3 $$
CREATE PROCEDURE ep1_3(num1 int,num2 int)
BEGIN
 CREATE OR REPLACE TEMPORARY TABLE ej1_3( 
  id int AUTO_INCREMENT,
  num int,
    PRIMARY KEY (id));


  INSERT INTO ej1_3(num) VALUES 
  (num1),(num2);

  SELECT MAX(num) FROM ej1_3 e;

END
$$

DELIMITER ;

CALL ep1_3(1,2);

-- procedimientos almacenados 2

-- funcion sql
DELIMITER $$
DROP PROCEDURE IF EXISTS ep2_1 $$
CREATE PROCEDURE ep2_1(num1 int,num2 int, num3 int)
BEGIN
  DECLARE mayor int;
  SET mayor=GREATEST(num1,num2,num3);

SELECT mayor;


END
$$

DELIMITER ;

CALL ep2_1(5,8,6);

-- con if 

  DELIMITER $$
DROP PROCEDURE IF EXISTS ep2_2 $$
CREATE PROCEDURE ep2_2 (num1 int, num2 int, num3 int)
BEGIN

  IF (num1>num2) AND (num1>num3) THEN 
      SELECT num1;
    ELSEIF  
       (num2>num1) AND (num2>num3) THEN 
      SELECT num2;
      ELSE 
      SELECT num3;
  END IF;

END
$$
DELIMITER ;

CALL ep2_2(5,9, 10);


-- consulta totales

DELIMITER $$
DROP PROCEDURE IF EXISTS ep2_3 $$
CREATE PROCEDURE ep2_3(num1 int,num2 int, num3 int)
BEGIN
 CREATE OR REPLACE TEMPORARY TABLE ej2_3( 
  id int AUTO_INCREMENT,
  num int,
    PRIMARY KEY (id));


  INSERT INTO ej2_3(num) VALUES 
  (num1),(num2),(num3);

  SELECT MAX(num) FROM ej2_3 e;

END
$$

DELIMITER ;

CALL ep2_3(1,25,5);

 -- procedimientos almacenados 3

DELIMITER $$
DROP PROCEDURE IF EXISTS ep3 $$
CREATE PROCEDURE ep3(num1 int,num2 int, num3 int,OUT num4 int, OUT num5 int)
BEGIN
  SELECT num1,num2,num3,num4,num5;
  SET num4=GREATEST(num1,num2,num3);
  SET num5=LEAST(num1,num2,num3);

SELECT num4;
  SELECT num5;


END
$$

DELIMITER ;


CALL ep3(1,2,3,@num4,@num5);

--  procedimientos almacenados 4

DELIMITER $$
DROP PROCEDURE IF EXISTS ep4 $$
CREATE PROCEDURE ep4(fecha1 date, fecha2 date)
BEGIN
 
  SELECT DATEDIFF(fecha1,fecha2) dias;

 
END
$$

DELIMITER ;

CALL ep4('2019-10-25','2019-10-1');

-- procedimientos almacenados 5

DELIMITER $$
DROP PROCEDURE IF EXISTS ep5 $$
CREATE PROCEDURE ep5(fecha1 date, fecha2 date)
BEGIN
  
  SELECT TIMESTAMPDIFF(MONTH, fecha1, fecha2 ) Nmeses;

 
END
$$

DELIMITER ;


CALL ep5('2018-10-25','2019-10-1');

--  procedimientos almacenados 6

DELIMITER $$
DROP PROCEDURE IF EXISTS ep6 $$
CREATE PROCEDURE ep6(fecha1 date, fecha2 date,OUT dias int,OUT meses int,OUT año int )
BEGIN
  
  SELECT fecha1,fecha2,dias,meses,año;
  SET dias=DATEDIFF(fecha1,fecha2);
  SET meses=TIMESTAMPDIFF(MONTH,fecha1,fecha2);
  SET año=TIMESTAMPDIFF(YEAR, fecha1,fecha2);

SELECT dias;
  SELECT meses;
  SELECT año;


 
END
$$

DELIMITER ;


CALL ep6('2025-10-1', '2020-10-25',@dias,@meses,@año);


-- 

DELIMITER $$
DROP PROCEDURE IF EXISTS ep7 $$
CREATE PROCEDURE ep7(texto varchar (50))
BEGIN 

  DROP TABLE IF EXISTS ept7;
  CREATE TABLE IF NOT EXISTS ept7(
  id int AUTO_INCREMENT,
  texto varchar(50),
  caracteres varchar(50),
  PRIMARY KEY(id)
  );
    INSERT INTO ept7(texto) VALUES
    (texto);

END
$$
DELIMITER ;


CALL ep7('alejandro');

SELECT * FROM ept7 e;

 -- caracteres
DELIMITER $$
DROP PROCEDURE IF EXISTS ept7 $$
CREATE PROCEDURE ept7(frase varchar(150))
BEGIN
  SELECT LENGTH(frase);
END
$$

DELIMITER ;


CALL ept7('juan');


