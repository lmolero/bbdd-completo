﻿CREATE DATABASE IF NOT EXISTS restricciones;
USE restricciones;

CREATE TABLE IF NOT EXISTS clientes(
  id int  NOT NULL AUTO_INCREMENT,
  edad int NOT NULL,
  PRIMARY KEY(id));

ALTER TABLE clientes ADD COLUMN IF NOT EXISTS edad1 int;

CREATE OR REPLACE VIEW  vista1 AS 
  SELECT * FROM clientes c
  WHERE c.edad>=0 AND  c.edad1>c.edad
WITH LOCAL CHECK OPTION;

TRUNCATE clientes;

INSERT IGNORE INTO vista1 VALUES 
  (DEFAULT, -10, 10),
  (DEFAULT, 10,0),
    (DEFAULT, 10,20);

SELECT * FROM vista1 v;
  SELECT * FROM clientes;

  CREATE TABLE IF NOT EXISTS ventas(
    id int AUTO_INCREMENT,
    cliente int,
    fecha date,
    cantidad int,
    PRIMARY KEY(id));

  ALTER TABLE ventas ADD CONSTRAINT fkventasclientes FOREIGN KEY IF NOT EXISTS (cliente)
  REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE;

  CREATE OR REPLACE VIEW vista2 AS 
  SELECT * FROM ventas 
    WHERE fecha<NOW() AND 
          cantidad BETWEEN 10 AND 100
  WITH LOCAL CHECK OPTION;

  TRUNCATE ventas;
   
  INSERT IGNORE INTO vista2 VALUES
    (DEFAULT, NULL, '2020/1/1', 0),
    (DEFAULT, NULL, '2020/1/1', 80),
    (DEFAULT, NULL, '2018/1/1', 80);

  SELECT * FROM ventas v;

  CREATE OR REPLACE VIEW vista3 AS
    SELECT * FROM ventas v
    WHERE v.cantidad BETWEEN 10 AND  100
    WITH LOCAL CHECK OPTION;

  TRUNCATE ventas;

INSERT IGNORE INTO vista3 VALUES
    (DEFAULT, NULL, '2020/1/1', 0),
    (DEFAULT, NULL, '2020/1/1', 80),
    (DEFAULT, NULL, '2018/1/1', 80);

SELECT * FROM ventas;

  CREATE OR REPLACE VIEW vista4 AS
    SELECT * FROM vista3 v
      WHERE fecha<NOW()
    WITH LOCAL CHECK OPTION;


      TRUNCATE ventas ;

INSERT IGNORE INTO vista4 VALUES
    (DEFAULT, NULL, '2020/1/1', 0),
    (DEFAULT, NULL, '2020/1/1', 80),
    (DEFAULT, NULL, '2018/1/1', 80),
    (DEFAULT, NULL, '2018/1/1', 0);

SELECT * FROM ventas;

 CREATE OR REPLACE VIEW vista5 AS
    SELECT * FROM vista3 v
      WHERE fecha<NOW()
    WITH CASCADED CHECK OPTION;

TRUNCATE  ventas ;

INSERT IGNORE INTO vista5 VALUES
    (DEFAULT, NULL, '2020/1/1', 0),
    (DEFAULT, NULL, '2020/1/1', 80),
    (DEFAULT, NULL, '2018/1/1', 80),
    (DEFAULT, NULL, '2018/1/1', 0);

SELECT * FROM ventas v;

  -- fecha de ventas menor que hoy
   -- cantidad mayor que 10 y menor que 100









