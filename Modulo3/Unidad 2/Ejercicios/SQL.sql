﻿ USE practica1; -- selecciono la base de datos en la que voy a trabajar

/* COLSULTA 1 */
SELECT E.emp_no,
       E.apellido,
       E.oficio,
       E.dir,
       E.fecha_alt,
       E.salario,
       E.comision,
       E.dept_no
 FROM EMPLE E; 

/* CONSULTA 2 */
  SELECT d.dept_no,
         d.dnombre,
         d.loc    
  FROM depart d;

  /* CONSULTA 3*/
SELECT DISTINCT
       e.apellido,
       e.oficio
      
FROM emple e;

/* CONSULTA 4 */
SELECT d.dept_no,
       d.loc 
FROM depart d;

/* CONSULTA 5 */
SELECT
       d.dept_no,
       d.dnombre,
       d.loc 
FROM depart d;
 
/* CONSULTA 6 */
SELECT
  COUNT(*)
FROM emple e;


