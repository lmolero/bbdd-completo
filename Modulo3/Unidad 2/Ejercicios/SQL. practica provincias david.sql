﻿SELECT
  provincia
FROM provincias;
SELECT
  provincia,
  poblacion / superficie densidad
FROM provincias;
SELECT
  SQRT(2);



SELECT
  provincia,
  CHAR_LENGTH(provincia)
FROM provincias;

SELECT DISTINCT
  autonomia
FROM provincias;

SELECT
  provincia
FROM provincias
WHERE provincia = autonomia;

SELECT
  provincia
FROM provincias
WHERE provincia LIKE "%ue%";

SELECT
  provincia
FROM provincias
WHERE provincia LIKE "a%"
OR "á%";

SELECT DISTINCT
  autonomia
FROM provincias
WHERE autonomia LIKE "%ana";

SELECT DISTINCT
  autonomia,
  CHAR_LENGTH(autonomia)
FROM provincias
ORDER BY autonomia DESC;

SELECT DISTINCT
  provincia
FROM provincias
WHERE autonomia LIKE '% %';

SELECT
  provincia
FROM provincias
WHERE provincia LIKE '% %';

SELECT DISTINCT
  autonomia
FROM provincias
WHERE autonomia NOT LIKE '% %'
ORDER BY autonomia DESC;

SELECT DISTINCT
  autonomia
FROM provincias
WHERE provincia LIKE '% %'
ORDER BY autonomia ASC;

SELECT
  provincia
FROM provincias
WHERE provincia NOT LIKE '% %';

SELECT DISTINCT
  autonomia
FROM provincias
WHERE autonomia LIKE '% %'
ORDER BY autonomia DESC;

SELECT DISTINCT
  autonomia
FROM provincias
WHERE autonomia LIKE 'can%'
ORDER BY autonomia ASC;

SELECT DISTINCT
  autonomia
FROM provincias
WHERE poblacion >= 1000000
ORDER BY autonomia ASC;

SELECT
  SUM(poblacion)
FROM provincias;

SELECT
  COUNT(provincia)
FROM provincias;

SELECT
  autonomia
FROM provincia;

SELECT
  LOCATE('la', 'lalalala');

SELECT
  autonomia
FROM provincias
WHERE LOCATE(provincia, autonomia) > 0;


SELECT
  provincia
FROM provincias
WHERE provincia LIKE '%ñ%' COLLATE utf8_bin
UNION
SELECT
  autonomia
FROM provincias
WHERE autonomia LIKE '%ñ%' COLLATE utf8_bin;

SELECT
  SUM(superficie)
FROM provincias;

SELECT
  provincia
FROM provincias
ORDER BY provincia ASC LIMIT 1;

SELECT
  provincia
FROM provincias
WHERE CHAR_LENGTH(provincia) > CHAR_LENGTH(autonomia);

SELECT
  COUNT(DISTINCT autonomia)
FROM provincias;

SELECT
  MIN(CHAR_LENGTH(autonomia))
FROM provincias;

SELECT
  MAX(CHAR_LENGTH(provincia))
FROM provincias;

SELECT
  ROUND(AVG(poblacion))
FROM provincias
WHERE poblacion BETWEEN 2000000 AND 3000000;

SELECT DISTINCT
  autonomia
FROM provincias
WHERE LOWER(provincia) LIKE '%á%' COLLATE utf8_bin
OR LOWER(provincia) LIKE '%é%' COLLATE utf8_bin
OR LOWER(provincia) LIKE '%í%' COLLATE utf8_bin
OR LOWER(provincia) LIKE '%ó%' COLLATE utf8_bin
OR LOWER(provincia) LIKE '%ú%' COLLATE utf8_bin;

SELECT
  MAX(poblacion)
FROM provincias;

SELECT
  provincia
FROM provincias
WHERE poblacion = (SELECT
    MAX(poblacion)
  FROM provincias);

SELECT
  MAX(poblacion)
FROM provincias
WHERE poblacion <= 1000000;

SELECT
  provincia
FROM provincias
WHERE poblacion = (SELECT
    MAX(poblacion)
  FROM provincias
  WHERE poblacion <= 1000000);

SELECT
  provincia
FROM provincias
WHERE poblacion = (SELECT
    MIN(poblacion)
  FROM provincias
  WHERE poblacion >= 1000000);

SELECT
  autonomia
FROM provincias
WHERE superficie = (SELECT
    MAX(superficie)
  FROM provincias);

SELECT
  provincia
FROM provincias
WHERE poblacion > (SELECT
    AVG(poblacion)
  FROM provincias);

SELECT  DENSE_RANK(poblacion) FROM provincias;

/*(37) Densidad de población del país*/

SELECT
  SUM(p.poblacion)
FROM provincias p;

SELECT
  SUM(p.superficie)
FROM provincias p;


SELECT
  (SELECT
      SUM(p.poblacion)
    FROM provincias p) / (SELECT
      SUM(p.superficie)
    FROM provincias p);c

/*(38) ¿Cuántas provincias tiene cada comunidad autónoma?*/

SELECT
  p.autonomia,
  COUNT(p.provincia)
FROM provincias p
GROUP BY p.autonomia;

/*(39) Listado del número de provincias por autonomía ordenadas de más a menos provincias y por autonomía en caso de coincidir*/

SELECT
  COUNT(p.provincia),
  p.autonomia
FROM provincias p
GROUP BY p.autonomia
ORDER BY COUNT(p.provincia) DESC, p.autonomia ASC;

/*(40) ¿Cuántas provincias con nombre compuesto tiene cada comunidad autónoma?*/

SELECT DISTINCT
  autonomia,
  COUNT(*)
FROM provincias
WHERE provincia LIKE '% %'
GROUP BY autonomia;

/*(41) Autonomías uniprovinciales*/

SELECT
  p.autonomia,
  COUNT(p.autonomia) cuenta
FROM provincias p
GROUP BY p.autonomia;

SELECT
  c1.autonomia
FROM (SELECT
    p.autonomia,
    COUNT(p.autonomia) cuenta
  FROM provincias p
  GROUP BY p.autonomia) c1
WHERE cuenta = 1;

-- (42) ¿Qué autonomía tiene 5 provincias?
SELECT
  p.autonomia
FROM provincias p
GROUP BY autonomia
HAVING COUNT(*) = 5;

-- (43) Población de la autonomía más poblada
SELECT
  autonomia,
  SUM(p.poblacion)
FROM provincias p
GROUP BY p.autonomia;

CREATE OR REPLACE VIEW sumap
AS
SELECT
  autonomia,
  SUM(p.poblacion) suma
FROM provincias p
GROUP BY p.autonomia;

SELECT
  MAX(s.suma)
FROM sumap s;

-- (44) ¿Qué porcentaje del total nacional representa Cantabria en población y en superficie?

SELECT
  (SELECT
      poblacion
    FROM provincias
    WHERE provincia = 'cantabria') / (SELECT
      SUM(poblacion)
    FROM provincias) * 100 pob,
  (SELECT
      superficie
    FROM provincias
    WHERE provincia = 'cantabria') / (SELECT
      SUM(superficie)
    FROM provincias) * 100 sup;

-- (45) Automía más extensa
SELECT
  p.autonomia,
  SUM(p.superficie)
FROM provincias p
GROUP BY p.autonomia;

CREATE OR REPLACE VIEW sums
AS
SELECT
  p.autonomia,
  SUM(p.superficie) sumas
FROM provincias p
GROUP BY p.autonomia;

SELECT
  MAX(s.sumas)
FROM sums s;

CREATE OR REPLACE VIEW autonomiaex
AS
SELECT
  MAX(s.sumas)
FROM sums s;

SELECT
  MAX(sumas)
FROM sums;

SELECT
  autonomia
FROM sums
WHERE sumas = (SELECT
    MAX(sumas)
  FROM sums);

-- (46) Autonomía con más provincias. Utiliza como alias n y c1.

SELECT
  COUNT(p.provincia) cuentap
FROM provincias p
GROUP BY p.autonomia;

CREATE OR REPLACE VIEW automaspro
AS
SELECT
  p.autonomia,
  COUNT(p.provincia) cuentap
FROM provincias p
GROUP BY p.autonomia;

SELECT
  MAX(a.cuentap)
FROM automaspro a;

SELECT
  autonomia
FROM automaspro a
WHERE cuentap = (SELECT
    MAX(a.cuentap)
  FROM automaspro a);

-- (47) ¿En qué posición del ranking autonómico por población de mayor a menor está Cantabria? 

SELECT
  COUNT(*)
FROM (SELECT
    autonomia,
    SUM(poblacion) p
  FROM provincias
  GROUP BY autonomia) c1
WHERE p >= (SELECT
    p
  FROM (SELECT
      autonomia,
      SUM(poblacion) p
    FROM provincias
    GROUP BY autonomia) c1
  WHERE autonomia = 'cantabria');


-- para provincias
SELECT
  *
FROM provincias p
WHERE p.provincia = 'cantabria';

SELECT
  COUNT(*)
FROM provincias
WHERE poblacion >= (SELECT
    p.poblacion
  FROM provincias p
  WHERE p.provincia = 'cantabria');

-- (48) Provincia más despoblada de la autonomía más poblada
SELECT
  p.autonomia,
  SUM(p.poblacion) suma
FROM provincias p
GROUP BY p.autonomia;

SELECT
  MAX(suma)
FROM (SELECT
    p.autonomia,
    SUM(p.poblacion) suma
  FROM provincias p
  GROUP BY p.autonomia) c48;

-- c1
SELECT
  autonomia,
  SUM(poblacion) s
FROM provincias
GROUP BY autonomia;
-- c2
SELECT
  MAX(s)
FROM (SELECT
    autonomia,
    SUM(poblacion) s
  FROM provincias
  GROUP BY autonomia) c1;
-- c3
SELECT
  autonomia
FROM provincias
GROUP BY autonomia
HAVING SUM(poblacion) = (SELECT
    MAX(s)
  FROM (SELECT
      autonomia,
      SUM(poblacion) s
    FROM provincias
    GROUP BY autonomia) c1);
-- c4
SELECT
  *
FROM provincias
WHERE autonomia = (SELECT
    autonomia
  FROM provincias
  GROUP BY autonomia
  HAVING SUM(poblacion) = (SELECT
      MAX(s)
    FROM (SELECT
        autonomia,
        SUM(poblacion) s
      FROM provincias
      GROUP BY autonomia) c1));
-- c5
SELECT
  MIN(poblacion)
FROM provincias
WHERE autonomia = (SELECT
    autonomia
  FROM provincias
  GROUP BY autonomia
  HAVING SUM(poblacion) = (SELECT
      MAX(s)
    FROM (SELECT
        autonomia,
        SUM(poblacion) s
      FROM provincias
      GROUP BY autonomia) c1));
-- resultado
SELECT
  c4.provincia
FROM (SELECT
    *
  FROM provincias
  WHERE autonomia = (SELECT
      autonomia
    FROM provincias
    GROUP BY autonomia
    HAVING SUM(poblacion) = (SELECT
        MAX(s)
      FROM (SELECT
          autonomia,
          SUM(poblacion) s
        FROM provincias
        GROUP BY autonomia) c1))) c4
WHERE c4.poblacion = (SELECT
    MIN(poblacion)
  FROM provincias
  WHERE autonomia = (SELECT
      autonomia
    FROM provincias
    GROUP BY autonomia
    HAVING SUM(poblacion) = (SELECT
        MAX(s)
      FROM (SELECT
          autonomia,
          SUM(poblacion) s
        FROM provincias
        GROUP BY autonomia) c1)));




-- (17) Mostrar los datos de los empleados cuyo oficio sea ANALISTA
SELECT
  *
FROM emple e
WHERE e.oficio = 'analista';

-- (18) Mostrar los datos de los empleados cuyo oficio se ANALISTA y ganen más de 2000
SELECT
  *
FROM emple e
WHERE e.oficio = 'analista'
AND e.salario > 2000;

-- (33) Listar el apellido de todos los empleados y ordenarlos por oficio y por nombre

SELECT
  e.apellido
FROM emple e
ORDER BY e.oficio, e.apellido;

-- (36) Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por Z. Listar todos los campos de la tabla empleados
SELECT
  *
FROM emple e
WHERE e.apellido NOT LIKE '%z';

-- (14) Mostrar el código de los empleados cuyo salario sea mayor que 2000
SELECT
  e.emp_no
FROM emple e
WHERE e.salario > 2000;

-- (15) Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000
SELECT
  e.emp_no,
  e.apellido
FROM emple e
WHERE e.salario < 2000;

-- (16) Mostrar los datos de los empleados cuyo salario esté entre 1500 y 2500
SELECT
  *
FROM emple e
WHERE e.salario BETWEEN 1500 AND 2500;

-- (19) Seleccionar el apellido y oficio de los empleados del departamento número 20
SELECT
  e.apellido,
  e.oficio
FROM emple e
WHERE e.dept_no = '20';
-- (21) Mostrar todos los datos de empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente.
SELECT
  *
FROM emple e
WHERE e.apellido LIKE 'm%'
OR e.apellido LIKE 'n%'
ORDER BY e.apellido;

-- (22) Seleccionar los empleados cuyo oficio sea VENDEDOR. Mostrar los datos ordenados por apellido de forma ascendente.
SELECT
  *
FROM emple e
WHERE e.oficio = 'vendedor'
ORDER BY e.apellido;

-- (24) Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ANALISTA. Ordenar el resultado por apellido y oficio de forma ascendente
SELECT
  *
FROM emple e
WHERE e.dept_no = '10'
AND e.oficio = 'analista'
ORDER BY e.apellido, e.oficio;

-- (25) Realizar un listado de los distintos meses en que los empleados se han dado de alta
SELECT DISTINCT
  MONTH(e.fecha_alt)
FROM emple e;

-- (26) Realizar un listado de los distintos años en los que los empleados se han dado de alta
SELECT DISTINCT
  YEAR(e.fecha_alt)
FROM emple e;

-- (27) Realizar un listado de los distintos días del mes en que los empleados se han dado de alta

SELECT DISTINCT
  DAY(e.fecha_alt)
FROM emple e;

-- (28) Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20
SELECT
  e.apellido
FROM emple e
WHERE e.salario > 2000
OR e.dept_no = 20;

-- (35) Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A o por M. Listar el apellido de los empleados
SELECT
  apellido
FROM emple
WHERE apellido LIKE 'a%'
OR apellido LIKE 'm%';

-- (37) Seleccionar de la tabla EMPLE aquellas filas cuyo apellido empiece por A y el oficio tenga una E en cualquier posición. Ordenar la salida por oficio y por salario de forma descendente
SELECT
  *
FROM emple
WHERE apellido LIKE 'a%'
AND oficio LIKE '%e%'
ORDER BY oficio, salario DESC;

-- (34) Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A. Listar el apellido de los empleados
SELECT
  apellido
FROM emple
WHERE apellido LIKE 'a%';

-- (6) Indicar el número de empleados que hay
SELECT
  COUNT(*)
FROM emple e;

-- (9) Indicar el número de departamentos que hay
SELECT
  COUNT(*)
FROM depart d;

-- (20) Contar el número de empleados cuyo oficio sea VENDEDOR

SELECT
  COUNT(*)
FROM emple e
WHERE e.oficio LIKE 'VENDEDOR';

-- (29) Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece
SELECT
  e.apellido,
  d.dnombre
FROM emple e
  JOIN depart d
    ON e.dept_no = d.dept_no;

-- (30) Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que pertenece. Ordenar los resultados por apellido de forma descendente.
SELECT
  e.apellido,
  e.oficio,
  d.dnombre
FROM emple e
  JOIN depart d
    ON e.dept_no = d.dept_no
ORDER BY e.apellido DESC;

-- (23) Mostrar los apellidos del empleado que más gana
SELECT
  MAX(e.salario)
FROM emple e;

SELECT
  e.apellido
FROM emple e
WHERE e.salario = (SELECT
    MAX(e.salario)
  FROM emple e);

-- (10) Indicar el número de empleados más el número de departamentos

/*
SELECT ((SELECT COUNT( *) FROM emple e)+
 ( SELECT  COUNT(*) FROM depart d));
  */

-- (31) Listar el número de departamento de aquellos departamentos que tengan empleados y el número de empleados que tengan.

SELECT
  (e.emp_no),
  (e.dept_no)
FROM depart d
  JOIN emple e USING (dept_no);

SELECT
  dept_no,
  COUNT(*)
FROM (SELECT
    (e.emp_no),
    (e.dept_no)
  FROM depart d
    JOIN emple e USING (dept_no)) c1
GROUP BY dept_no;

SELECT
  e.dept_no,
  COUNT(*)
FROM emple e
GROUP BY e.dept_no;

--  (32) Listar el número de empleados por departamento, ordenados de más a menos empleados, incluyendo los departamentos que no tengan empleados. El encabezado de la tabla será dnombre y NUMERO_DE_EMPLEADOS

SELECT
  (e.emp_no),
  (e.dept_no)
FROM depart d
  JOIN emple e USING (dept_no);

SELECT
  c1.dnombre,
  COUNT(*)
FROM (SELECT
    *
  FROM depart d
    JOIN emple e USING (dept_no)) c1
GROUP BY dept_no;

SELECT
  *
FROM (SELECT
    c1.dnombre,
    COUNT(*)
  FROM (SELECT
      *
    FROM depart d
      JOIN emple e USING (dept_no)) c1
  GROUP BY dept_no) c1
  LEFT JOIN emple e USING (dept_no)
WHERE e.dept_no IS NULL;

-- lo que funciona 
SELECT
  dept_no,
  COUNT(*) n
FROM emple
GROUP BY dept_no;

SELECT
  dnombre,
  IFNULL(n, 0) n
FROM (SELECT
    dept_no,
    COUNT(*) n
  FROM emple
  GROUP BY dept_no) c1
  RIGHT JOIN depart USING (dept_no);

-- (15) Listar el nombre de la persona y el nombre de su supervisor
SELECT
  s.persona,
  s.supervisor
FROM supervisa s;

-- (19) Indicar el nombre de las personas que trabajan para FAGOR

SELECT
  t.persona
FROM trabaja t
WHERE t.compañia = 'fagor';

-- (22) Indicar el nombre de las personas que trabajan para FAGOR o para INDRA
SELECT
  t.persona
FROM trabaja t
WHERE t.compañia = 'fagor'
OR t.compañia = 'indra';

-- (01) Indicar el número de ciudades que hay en la tabla ciudades
SELECT
  COUNT(*)
FROM ciudad c;

-- (48) Calcula la cantidad media de programas por pedido que se distribuyen cuyo código es 7


SELECT
  AVG(cantidad)
FROM distribuye d
WHERE d.codigo = 7;

-- (49) Calcula la mínima cantidad de programas de código 7 que se han enviado a los comercios

SELECT
  MIN(cantidad)
FROM distribuye d
WHERE d.codigo = 7;

-- (50) Calcula la máxima cantidad de programas de código 7 que se ha distribuido a las tiendas
SELECT
  MAX(cantidad)
FROM distribuye d
WHERE d.codigo = 7;

-- (52) Calcular el número de registros que se han realizado por Internet
SELECT
  COUNT(*)
FROM registra r
WHERE r.medio = 'internet';

-- (5) Genera una lista de las ciudades con establecimientos donde se distribuyen programas, sin que aparezcan valores duplicados (utiliza DISTINCT)
SELECT DISTINCT
  ciudad
FROM comercio
  JOIN distribuye USING (cif);

-- (51) ¿En cuántos establecimientos se distribuye el programa cuyo código es 7?
SELECT
  COUNT(DISTINCT comercio.cif)
FROM distribuye comercio
  JOIN distribuye USING (cif)
WHERE comercio.codigo = 7;

-- (5b) Genera una lista de las ciudades con establecimientos donde se han registrado programas, sin que aparezcan valores duplicados (utiliza DISTINCT)
SELECT DISTINCT
  ciudad
FROM comercio
  JOIN registra r USING (cif);

-- (38) Genera un listado en el que aparezca cada cliente junto al programa que ha registrado, el medio con el que lo ha hecho y el comercio en el que lo ha adquirido
SELECT
  cliente.nombre,
  p.nombre,
  r.medio,
  c.nombre
FROM registra r
  JOIN cliente USING (dni)
  JOIN programa p USING (codigo)
  JOIN comercio c USING (cif);

-- (41) Nombre de aquellos fabricantes cuyo país es el mismo que Oracle (Subconsulta)
SELECT
  f.nombre
FROM fabricante f
WHERE f.pais = 'Estados Unidos'
AND f.nombre <> 'oracle';

-- (42) Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez (Subconsulta), excluyendo Pepe Pérez
SELECT
  c.nombre
FROM cliente c
WHERE edad = 45
AND c.nombre <> 'pepe pérez';

-- (43) Genera un listado con los comercios que tienen su sede en la misma ciudad que tiene el comercio FNAC (Subconsulta), excluyendo a la FNAC
SELECT
  *
FROM comercio c
WHERE c.nombre = 'fnac';

SELECT
  c.nombre
FROM (SELECT
    c.cif,
    c.nombre,
    c.ciudad
  FROM comercio c
  WHERE c.nombre = 'fnac') c1
  JOIN comercio c USING (ciudad)
WHERE c.ciudad <> c1.ciudad;

-- (27) Genera un listado de los programas que desarrolla Oracle
SELECT
  p.nombre,
  d.id_fab
FROM programa p
  JOIN desarrolla d USING (codigo);

SELECT
  p.*
FROM programa p
  JOIN desarrolla d USING (codigo)
  JOIN fabricante f USING (id_fab)
WHERE f.nombre = 'oracle';

SELECT
  id_fab
FROM fabricante
WHERE nombre = 'Oracle';

SELECT
  codigo
FROM desarrolla
WHERE id_fab = (SELECT
    id_fab
  FROM fabricante
  WHERE nombre = 'Oracle');

SELECT
  *
FROM (SELECT
    codigo
  FROM desarrolla
  WHERE id_fab = (SELECT
      id_fab
    FROM fabricante
    WHERE nombre = 'Oracle')) c1
  JOIN programa USING (codigo);

-- (30) ¿Qué fabricante ha desarrollado Freddy Hardest?
SELECT
  p.codigo
FROM programa p
WHERE p.nombre = 'Freddy Hardest';

SELECT
  f.nombre
FROM desarrolla d
  JOIN (SELECT
      p.codigo
    FROM programa p
    WHERE p.nombre = 'Freddy Hardest') c1 USING (codigo)
  JOIN fabricante f USING (id_fab);

-- (31) Selecciona el nombre de los programas que se registran por Internet
SELECT
  p.nombre
FROM registra r
  JOIN programa p USING (codigo)
WHERE r.medio = 'internet';

-- (33) ¿Qué medios ha utilizado para registrarse Pepe Pérez?
SELECT
  r.medio
FROM registra r
  JOIN cliente USING (dni)
WHERE nombre = 'Pepe Pérez';

-- (35) ¿Qué programas han recibido registros por tarjeta postal?
SELECT
  p.nombre
FROM programa p
  JOIN registra r USING (codigo)
WHERE r.medio = 'tarjeta postal';

-- (36) ¿En qué localidades se han registrado productos por Internet?
SELECT
  c.ciudad
FROM comercio c
  JOIN registra r USING (cif)
WHERE r.medio = 'internet';



-- (32) Selecciona el nombre de las personas que se registran por Internet
SELECT
  c.nombre
FROM cliente c
  JOIN registra r USING (dni)
WHERE r.medio = 'internet'
ORDER BY c.nombre;

-- (37) Obtén un listado de los nombres de las personas que se han registrado por Internet, junto al nombre de los programas para los que ha efectuado el registro
SELECT
  c.nombre
FROM cliente c
  JOIN registra r USING (dni)
  JOIN programa p USING (codigo)
WHERE r.medio = 'internet';

-- (40) Obtén el nombre de los usuarios que han registrado Access XP

SELECT
  p.codigo,
  p.nombre,
  r.dni
FROM programa p
  JOIN registra r USING (codigo);

SELECT
  c40.nombre,
  c.nombre
FROM (SELECT
    p.codigo,
    p.nombre,
    r.dni
  FROM programa p
    JOIN registra r USING (codigo)) c40
  JOIN cliente c
  USING (dni)
WHERE c40.nombre = 'Access XP';

-- (28) ¿Qué comercios distribuyen Windows?
SELECT
  *
FROM programa p
  JOIN distribuye d USING (codigo)
WHERE p.nombre = 'WINDOWS';

SELECT
  c.nombre,
  c.ciudad
FROM (SELECT
    *
  FROM programa p
    JOIN distribuye d USING (codigo)
  WHERE p.nombre = 'WINDOWS') c28
  JOIN comercio c USING (cif);

-- (29) Genera un listado del nombre de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid

SELECT
  *
FROM programa p
  JOIN distribuye d USING (codigo);

SELECT DISTINCT
  c29.codigo,
  c29.nombre,
  c29.version,

  c29.cantidad

FROM (SELECT
    *
  FROM programa p
    JOIN distribuye d USING (codigo)) c29
  JOIN comercio c USING (cif)
WHERE c.nombre = 'el corte inglés'
AND c.ciudad = 'madrid';

-- (39) Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle

SELECT
  *
FROM fabricante f
  JOIN desarrolla d USING (id_fab)
WHERE f.nombre = 'oracle';

SELECT
  *
FROM (SELECT
    *
  FROM fabricante f
    JOIN desarrolla d USING (id_fab)
  WHERE f.nombre = 'oracle') c39
  JOIN distribuye d USING (codigo);

SELECT DISTINCT
  c.ciudad
FROM (SELECT
    *
  FROM (SELECT
      *
    FROM fabricante f
      JOIN desarrolla d USING (id_fab)
    WHERE f.nombre = 'oracle') c39
    JOIN distribuye d USING (codigo)) c39_1
  JOIN comercio c USING (cif);

-- (53) Obtener el número total de programas que se han distribuido en Sevilla
SELECT
  *
FROM distribuye d
  JOIN programa p USING (codigo);

SELECT
  SUM(c53.cantidad) numprogramas
FROM (SELECT
    *
  FROM distribuye d
    JOIN programa p USING (codigo)) c53
  JOIN comercio c USING (cif)
WHERE c.ciudad = 'sevilla';

-- (54) Calcular el número total de programas que han desarrollado los fabricantes cuyo país es Estados Unidos
SELECT
  *
FROM fabricante f
  JOIN desarrolla d USING (id_fab)
WHERE f.pais = 'estados unidos';

-- (26b) Obtén un listado de aquellos programas que tengan más de una versión, ordenando la lista por programa y por la versión de más a menos moderna
SELECT
  p.nombre
FROM programa p
GROUP BY p.nombre
HAVING COUNT(*) > 1
ORDER BY p.version DESC;

SELECT
  codigo,
  nombre,
  version
FROM programa p
  JOIN (SELECT
      p.nombre
    FROM programa p
    GROUP BY p.nombre
    HAVING COUNT(*) > 1) C1 USING (nombre)
ORDER BY nombre, version DESC;

-- solucion david
SELECT
  *
FROM programa
WHERE nombre IN (SELECT
    nombre
  FROM programa
  GROUP BY nombre
  HAVING COUNT(*) > 1)
ORDER BY nombre, version DESC;


-- (28b) ¿Qué comercios NO distribuyen Windows?

SELECT
  *
FROM programa p
  JOIN distribuye d USING (codigo)
WHERE p.nombre = 'WINDOWS';

SELECT
  c.cif
FROM comercio c;

SELECT
  *
FROM (SELECT
    c.cif
  FROM comercio c) c1
  LEFT JOIN (SELECT
      *
    FROM programa p
      JOIN distribuye d USING (codigo)
    WHERE p.nombre = 'WINDOWS') c2 USING (cif);

SELECT
  *
FROM comercio c
  JOIN (SELECT
      *
    FROM (SELECT
        c.cif
      FROM comercio c) c1
      LEFT JOIN (SELECT
          *
        FROM programa p
          JOIN distribuye d USING (codigo)
        WHERE p.nombre = 'WINDOWS') c2 USING (cif)) c3 USING (cif)
WHERE c3.codigo IS NULL;


-- my solucion


--  (28e) ¿Qué programas sólo se distribuyen en un comercio? Entendiendo como programas diferentes las distintas versiones de un programa.

SELECT
  d.codigo,
  COUNT(d.codigo) cuenta
FROM distribuye d
GROUP BY d.codigo
HAVING cuenta = 1;

SELECT
  *
FROM programa p
  JOIN (SELECT
      d.codigo,
      COUNT(d.codigo) cuenta
    FROM distribuye d
    GROUP BY d.codigo
    HAVING cuenta = 1) unicosp USING (codigo);

-- (28d) ¿Qué comercios sólo distribuyen Windows?
SELECT
  *
FROM distribuye d
  JOIN programa p USING (codigo)
WHERE p.nombre = 'windows'
GROUP BY d.cif;

SELECT * FROM (SELECT * FROM distribuye d
  JOIN programa p USING(codigo)
  WHERE p.nombre='windows'
  GROUP BY d.cif)c28d JOIN comercio c USING(cif)
WHERE cif not in(
select distinct c28d.cif from(
select p.codigo from programa p where p.nombre!='windows')c3;



-- asi lo hizo david
SELECT
  *
FROM (SELECT DISTINCT
    cif
  FROM (SELECT
      codigo
    FROM programa
    WHERE nombre = 'windows') c1
    JOIN distribuye USING (codigo)) c2
WHERE cif NOT IN (SELECT DISTINCT
    cif
  FROM (SELECT
      codigo
    FROM programa
    WHERE nombre != 'windows') c3
    JOIN distribuye USING (codigo));


-- (44) Nombre de aquellos clientes, ordenados alfabéticamente, que han registrado un producto de la misma forma que el cliente Pepe Pérez (Subconsulta)

SELECT
  r.codigo
FROM registra r
  JOIN cliente c USING (dni)
WHERE c.nombre = 'pepe pérez';

SELECT
  *
FROM registra r
WHERE r.codigo IN (SELECT DISTINCT
    r.codigo
  FROM registra r
    JOIN cliente c USING (dni)
  WHERE c.nombre = 'pepe pérez');

CREATE OR REPLACE VIEW igualpepe
AS
SELECT
  *
FROM registra r
WHERE r.codigo IN (SELECT DISTINCT
    r.codigo
  FROM registra r
    JOIN cliente c USING (dni)
  WHERE c.nombre = 'pepe pérez');

SELECT DISTINCT
  c.nombre
FROM cliente c
  JOIN igualpepe i USING (dni)
WHERE c.nombre NOT IN ('pepe peréz');

--  (28f) ¿Qué comercios sólo distribuyen un programa?
SELECT
  COUNT(d.cif) comercio
FROM distribuye d
GROUP BY d.cif
HAVING comercio = 1;

-- solucion david 
SELECT
  dni
FROM cliente
WHERE nombre = 'pepe perez';
SELECT
  medio
FROM registra
WHERE dni = (SELECT
    dni
  FROM cliente
  WHERE nombre = 'pepe perez');

SELECT
  dni
FROM (SELECT
    medio
  FROM registra
  WHERE dni = (SELECT
      dni
    FROM cliente
    WHERE nombre = 'pepe perez')) c1
  JOIN registra USING (medio)
WHERE dni <> (SELECT
    dni
  FROM cliente
  WHERE nombre = 'pepe perez');

SELECT
  *
FROM (SELECT
    dni
  FROM (SELECT
      medio
    FROM registra
    WHERE dni = (SELECT
        dni
      FROM cliente
      WHERE nombre = 'pepe perez')) c1
    JOIN registra USING (medio)
  WHERE dni <> (SELECT
      dni
    FROM cliente
    WHERE nombre = 'pepe perez')) c2
  JOIN cliente USING (dni)
ORDER BY nombre;

-- (58) Listado de la cantidad de versiones de cada programa ordenados por el nombre del programa
SELECT
  p.nombre,
  COUNT(p.nombre)
FROM programa p
GROUP BY p.nombre
ORDER BY p.nombre;

-- (59) Nombre de los programas que sólo dispongan de una única versión
SELECT
  *
FROM (SELECT
    p.nombre,
    COUNT(p.nombre) cuenta
  FROM programa p
  GROUP BY p.nombre) c
HAVING c.cuenta = 1;

-- solucion david 
SELECT
  nombre
FROM programa
GROUP BY 1
HAVING COUNT(*) = 1;

-- (61) Nombre de los programas que se distribuyen en un único local, entendiendo como el mismo programa a todas sus versiones
SELECT
  p.nombre
FROM programa p
  JOIN distribuye d USING (codigo)
GROUP BY p.nombre
HAVING COUNT(d.cif) = 1;

-- (62) Nombre de los programas que se distribuyen en un único comercio, entendiendo como el mismo programa a todas sus versiones

SELECT
  *
FROM (SELECT DISTINCT
    p.codigo,
    p.nombre,
    p.version
  FROM programa p
    JOIN distribuye d USING (codigo)
    JOIN comercio c USING (cif)) c1
GROUP BY c1.nombre
HAVING COUNT(*) = 1;


SELECT
  programa
FROM (SELECT DISTINCT
    p.nombre programa,
    c.nombre comercio
  FROM programa p
    JOIN distribuye d USING (codigo)
    JOIN comercio c USING (cif)) c1
GROUP BY 1
HAVING COUNT(*) = 1;


-- (60) Nombre de los comercios que distribuyen únicamente 2 programas. Entendiendo por un único programa a todas sus versiones.
SELECT
  d.cif,
  p.codigo,
  p.nombre
FROM programa p
  JOIN distribuye d USING (codigo)
;

SELECT
  c.nombre
FROM comercio c
  JOIN (SELECT
      d.cif,
      p.codigo,
      p.nombre
    FROM programa p
      JOIN distribuye d USING (codigo)) c1 USING (cif)
GROUP BY c1.cif
HAVING COUNT(*) = 2;

-- (64) Ciudades donde se puedan adquirir programas distribuidos en exclusiva por los comercios (no los locales comerciales, aunque sí que hay que tener en cuenta el stock)
SELECT
  *
FROM programa p
  JOIN distribuye d USING (codigo)
  JOIN comercio c USING (cif)
GROUP BY c.cif;


SELECT DISTINCT
  ciudad
FROM (SELECT
    programa
  FROM (SELECT DISTINCT
      programa.nombre programa,
      comercio.nombre comercio
    FROM programa
      JOIN distribuye USING (codigo)
      JOIN comercio USING (cif)) c1
  GROUP BY 1
  HAVING COUNT(*) = 1) c2
  JOIN programa
    ON programa = nombre
  JOIN distribuye USING (codigo)
  JOIN comercio USING (cif);

-- (57) Nombre de los comercios que sólo distribuyen un único programa además de Windows


SELECT
  c2.comercio
FROM (SELECT DISTINCT
    c.nombre comercio,
    p.nombre programa
  FROM distribuye d
    JOIN programa p USING (codigo)
    JOIN comercio c USING (cif)) c2
GROUP BY c2.comercio
HAVING COUNT(*) = 2;

SELECT
  c.nombre comercio
FROM programa p
  JOIN distribuye d USING (codigo)
  JOIN comercio c USING (cif)
WHERE p.nombre = 'windows';


SELECT
  *
FROM (SELECT
    c.nombre comercio
  FROM programa p
    JOIN distribuye d USING (codigo)
    JOIN comercio c USING (cif)
  WHERE p.nombre = 'windows') c1
  NATURAL JOIN (SELECT
      c2.comercio
    FROM (SELECT DISTINCT
        c.nombre comercio,
        p.nombre programa
      FROM distribuye d
        JOIN programa p USING (codigo)
        JOIN comercio c USING (cif)) c2
    GROUP BY c2.comercio
    HAVING COUNT(*) = 2) c2;

-- (63) Nombres de comercios (no locales comerciales) que tengan exclusivas de distribución. Es decir, que distribuyan programas que sólo ellos distribuyen.
SELECT
  p.nombre programa,
  c.nombre comercio
FROM programa p
  JOIN distribuye d USING (codigo)
  JOIN comercio c USING (cif);


SELECT
  c1.programa
FROM (SELECT DISTINCT
    p.nombre programa,
    c.nombre comercio
  FROM programa p
    JOIN distribuye d USING (codigo)
    JOIN comercio c USING (cif)) c1
GROUP BY c1.programa
HAVING COUNT(*) = 1;


SELECT DISTINCT
  c1.comercio
FROM (SELECT
    c1.programa
  FROM (SELECT DISTINCT
      p.nombre programa,
      c.nombre comercio
    FROM programa p
      JOIN distribuye d USING (codigo)
      JOIN comercio c USING (cif)) c1
  GROUP BY c1.programa
  HAVING COUNT(*) = 1) c2
  JOIN (SELECT DISTINCT
      p.nombre programa,
      c.nombre comercio
    FROM programa p
      JOIN distribuye d USING (codigo)
      JOIN comercio c USING (cif)) c1 USING (programa);

-- ciclistas

--  (d) Obtener los datos de las etapas que no comienzan en la misma ciudad en que acaba la etapa anterior

SELECT
  *
FROM etapa e;

-- (b) Obtener las poblaciones que tienen la meta de alguna etapa, pero desde las que no se realiza ninguna salida. Resuélvela utilizando NOT IN

SELECT
  e.salida
FROM etapa e;



SELECT DISTINCT
  llegada
FROM etapa
WHERE llegada NOT IN (SELECT
    salida
  FROM etapa);


-- (l) Obtener el nombre de los puertos de montaña que tienen una altura superior a la altura media de todos los puertos
SELECT
  AVG(p.altura) media
FROM puerto p;

SELECT
  p.nompuerto
FROM puerto p
WHERE p.altura > (SELECT
    AVG(p.altura) media
  FROM puerto p);

-- (08) ¿Cuántos maillots diferentes ha llevado Induráin?
SELECT
  c.dorsal
FROM ciclista c
WHERE c.nombre LIKE 'Miguel Induráin';

SELECT DISTINCT
  código
FROM lleva
WHERE dorsal IN (SELECT
    c.dorsal
  FROM ciclista c
  WHERE c.nombre LIKE 'Miguel Induráin');

SELECT
  COUNT(*)
FROM (SELECT DISTINCT
    código
  FROM lleva
  WHERE dorsal IN (SELECT
      c.dorsal
    FROM ciclista c
    WHERE c.nombre LIKE 'Miguel Induráin')) c;

-- (a) Obtener los datos de las etapas que pasan por algún puerto de montaña y que tienen salida y llegada en la misma población

SELECT DISTINCT
  e.*
FROM etapa e
  JOIN puerto p
    ON e.numetapa = p.numetapa
WHERE salida = llegada;

-- (c) Obtener el nombre y el equipo de los ciclistas que han ganado alguna etapa llevando el maillot amarillo, mostrando también el número de etapa

SELECT
  m.código
FROM maillot m
WHERE m.color = 'amarillo';

SELECT
  c.nombre,
  c.nomequipo,
  l.numetapa
FROM ciclista c
  JOIN etapa e USING (dorsal)
  JOIN lleva l USING (numetapa, dorsal)
WHERE l.código = (SELECT
    m.código
  FROM maillot m
  WHERE m.color = 'amarillo');

-- (e) Obtener el número de las etapas que tienen algún puerto de montaña, indicando cuántos tiene cada una de ellas

SELECT
  p.numetapa,
  COUNT(*)
FROM puerto p
GROUP BY p.numetapa;

-- (g) Obtener el nombre y el equipo de los ciclistas que han llevado algún maillot o que han ganado algún puerto

SELECT DISTINCT
  c.nombre,
  c.nomequipo
FROM ciclista c
  JOIN lleva l USING (dorsal)
UNION
SELECT DISTINCT
  c.nombre,
  c.nomequipo
FROM ciclista c
  JOIN puerto p USING (dorsal);

-- (k) Obtener la edad media de los ciclistas que han ganado alguna etapa


SELECT DISTINCT
  e.dorsal
FROM etapa e;

SELECT
  AVG(c.edad)
FROM (SELECT DISTINCT
    e.dorsal
  FROM etapa e) c1
  JOIN ciclista c USING (dorsal);

-- (03) Cuántos puertos ha ganado Induráin llevando algún maillot
SELECT
  c.dorsal,
  c.nombre
FROM ciclista c
WHERE c.nombre = 'Miguel Induráin';

SELECT
  COUNT(*)
FROM (SELECT
    c.dorsal,
    c.nombre
  FROM ciclista c
  WHERE c.nombre = 'Miguel Induráin') c1
  JOIN puerto p USING (dorsal)
  JOIN lleva l USING (dorsal, numetapa);

-- (d) Obtener los datos de las etapas que no comienzan en la misma ciudad en que acaba la etapa anterior


SELECT
  e.numetapa,
  e.salida,
  e.llegada
FROM etapa e
WHERE e.salida <> (e.llegada);

SELECT
  e1.*
FROM etapa e1
  JOIN etapa e2
    ON e1.numetapa - 1 = e2.numetapa
    AND e2.llegada <> e1.salida;

-- (02) Cuántas etapas con puerto ha ganado Induráin llevando algún maillot

SELECT
  c.dorsal,
  c.nombre
FROM ciclista c
WHERE c.nombre = 'Miguel Induráin';

SELECT
  COUNT(*)
FROM (SELECT
    c.dorsal,
    c.nombre
  FROM ciclista c
  WHERE c.nombre = 'Miguel Induráin') c1
  JOIN etapa e
    ON c1.dorsal = e.dorsal
  JOIN puerto p USING (numetapa)
  JOIN lleva l
    ON e.numetapa = l.numetapa
    AND e.dorsal = l.dorsal;

-- (07) Qué maillots ha llevado Induráin en etapas con puerto cuya etapa haya ganado

SELECT
  p.numetapa,
  p.dorsal
FROM etapa e
  JOIN puerto p USING (numetapa);

SELECT
  c.dorsal
FROM ciclista c
WHERE nombre = 'miguel induráin';

SELECT
  l.código
FROM lleva l
  JOIN (SELECT
      p.numetapa,
      p.dorsal
    FROM etapa e
      JOIN puerto p USING (numetapa)) c1 USING (numetapa, dorsal)
  JOIN (SELECT
      c.dorsal
    FROM ciclista c
    WHERE nombre = 'miguel induráin') c2 USING (dorsal);

-- (m) Obtener las poblaciones de salida y de llegada de las etapas donde se encuentran los puertos con mayor pendiente

SELECT
  p.nompuerto,
  MAX(p.pendiente)
FROM puerto p;

SELECT
  p.numetapa,
  c1.nompuerto,
  c1.max
FROM puerto p
  JOIN (SELECT
      p.nompuerto,
      MAX(p.pendiente) max
    FROM puerto p) c1 USING (nompuerto);



SELECT
  e.salida,
  e.llegada

FROM etapa e
  JOIN (SELECT
      p.numetapa,
      c1.nompuerto,
      c1.max
    FROM puerto p
      JOIN (SELECT
          p.nompuerto,
          MAX(p.pendiente) max
        FROM puerto p) c1 USING (nompuerto)) c2 USING (numetapa);

-- (n) Obtener el dorsal y el nombre de los ciclistas que han ganado los puertos de mayor altura

SELECT
  MAX(p.altura)
FROM puerto p;

SELECT
  altura,
  dorsal
FROM puerto
WHERE altura = (SELECT
    MAX(p.altura)
  FROM puerto p);

SELECT
  c.dorsal,
  c.nombre

FROM ciclista c
  JOIN (SELECT
      altura,
      dorsal
    FROM puerto
    WHERE altura = (SELECT
        MAX(p.altura)
      FROM puerto p)) c1
  USING (dorsal);

-- (j) Obtener los números de las etapas que no tienen puertos de montaña

SELECT
  *
FROM puerto p
  LEFT JOIN etapa e
    ON p.numetapa = e.numetapa;

SELECT
  c1.numetapa
FROM (SELECT
    e.numetapa
  FROM puerto p
    RIGHT JOIN etapa e USING (numetapa)
  WHERE p.numetapa IS NULL) c1;

-- (b2) Obtener las poblaciones que tienen la meta de alguna etapa, pero desde las que no se realiza ninguna salida. Resuélvela con un producto externo
SELECT DISTINCT
  e.llegada
FROM etapa e
WHERE e.llegada NOT IN (SELECT
    e1.salida
  FROM etapa e1);

-- (09) Cuántas etapas con puerto ha ganado Induráin llevando algún maillot

SELECT
  e.numetapa,
  e.dorsal
FROM etapa e
  JOIN puerto p USING (numetapa);

SELECT
  c.dorsal
FROM ciclista c
WHERE c.nombre = 'Miguel Induráin';

SELECT
  COUNT(*)
FROM lleva l
  JOIN (SELECT
      e.numetapa,
      e.dorsal
    FROM etapa e
      JOIN puerto p USING (numetapa)) c1 USING (numetapa, dorsal)
  JOIN (SELECT
      c.dorsal
    FROM ciclista c
    WHERE c.nombre = 'Miguel Induráin') c2 USING (dorsal);

-- (f) Obtener el nombre y la edad de los ciclistas que han llevado dos o más maillots en una misma etapa
SELECT
  l.dorsal,
  e.numetapa,
  COUNT(l.código) cuenta
FROM lleva l
  JOIN etapa e USING (numetapa)
GROUP BY l.dorsal,
         e.numetapa
HAVING cuenta > 1;

SELECT
  c.nombre,
  c.edad
FROM ciclista c
  JOIN (SELECT
      l.dorsal,
      e.numetapa,
      COUNT(l.código) cuenta
    FROM lleva l
      JOIN etapa e USING (numetapa)
    GROUP BY l.dorsal,
             e.numetapa
    HAVING cuenta > 1) c1 USING (dorsal);

-- (01) En cuántas etapas ha llevado maillot Induráin con puerto y además las ha ganado


SELECT
  COUNT(*)
FROM lleva l
  JOIN (SELECT
      e.numetapa,
      e.dorsal
    FROM etapa e
      JOIN puerto p USING (numetapa)) c1 USING (numetapa, dorsal)
  JOIN (SELECT
      c.dorsal
    FROM ciclista c
    WHERE c.nombre = 'Miguel Induráin') c2 USING (dorsal);

-- (06) Código de los maillots que han sido llevados por más de un equipo

SELECT
  l.código,
  COUNT(DISTINCT c.nomequipo) cuenta
FROM ciclista c
  JOIN lleva l USING (dorsal)
GROUP BY l.código
HAVING cuenta > 1;

-- (o) Obtener los datos de las etapas cuyos puertos (todos) superan los 1300 metros de altura

SELECT
  *
FROM puerto
WHERE p.altura > 1300
GROUP BY e.numetapa;

-- c1
SELECT
  numetapa,
  COUNT(*) n
FROM puerto
GROUP BY numetapa;

-- c2
SELECT
  numetapa,
  COUNT(*) n
FROM puerto
WHERE altura > 1300
GROUP BY numetapa;


SELECT
  etapa.*
FROM (SELECT
    numetapa,
    COUNT(*) n
  FROM puerto
  GROUP BY numetapa) c1
  JOIN (SELECT
      numetapa,
      COUNT(*) n
    FROM puerto
    WHERE altura > 1300
    GROUP BY numetapa) c2 USING (numetapa, n)
  JOIN etapa USING (numetapa);

-- (i) Obtener el código y el color de aquellos maillots que sólo han sido llevados por ciclistas de un mismo equipo
SELECT
  c.dorsal,
  c.nomequipo,
  l.código
FROM ciclista c
  JOIN lleva l USING (dorsal);

SELECT
  c1.código,
  COUNT(DISTINCT c1.nomequipo) cuenta
FROM (SELECT
    c.dorsal,
    c.nomequipo,
    l.código
  FROM ciclista c
    JOIN lleva l USING (dorsal)) c1
GROUP BY c1.código
HAVING cuenta = 1;

SELECT
  m.código,
  m.color
FROM maillot m
  JOIN (SELECT
      c1.código,
      COUNT(DISTINCT c1.nomequipo) cuenta
    FROM (SELECT
        c.dorsal,
        c.nomequipo,
        l.código
      FROM ciclista c
        JOIN lleva l USING (dorsal)) c1
    GROUP BY c1.código
    HAVING cuenta = 1) c1
    ON c1.código = m.código;

-- (h) Obtener los datos de los ciclistas que han vestido todos los maillots (no necesariamente en la misma etapa);
SELECT
  l.dorsal,
  COUNT(DISTINCT l.código) cuenta
FROM lleva l
GROUP BY l.código;

SELECT
  *
FROM ciclista c
  JOIN (SELECT
      l.dorsal,
      COUNT(DISTINCT l.código) cuenta
    FROM lleva l
    GROUP BY l.código) c1 USING (dorsal)
HAVING c1.cuenta = 6;



-- (05) Nombre de los ciclistas que han ganado todos los puertos de alguna etapa. Ordena por nombre.
-- c1
SELECT
  p.numetapa,
  COUNT(*) n
FROM puerto p
GROUP BY p.numetapa;
-- c2

SELECT
  p.numetapa,
  p.dorsal,
  COUNT(*) n
FROM puerto p
GROUP BY p.dorsal,
         p.numetapa;

SELECT
  c2.dorsal
FROM (SELECT
    p.numetapa,
    COUNT(*) n
  FROM puerto p
  GROUP BY p.numetapa) c1
  JOIN (SELECT
      p.numetapa,
      p.dorsal,
      COUNT(*) n
    FROM puerto p
    GROUP BY p.dorsal,
             p.numetapa) c2 USING (numetapa, n);

SELECT
  c.nombre
FROM ciclista c
  JOIN (SELECT
      c2.dorsal
    FROM (SELECT
        p.numetapa,
        COUNT(*) n
      FROM puerto p
      GROUP BY p.numetapa) c1
      JOIN (SELECT
          p.numetapa,
          p.dorsal,
          COUNT(*) n
        FROM puerto p
        GROUP BY p.dorsal,
                 p.numetapa) c2 USING (numetapa, n)) c3 USING (dorsal)
ORDER BY c.nombre;
USE ciclitas;
-- (04) Cuántas etapas sin puerto ha ganado Induráin llevando algún maillot

SELECT
  COUNT(DISTINCT dorsal, numetapa)
FROM (SELECT
    dorsal
  FROM ciclista
  WHERE nombre = 'Miguel Induráin') c1
  JOIN (SELECT
      *
    FROM (SELECT
        numetapa,
        dorsal
      FROM etapa) c01
      LEFT JOIN (SELECT DISTINCT
          numetapa
        FROM puerto) c02 USING (numetapa)
    WHERE c02.numetapa IS NULL) c2 USING (dorsal)
  JOIN lleva USING (numetapa, dorsal);

-- (p) Obtener el nombre de los ciclistas que pertenecen a un equipo de más de cinco ciclistas
-- y que han ganado alguna etapa, indicando también cuántas etapas han ganado

SELECT
  nomequipo
FROM Ciclista
GROUP BY nomequipo
HAVING COUNT(DISTINCT dorsal) > 5;

SELECT
  c.dorsal,
  c.nombre,
  COUNT(*) AS num
FROM Ciclista c
  JOIN etapa e USING (dorsal)
WHERE c.dorsal = e.dorsal
AND c.nomequipo IN (SELECT
    nomequipo
  FROM ciclista
  GROUP BY nomequipo
  HAVING COUNT(DISTINCT dorsal) > 5)
GROUP BY c.dorsal,
         c.nombre
ORDER BY nombre;
USE jardineria;

-- (4.07.11) Clientes que no tiene asignado representante de ventas

SELECT
  *
FROM empleados e
  LEFT JOIN clientes c
    ON e.CodigoEmpleado = c.CodigoEmpleadoRepVentas
WHERE c.CodigoEmpleadoRepVentas NOT IN (e.CodigoEmpleado);

-- (4.07.14) Sacar los distintos estados por los que puede pasar un pedido

SELECT DISTINCT
  p.Estado
FROM pedidos p;

-- (4.10.41) ¿De qué ciudades son los clientes?
SELECT
DISTINCT
  c.Ciudad
FROM clientes c;

-- (4.07.06) Sacar el nombre de los clientes españoles
SELECT
  c.NombreCliente
FROM clientes c
WHERE c.Pais = 'españa'
OR c.Pais = 'spain';

-- (4.09.03) Obtener un listado con el nombre de los empleados junto con el nombre de sus jefes.

SELECT
  e0.nombre,
  e1.nombre
FROM empleados e0
  JOIN empleados e1
    ON e0.CodigoJefe = e1.CodigoEmpleado;


-- (4.08.01) Obtener el nombre del producto más caro
SELECT
  MAX(p.PrecioVenta)
FROM productos p;

SELECT
  p.Nombre
FROM productos p
WHERE p.PrecioVenta = (SELECT
    MAX(p.PrecioVenta)
  FROM productos p);

-- (4.07.07) Sacar cuántos clientes tiene cada país
SELECT
  c.Pais,
  COUNT(*)
FROM clientes c
GROUP BY c.Pais;

-- (4.07.10) Sacar el código de empleado y número de clientes al que atiende cada representante de ventas

SELECT
  c.CodigoEmpleadoRepVentas,
  COUNT(DISTINCT c.CodigoCliente) cuenta
FROM clientes c
  JOIN (SELECT
      e.CodigoEmpleado
    FROM empleados e) c1
    ON c.CodigoEmpleadoRepVentas = c1.CodigoEmpleado
GROUP BY c.CodigoEmpleadoRepVentas;

-- (4.10.11) Sacar los clientes que residan en la misma ciudad donde hay una oficina, indicando dónde está la oficina.

SELECT
  c.Ciudad,
  c.NombreCliente,
  o.LineaDireccion1,
  o.LineaDireccion2
FROM clientes c
  JOIN oficinas o USING (Ciudad)
WHERE c.Ciudad = o.Ciudad;

-- (4.10.17) Sacar cuántos pedidos tiene cada cliente en cada estado.
SELECT
  p.CodigoCliente,
  p.Estado,
  COUNT(*)
FROM pedidos p
GROUP BY p.CodigoCliente,
         p.Estado;

-- (4.09.04) Obtener el nombre de los clientes a los que no se les ha entregado a tiempo un pedido. Es decir, FechaEntrega mayor que FechaEsperada

SELECT
  p.CodigoCliente
FROM pedidos p
WHERE p.FechaEntrega > p.FechaEsperada;

SELECT DISTINCT
  c.NombreCliente
FROM clientes c
  JOIN (SELECT
      p.CodigoCliente
    FROM pedidos p
    WHERE p.FechaEntrega > p.FechaEsperada) c1
    ON c.CodigoCliente = c1.CodigoCliente
ORDER BY c.NombreCliente;



-- (4.10.36) Código del frutal que más margen de beneficios proporciona

SELECT
  p.CodigoProducto,
  (p.PrecioVenta - p.PrecioProveedor) ganancia
FROM productos p
WHERE p.Gama = 'frutales';

SELECT
  MAX(c1.ganancia) maximo
FROM (SELECT
    p.CodigoProducto,
    (p.PrecioVenta - p.PrecioProveedor) ganancia
  FROM productos p
  WHERE p.Gama = 'frutales') c1;

SELECT
  codigoproducto
FROM productos
WHERE gama = 'frutales'
AND precioventa - precioproveedor = (SELECT
    MAX(beneficio)
  FROM (SELECT
      codigoproducto,
      precioventa - precioproveedor beneficio
    FROM productos
    WHERE gama = 'frutales') c1);


-- (4.10.03) Sacar el nombre de los clientes que hayan hecho pedidos en 2008
SELECT 
       p.CodigoCliente
  FROM pedidos p
  WHERE year(p.FechaPedido)=2008 ;

SELECT DISTINCT
       c.NombreCliente
  FROM clientes c
  JOIN (SELECT DISTINCT
       p.CodigoCliente
  FROM pedidos p
  WHERE year(p.FechaPedido)=2008)c1 USING(CodigoCliente);

-- (4.07.06b) Muestra el nombre de cliente repetido, ¿puede ser esto posible?

SELECT c.NombreCliente ,COUNT(c.NombreCliente) cuenta
     FROM clientes c
  GROUP BY c.NombreCliente
HAVING cuenta>1;

-- (4.10.44) Con objeto de acortar distancias entre comerciales y sus clientes, obtener un listado de los códigos de los comerciales 
  -- que están atendiendo clientes de fuera de su ciudad existiendo una oficina en la misma ciudad que el cliente.
  -- La idea es enviarles una circular y puedan realizar las gestiones oportunas para que 
  -- sus clientes puedan ser atendidos por otro comercial más próximo geográficamente

SELECT 
      c.CodigoEmpleadoRepVentas,
      o.Ciudad
  FROM clientes c
  JOIN oficinas o USING (Ciudad);

SELECT DISTINCT
      e.CodigoEmpleado
       FROM empleados e
  JOIN oficinas o USING(CodigoOficina)
  JOIN (SELECT 
      c.CodigoEmpleadoRepVentas,
      o.Ciudad
  FROM clientes c
  JOIN oficinas o USING (Ciudad))c1 ON c1.CodigoEmpleadoRepVentas=e.CodigoEmpleado
WHERE c1.Ciudad<>o.Ciudad;

-- (4.08.04) Sacar el códido de el/los producto/s que más unidades tiene en stock y el que menos

SELECT MAX(p.CantidadEnStock) maximo FROM productos p;

SELECT MIN(p.CantidadEnStock) minimo FROM productos p;


SELECT DISTINCT p.CodigoProducto FROM productos p
  WHERE p.CantidadEnStock=(SELECT MAX(p.CantidadEnStock) maximo FROM productos p)
UNION
SELECT DISTINCT p.CodigoProducto FROM productos p
  WHERE p.CantidadEnStock=(SELECT MIN(p.CantidadEnStock) minimo FROM productos p);

-- (4.08.02) Obtener el nombre del producto del que más unidades se hayan vendido en un mismo pedido
 SELECT MAX(d.Cantidad) FROM detallepedidos d;

SELECT * FROM detallepedidos d
  WHERE d.Cantidad=(SELECT MAX(d.Cantidad) FROM detallepedidos d);

SELECT 
       p.Nombre
  FROM productos p 
  JOIN (SELECT * FROM detallepedidos d
  WHERE d.Cantidad=(SELECT MAX(d.Cantidad) FROM detallepedidos d))c1
  ON c1.CodigoProducto= p.CodigoProducto;

  -- (4.08.07) Facturación total del producto más caro
    SELECT MAX(d.PrecioUnidad) FROM detallepedidos d;

    SELECT * FROM detallepedidos d
      WHERE d.PrecioUnidad=(SELECT MAX(d.PrecioUnidad) FROM detallepedidos d);


    SELECT SUM(d.Cantidad*d.PrecioUnidad) FROM (
      SELECT DISTINCT d.CodigoProducto FROM detallepedidos d
      WHERE d.PrecioUnidad=(SELECT MAX(d.PrecioUnidad) FROM detallepedidos d))c1
    JOIN detallepedidos d USING(CodigoProducto);

    -- (4.10.02) Sacar el listado con los nombres de los clientes y el total pagado por cada uno de ellos.
      

      SELECT NombreCliente,Pagos FROM (
SELECT CodigoCliente,SUM(Cantidad) Pagos
FROM pagos GROUP BY CodigoCliente
) c1 JOIN clientes USING(CodigoCliente);

-- (4.10.13) Sacar el número de clientes que tiene asignado cada representante de ventas mostrando su nombre.
SELECT CodigoEmpleadoRepVentas CodigoEmpleado,COUNT(*) n
FROM clientes GROUP BY CodigoEmpleadoRepVentas;

  SELECT Nombre,n FROM (
SELECT CodigoEmpleadoRepVentas CodigoEmpleado,COUNT(*) n
FROM clientes GROUP BY CodigoEmpleadoRepVentas
) c1 JOIN empleados USING(CodigoEmpleado);



 -- (18) Realizar una consulta en la que se muestre por cada código hospital el nombre de las especialidades que tiene. Ordenar por código de hospital y especialidad



SELECT DISTINCT h.cod_hospital,
       m.especialidad 
  FROM hospitales h
  JOIN medicos m ON h.cod_hospital = m.cod_hospital
ORDER BY h.cod_hospital, m.especialidad;

-- (4) Visualizar el nombre de los empleados vendedores del departamento VENTAS (Nombre del departamento=VENTAS, oficio=VENDEDOR).
  SELECT 
         e.apellido
          FROM depart d
    JOIN emple e USING (dept_no)
    WHERE d.dnombre='ventas' AND e.oficio='vendedor';

-- (6) Visualizar los oficios de los empleados del departamento VENTAS.
  SELECT DISTINCT
         e.oficio
    FROM emple e
    JOIN depart d USING(dept_no)
    WHERE d.dnombre='ventas';

-- (5) Visualizar el número de vendedores del departamento VENTAS (utilizar la función COUNT sobre la consulta (4)).

SELECT 
         COUNT(*)
          FROM depart d
    JOIN emple e USING (dept_no)
    WHERE d.dnombre='ventas' AND e.oficio='vendedor';

-- (1) Visualizar el número de empleados de cada departamento. Utilizar GROUP BY para agrupar por departamento.

  SELECT e.dept_no,COUNT(*) FROM emple e
    GROUP BY e.dept_no;
  
-- (10) Para cada oficio obtener la suma de salarios.
  SELECT  e.oficio,SUM(e.salario) FROM emple e
    GROUP BY e.oficio;

  -- (15) Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades.
SELECT h.estanteria ,SUM(h.unidades)
  FROM herramientas h
  GROUP BY h.estanteria;

-- (19) Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos (tendrás que partir de la consulta anterior y utilizar GROUP BY).



SELECT h.cod_hospital, m.especialidad, COUNT(DISTINCT m.dni) nmedicos FROM hospitales h
  JOIN medicos m ON h.cod_hospital = m.cod_hospital
  GROUP BY h.cod_hospital, m.especialidad;

-- (3) Hallar la media de los salarios de cada código de departamento con empleados (utilizar la función AVG y GROUP BY).

  SELECT e.dept_no,AVG(e.salario) m FROM emple e
    GROUP BY e.dept_no;


-- (13) Mostrar el número de oficios distintos de cada departamento.
  SELECT e.dept_no ,COUNT(DISTINCT e.oficio) n FROM emple e
    GROUP BY e.dept_no;





SELECT * FROM pedido;