﻿-- 1- Visualizar el número de empleados de cada departamento. Utilizar GROUP BY para agrupar por departamento.

SELECT e.dept_no,COUNT(DISTINCT e.emp_no) numemplepordepart FROM emple e
  GROUP BY e.dept_no;

-- 2. Visualizar los departamentos con más de 5 empleados. Utilizar GROUP BY para agrupar por departamento y HAVING para establecer la condición sobre los grupos.

SELECT e.dept_no,COUNT(DISTINCT e.emp_no) numemplepordepart FROM emple e
  GROUP BY e.dept_no
HAVING numemplepordepart>=5;

-- 3. Hallar la media de los salarios de cada departamento (utilizar la función avg y GROUP BY)
  SELECT  e.dept_no,AVG(DISTINCT e.salario) FROM emple e
    GROUP BY e.dept_no;

  -- 4. Visualizar el nombre de los empleados vendedores del departamento ʻVENTASʼ (Nombre del departamento=ʼVENTASʼ,oficio=ʼVENDEDORʼ).
    SELECT 
           e.apellido
          FROM emple e
      JOIN depart d USING(dept_no)
      WHERE d.dnombre='ventas' AND e.oficio='vendedor';

-- 5. Visualizar el número de vendedores del departamento ʻVENTASʼ (utilizar la función COUNT sobre la consulta anterior).

        SELECT COUNT(DISTINCT e.apellido)
           
          FROM emple e
      JOIN depart d USING(dept_no)
      WHERE d.dnombre='ventas' AND e.oficio='vendedor';

-- 6. Visualizar los oficios de los empleados del departamento ʻVENTASʼ.

 SELECT 
        e.oficio
          FROM emple e
      JOIN depart d USING(dept_no)
      WHERE d.dnombre='ventas' ;

-- 7 A partir de la tabla EMPLE, visualizar el número de empleados de cada departamento cuyo oficio sea ʻEMPLEADOʼ (utilizar GROUP BY para agrupar por departamento. En la cláusula WHERE habrá que indicar que el oficio es ʻEMPLEADOʼ).

SELECT  e.dept_no,COUNT(DISTINCT e.emp_no) FROM emple e
   WHERE oficio='empleado'
  GROUP BY e.dept_no;

-- 8. Visualizar el departamento con más empleados
SELECT e.dept_no,e.oficio, COUNT(DISTINCT e.emp_no) FROM emple e
WHERE e.oficio='empleado' 
GROUP BY e.dept_no;

SELECT c1.dept_no,MAX(DISTINCT c1.cuenta) max FROM (SELECT e.dept_no,e.oficio, COUNT(DISTINCT e.emp_no) cuenta FROM emple e
WHERE e.oficio='empleado' 
GROUP BY e.dept_no) c1;

-- 9. Mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados.
SELECT e.dept_no,SUM(e.salario) sumsueldo FROM emple e 
  GROUP BY e.dept_no;

SELECT AVG(e.salario) FROM emple e
GROUP BY e.dept_no;

SELECT * FROM (
  SELECT e.dept_no,SUM(e.salario) sumsueldo FROM emple e 
  GROUP BY e.dept_no) c
  WHERE c.sumsueldo>(SELECT AVG(e.salario) FROM emple e);

-- 10. Para cada oficio obtener la suma de salarios.
SELECT e.oficio,SUM(e.salario) s FROM emple e
  GROUP BY e.oficio;

-- 11. Visualizar la suma de salarios de cada oficio del departamento ʻVENTASʼ
  SELECT e.oficio,SUM(e.salario) salventas FROM emple e
    JOIN depart d USING(dept_no)
    WHERE d.dnombre='ventas'
  GROUP BY e.oficio ;

-- 12. Visualizar el número de departamento que tenga más empleados cuyo oficio sea empleado.
SELECT e.dept_no,MAX(e.dept_no) cuenta FROM emple e
  WHERE e.oficio='empleado';

-- 13. Mostrar el número de oficios distintos de cada departamento.
  SELECT e.dept_no,COUNT(DISTINCT e.oficio) numoficio FROM emple e
    GROUP BY e.dept_no;

-- 14. Mostrar los departamentos que tengan más de dos personas trabajando en la misma profesión
  SELECT  coe.dept_no,coe.oficio
         FROM copy_1_of_emple coe
   GROUP BY coe.oficio, coe.dept_no 
  HAVING  COUNT(coe.emp_no) >2;

  -- 15. Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades.
SELECT h.estanteria,SUM(h.unidades) FROM herramientas h
  GROUP BY h.estanteria;

-- 16. Visualizar la estantería con más unidades de la tabla HERRAMIENTAS. (con totales y sin totales)
   SELECT c.estanteria,MAX(c.total) FROM (
    SELECT h.estanteria,SUM(h.unidades) total 
      FROM herramientas h
      GROUP BY h.estanteria) c;

-- 17. Mostrar el número de médicos que pertenecen a cada hospital, ordenado por número descendente de hospital.
SELECT m.cod_hospital,COUNT(m.dni) cuentamedicos FROM medicos m
  GROUP BY m.cod_hospital;

SELECT 
       h.nombre,
        ch.cuentamedicos 
  FROM hospitales h
  JOIN (
    SELECT m.cod_hospital,COUNT(m.dni) cuentamedicos 
      FROM medicos m
  GROUP BY m.cod_hospital)ch USING (cod_hospital)
GROUP BY h.nombre ORDER BY h.nombre DESC;

-- 18. Realizar una consulta en la que se muestre por cada hospital el nombre de las especialidades que tiene.
SELECT DISTINCT
       h.nombre,
      m.especialidad 
  FROM hospitales h
  JOIN medicos m USING(cod_hospital);

-- 19. Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos (tendrás que partir de la consulta anterior y utilizar GROUP BY).
SELECT c19.*,COUNT(c19.cod_hospital) 
  FROM (
    SELECT DISTINCT
      h.cod_hospital,
       h.nombre,
      m.especialidad 
      FROM hospitales h
       JOIN medicos m USING(cod_hospital)) c19
GROUP BY c19.nombre,c19.especialidad;

-- 20. Obtener por cada hospital el número de empleados.
SELECT m.cod_hospital,COUNT(dni)nummedicoporhospital FROM medicos m
  GROUP BY m.cod_hospital;

-- 21. Obtener por cada especialidad el número de trabajadores.
  SELECT m.especialidad,COUNT(m.dni) FROM medicos m
    GROUP BY m.especialidad;

  -- 22. Visualizar la especialidad que tenga más médicos.
SELECT MAX(c22.cuenta) maxcuenta
  FROM ( SELECT m.especialidad,COUNT(m.dni) cuenta FROM medicos m
    GROUP BY m.especialidad)c22;

SELECT m.especialidad,COUNT(m.dni) cuenta FROM medicos m
    GROUP BY m.especialidad HAVING cuenta=(SELECT MAX(c22.cuenta) maxcuenta
  FROM ( SELECT m.especialidad,COUNT(m.dni) cuenta FROM medicos m
    GROUP BY m.especialidad)c22);


-- 23. ¿Cuál es el nombre del hospital que tiene mayor número de plazas?
  SELECT h.nombre,MAX(h.num_plazas) FROM hospitales h;


-- 24. Visualizar las diferentes estanterías de la tabla HERRAMIENTAS ordenados descendentemente por estantería.
SELECT * FROM herramientas h
   ORDER BY h.estanteria DESC;

-- 25. Averiguar cuántas unidades tiene cada estantería.
 SELECT h.descripcion,h.estanteria,SUM(h.unidades) unidades 
  FROM herramientas h
  GROUP BY h.estanteria;

-- 26. Visualizar las estanterías que tengan más de 15 unidades
SELECT * FROM  (
  SELECT h.descripcion,h.estanteria,SUM(h.unidades) unidades 
  FROM herramientas h
  GROUP BY h.estanteria) c 
  WHERE c.unidades>=15;

-- 27. ¿Cuál es la estantería que tiene más unidades?
CREATE OR REPLACE VIEW c27 AS 
    SELECT h.descripcion,h.estanteria,SUM(h.unidades) unidades 
  FROM herramientas h
  GROUP BY h.estanteria;

SELECT c.estanteria, MAX(c.unidades) FROM c27 c;

-- 28. A partir de las tablas EMPLE y DEPART mostrar los datos del departamento que no tiene ningún empleado.
SELECT 
       e.dept_no,
       d.dept_no,
       d.dnombre,
       d.loc 
  FROM  emple e
  LEFT JOIN depart d USING(dept_no)
  WHERE e.oficio='empleado' IS NULL;

-- 29. Mostrar el número de empleados de cada departamento. En la salida se debe mostrar también los departamentos que no tienen ningún empleado.
  SELECT e.dept_no,COUNT(e.emp_no) FROM emple e
   RIGHT JOIN depart d USING (dept_no)
    GROUP BY e.dept_no ;

-- 30. Obtener la suma de salarios de cada departamento, mostrando las columnas DEPT_NO, SUMA DE SALARIOS y DNOMBRE.En el resultado también se deben mostrar los departamentos que no tienen asignados empleados.
SELECT e.dept_no, SUM(e.salario) sumasalarios,d.dnombre  FROM emple e
  RIGHT  JOIN depart d USING(dept_no)
GROUP BY d.dept_no;

-- 31. Utilizar la función IFNULL en la consulta anterior para que en el caso de que un departamento no tenga empleados, aparezca como suma de salarios el valor 0.
SELECT d.dept_no, IFNULL(SUM(e.salario),0) sumasalarios,d.dnombre  FROM emple e
  RIGHT JOIN depart d USING(dept_no)
GROUP BY d.dept_no;

-- 32. Obtener el número de médicos que pertenecen a cada hospital, mostrando las columnas COD_HOSPITAL, NOMBRE y NÚMERO DE MÉDICOS. En el resultado deben aparecer también los datos de los hospitales que no tienen médicos.
SELECT h.cod_hospital,h.nombre, COUNT(m.dni) nummedicos FROM  medicos m
  RIGHT JOIN hospitales h USING(cod_hospital) 
  GROUP BY h.nombre;




 





  

  


     










