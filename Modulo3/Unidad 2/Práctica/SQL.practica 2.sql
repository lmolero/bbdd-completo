﻿/* ejercicio 1. practica2 */
SELECT COUNT(DISTINCT nombre) FROM ciudad c;

/* ejercicio 2. practica2 */

SELECT AVG(c.población) FROM ciudad c;

SELECT c.nombre
  FROM ciudad c
  WHERE c.población>(SELECT AVG(c.población) FROM ciudad c);

/* ejercicio 3. practica2 */

SELECT AVG(c.población) 
FROM ciudad c;

SELECT c.nombre
  FROM ciudad c
  WHERE c.población<(SELECT AVG(c.población) FROM ciudad c);

/* ejercicio 4. practica2 */

SELECT MAX(c.población) 
FROM ciudad c;

SELECT c.nombre 
  FROM ciudad c
  WHERE c.población=(SELECT MAX(c.población) 
FROM ciudad c);

/* ejercicio 5. practica2 */

SELECT MIN(c.población) 
FROM ciudad c;

SELECT c.nombre 
  FROM ciudad c
  WHERE c.población=(SELECT MIN(c.población) 
FROM ciudad c);

/* ejercicio 6. practica2 */
SELECT AVG(c.población) FROM ciudad c;

SELECT c.nombre
  FROM ciudad c
  WHERE c.población>(SELECT AVG(c.población) FROM ciudad c);

CREATE OR REPLACE VIEW c2_6 AS
SELECT *
  FROM ciudad c
  WHERE c.población>(SELECT AVG(c.población) FROM ciudad c);

SELECT COUNT(*) FROM c2_6 c;

/* ejercicio 7. practica2 */

  SELECT c.nombre ,SUM(DISTINCT c.población)FROM ciudad c
  GROUP BY c.nombre;

/* ejercicio 8. practica2 */
  SELECT t.compañia,COUNT(t.persona) numtrabajadores FROM trabaja t
    GROUP BY t.compañia;

CREATE OR REPLACE VIEW c2_8 AS 
  SELECT t.persona,t.compañia,COUNT(t.persona) numtrabajadores FROM trabaja t
    GROUP BY t.compañia;

/* ejercicio 9. practica2 */

SELECT MAX(DISTINCT numtrabajadores) 
FROM c2_8 c;

SELECT compañia FROM c2_8 c
  WHERE numtrabajadores=(SELECT MAX(DISTINCT numtrabajadores) 
FROM c2_8 c);

/* ejercicio 10. practica2 */

SELECT t.compañia,AVG(t.salario) FROM trabaja t
  GROUP BY t.compañia;

/* ejercicio 11. practica2 */
  SELECT
         p.nombre,
         c.población
    FROM ciudad c
    JOIN persona p ON c.nombre=p.ciudad;

/* ejercicio 12. practica2 */

SELECT   p.nombre, 
         p.calle,
         c.población
    FROM ciudad c
    JOIN persona p ON c.nombre=p.ciudad;

/* ejercicio 13. practica2 */

  SELECT p.nombre,
        p.ciudad AS cpersona,
         c.ciudad AS ccompañia 
    FROM persona p
    JOIN trabaja t ON p.nombre=t.persona
    JOIN compania c ON c.nombre=t.compañia;
  

/* ejercicio 14. practica2 */

  SELECT * FROM persona p,trabaja,compania c
    WHERE p.nombre=persona
    AND compañia=c.nombre
    AND c.ciudad=p.ciudad
    ORDER BY p.nombre;

/* ejercicio 15. practica2 */

  SELECT s.supervisor,
         s.persona FROM supervisa s;

  /* ejercicio 16. practica2 */

 SELECT s.supervisor,
         p.nombre,
          p.ciudad 
    FROM supervisa s
    JOIN persona p ON p.nombre=s.persona;

 /* ejercicio 17. practica2 */

  SELECT COUNT(DISTINCT c.nombre) FROM compania c;

/* ejercicio 18. practica2 */

  SELECT COUNT(DISTINCT p.ciudad) FROM persona p;

/* ejercicio 19. practica2 */

  SELECT t.persona
    FROM trabaja t
    WHERE t.compañia='fagor';

/* ejercicio 20. practica2 */
  SELECT t.persona  
    FROM trabaja t
    WHERE t.compañia NOT LIKE 'fagor';

/* ejercicio 21. practica2 */

SELECT COUNT(DISTINCT t.persona)
    FROM trabaja t
    WHERE t.compañia='indra';

/* ejercicio 22. practica2 */

  SELECT t.persona
    FROM trabaja t
    WHERE t.compañia='fagor' OR t.compañia= 'indra';

/* ejercicio 23. practica2 */

  SELECT  p.ciudad, 
         t.salario, 
         t.persona,
         t.compañia  
         FROM trabaja t
    JOIN persona p ON p.nombre=t.persona ORDER BY p.nombre, t.salario DESC;