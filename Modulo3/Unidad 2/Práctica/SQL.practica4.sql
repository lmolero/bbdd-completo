﻿-- 1 Averigua el DNI de todos los clientes
  SELECT c.dni
    FROM cliente c;

-- 2 Consulta todos los datos de todos los programas.
SELECT * FROM programa p;

-- 3 Obtén un listado con los nombres de todos los programas.
SELECT
       p.nombre
 FROM   programa p;

-- 4 Genera una lista con todos los comercios
  SELECT * FROM  comercio c;

-- 5 Genera una lista de las ciudades con establecimientos donde se venden programas, sin que aparezcan valores duplicados (utiliza DISTINCT).

  SELECT DISTINCT
         c.ciudad 
    FROM programa p
    JOIN distribuye d USING (codigo)
    JOIN comercio c USING (cif);

 -- 6 Obtén una lista con los nombres de programas, sin que aparezcan valores duplicados (utiliza DISTINCT).
SELECT DISTINCT p.nombre
  FROM  programa p;

-- 7 Obtén el DNI más 4 de todos los clientes.
SELECT dni+4 FROM cliente c;

-- Haz un listado con los códigos de los programas multiplicados por 7.
SELECT p.codigo*7 FROM programa p;

-- 9 ¿Cuáles son los programas cuyo código es inferior o igual a 10?
SELECT DISTINCT
       p.nombre
  FROM programa p
  WHERE p.codigo<=10;

-- 10 ¿Cuál es el programa cuyo código es 11?

  SELECT * FROM programa p
    WHERE p.codigo=11;

-- 11 ¿Qué fabricantes son de Estados Unidos?
  SELECT * FROM fabricante f
    WHERE f.pais='estados unidos';

-- 12 ¿Cuáles son los fabricantes no españoles? Utilizar el operador IN.
SELECT * FROM fabricante f
  WHERE f.pais  NOT IN ('españa');

-- 13 Obtén un listado con los códigos de las distintas versiones de Windows.
SELECT p.codigo
  FROM programa p
  WHERE p.nombre='windows';

-- 14 ¿En qué ciudades comercializa programas El Corte Inglés?
  SELECT DISTINCT
         c.ciudad
    FROM distribuye d
    JOIN programa p USING(codigo)
    JOIN comercio c USING(cif)
    WHERE c.nombre='El Corte Inglés';

  -- 15 ¿Qué otros comercios hay, además de El Corte Inglés? Utilizar el operador IN
    SELECT 
           c.nombre
      FROM comercio c
      WHERE c.nombre NOT IN ( 'El Corte Inglés');

-- 16 Genera una lista con los códigos de las distintas versiones de Windows y Access. Utilizar el operador IN.
  SELECT * FROM programa p
    WHERE p.nombre IN ('windows','access');

-- 17 Obtén un listado que incluya los nombres de los clientes de edades comprendidas entre 10 y 25 y de los mayores de 50 años. Da una solución con BETWEEN y otra sin BETWEEN.
  SELECT * FROM cliente c
    WHERE c.edad BETWEEN 10 AND 25 OR c.edad>=50;

  SELECT * FROM  cliente c
    WHERE c.edad>=50 OR c.edad>=10 AND c.edad<=25;

  -- 18 Saca un listado con los comercios de Sevilla y Madrid. No se admiten valores duplicados
    SELECT DISTINCT 
                    c.nombre
      FROM comercio c
      WHERE c.ciudad='sevilla' or c.ciudad='madrid' ;

-- 19 ¿Qué clientes terminan su nombre en la letra “o”?
  SELECT c.dni,
         c.nombre
    FROM cliente c
    WHERE c.nombre LIKE '%o';

-- 20 ¿Qué clientes terminan su nombre en la letra “o” y, además, son mayores de 30 años?
 SELECT 
         c.nombre
    FROM cliente c
    WHERE c.nombre LIKE '%o' AND c.edad>30;ç

-- 21 Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A o por una W.
SELECT * FROM programa p
  WHERE p.version LIKE '%i' OR p.nombre LIKE 'a%' OR p.nombre LIKE 'w%';

-- 22 Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A y termine por una S.

SELECT * FROM programa p
  WHERE p.version LIKE '%i' OR p.nombre LIKE 'a%' OR p.nombre LIKE '%s';

-- 23 Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, y cuyo nombre no comience por una A
SELECT * FROM programa p
  WHERE p.version LIKE '%i' AND  p.nombre NOT  LIKE 'a%';

-- 24 Obtén una lista de empresas por orden alfabético ascendente
  SELECT 
         f.nombre
  FROM fabricante f 
  ORDER BY f.nombre;

  -- 25 Genera un listado de empresas por orden alfabético descendente.
  SELECT 
         f.nombre
  FROM fabricante f 
  ORDER BY f.nombre DESC;

  -- 26 Obtén un listado de programas por orden de versión
    SELECT * FROM programa p
      ORDER BY p.version;

-- 27 Genera un listado de los programas que desarrolla Oracle.
SELECT 
       p.nombre,
       p.version
  FROM programa p
  JOIN desarrolla d USING (codigo)
  JOIN fabricante f USING (id_fab)
WHERE f.nombre='oracle';

-- 28 ¿Qué comercios distribuyen Windows?
SELECT * FROM programa p
    JOIN distribuye d USING(codigo)
    WHERE p.nombre='WINDOWS';

  SELECT  c.nombre 
          FROM (SELECT * FROM programa p
    JOIN distribuye d USING(codigo)
    WHERE p.nombre='WINDOWS') c28 JOIN comercio c USING(cif);

  -- 29 Genera un listado de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid.
 SELECT * FROM programa p
    JOIN distribuye d USING(codigo);

  SELECT DISTINCT
         c29.nombre,
        c29.cantidad
          FROM ( SELECT * FROM programa p
    JOIN distribuye d USING(codigo)) c29 JOIN comercio c USING(cif)
  WHERE c.nombre='el corte inglés' AND c.ciudad='madrid';

  -- 30 ¿Qué fabricante ha desarrollado Freddy Hardest?
     SELECT 
    p.codigo
    FROM programa p
    WHERE p.nombre='Freddy Hardest';

  SELECT 
         f.nombre
          FROM desarrolla d JOIN(SELECT 
    p.codigo
    FROM programa p
    WHERE p.nombre='Freddy Hardest') c1 USING(codigo)
  JOIN fabricante f USING(id_fab);

  -- 31 Selecciona el nombre de los programas que se registran por Internet
      SELECT 
         p.nombre
        FROM registra r
    JOIN programa p USING(codigo)
    WHERE r.medio='internet';

-- 32 Selecciona el nombre de las personas que se registran por Internet
    SELECT 
         c.nombre
    FROM cliente c
    JOIN registra r USING(dni)
    WHERE r.medio='internet' ORDER BY c.nombre;

-- 33 ¿Qué medios ha utilizado para registrarse Pepe Pérez?
    SELECT 
         r.medio FROM registra r
    JOIN cliente USING(dni)
  WHERE nombre='Pepe Pérez';

-- 34 ¿Qué usuarios han optado por Internet como medio de registro?
  SELECT 
         c.nombre
    FROM registra r
    JOIN cliente c USING(dni)
    WHERE r.medio='internet';

  -- 35 ¿Qué programas han recibido registros por tarjeta postal?
SELECT 
       p.nombre
  FROM programa p
  JOIN registra r USING(codigo)
  WHERE r.medio='tarjeta postal';

-- 36 ¿En qué localidades se han vendido productos que se han registrado por Internet?
    SELECT 
         c.ciudad
    FROM comercio c
    JOIN registra r USING(cif)
    WHERE r.medio='internet';

-- 37 Obtén un listado de los nombres de las personas que se han registrado por Internet, junto al nombre de los programas para los que ha efectuado el registro.
  SELECT   c.nombre
       FROM cliente c
        JOIN registra r USING(dni)
        JOIN programa p USING(codigo)
    WHERE r.medio='internet' ;

  -- 38 Genera un listado en el que aparezca cada cliente junto al programa que ha registrado, el medio con el que lo ha hecho y el comercio en el que lo ha adquirido.

 SELECT  
         c.nombre,
          p.nombre,
         r.medio,
         c1.nombre
       FROM cliente c
        JOIN registra r USING(dni)
        JOIN programa p USING(codigo)
        JOIN comercio c1 USING (cif) ;

-- 39 Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle
  SELECT * FROM fabricante f
  JOIN desarrolla d USING(id_fab)
WHERE f.nombre='oracle';

SELECT * FROM (SELECT * FROM fabricante f
  JOIN desarrolla d USING(id_fab)
WHERE f.nombre='oracle') c39 
JOIN distribuye d USING(codigo);

SELECT DISTINCT
               c.ciudad FROM (
  SELECT * FROM (
     SELECT * FROM fabricante f
        JOIN desarrolla d USING(id_fab)
        WHERE f.nombre='oracle'
    ) c39 JOIN distribuye d USING(codigo)
  ) c39_1 JOIN comercio c USING(cif);

-- 40 Obtén el nombre de los usuarios que han registrado Access XP
 SELECT * FROM  registra r
  JOIN programa p ON r.codigo = p.codigo
WHERE p.nombre='Access' AND p.version='XP';

SELECT * FROM (
   SELECT * FROM  registra r
  JOIN programa p ON r.codigo = p.codigo
WHERE p.nombre='Access' AND p.version='XP'
  ) c JOIN cliente c1 USING(dni);

-- 41 Nombre de aquellos fabricantes cuyo país es el mismo que ʻOracleʼ. (Subconsulta).
SELECT 
         f.nombre
    FROM fabricante f
    WHERE f.pais='Estados Unidos' AND f.nombre<>'oracle';

-- 42 Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez. (Subconsulta).
    SELECT
           c.nombre
      FROM cliente c
      WHERE edad=45 AND c.nombre<>'pepe pérez';

-- 43 Genera un listado con los comercios que tienen su sede en la misma ciudad que tiene el comercio ʻFNACʼ. (Subconsulta).
 SELECT * FROM comercio c
    WHERE c.nombre='fnac'  ;

  SELECT
         c.nombre
       FROM ( SELECT c.cif,c.nombre,c.ciudad FROM comercio c
    WHERE c.nombre='fnac') c1
    JOIN comercio c USING(ciudad)
    WHERE c.ciudad<>c1.ciudad;

  -- 44 Nombre de aquellos clientes que han registrado un producto de la misma forma que el cliente ʻPepe Pérezʼ. (Subconsulta).
    SELECT r.codigo FROM registra r
      JOIN cliente c USING(dni)
      WHERE c.nombre='pepe pérez';
    
    SELECT *FROM registra r
      WHERE r.codigo IN (SELECT DISTINCT r.codigo FROM registra r
      JOIN cliente c USING(dni)
      WHERE c.nombre='pepe pérez');

    CREATE OR REPLACE VIEW igualpepe AS
SELECT * FROM registra r
      WHERE r.codigo IN (SELECT DISTINCT r.codigo FROM registra r
      JOIN cliente c USING(dni)
      WHERE c.nombre='pepe pérez');

    SELECT DISTINCT
           c.nombre 
      FROM cliente c
      JOIN igualpepe i USING(dni)
    WHERE c.nombre NOT IN ('pepe peréz');

-- 45 Obtener el número de programas que hay en la tabla programas.
SELECT COUNT(*) FROM programa p;

-- 46 Calcula el número de clientes cuya edad es mayor de 40 años
  SELECT COUNT(c.dni) FROM  cliente c
    WHERE c.edad>40;

-- 47 Calcula el número de productos que ha vendido el establecimiento cuyo CIF es 1.
SELECT COUNT(DISTINCT d.cif) FROM distribuye d
  WHERE d.cif=1;

-- 48 Calcula la media de programas que se venden cuyo código es 7.
SELECT  avg(d.cantidad) FROM distribuye d
  WHERE d.codigo=7;

-- 49 Calcula la mínima cantidad de programas de código 7 que se ha vendido

  SELECT  MIN(d.cantidad) FROM distribuye d
  WHERE d.codigo=7;

  -- 50 Calcula la máxima cantidad de programas de código 7 que se ha vendido

 SELECT  MAX(d.cantidad) FROM distribuye d
  WHERE d.codigo=7;

-- 51 ¿En cuántos establecimientos se vende el programa cuyo código es 7?
  SELECT COUNT(d.cif) FROM distribuye d
    WHERE d.codigo=7;

  SELECT
  COUNT(DISTINCT comercio.cif)
FROM distribuye comercio
  JOIN distribuye USING (cif)
WHERE comercio.codigo = 7;

-- 52 Calcular el número de registros que se han realizado por Internet.

SELECT
  COUNT(*)
FROM registra r
WHERE r.medio = 'internet';

-- 53 Obtener el número total de programas que se han vendido en ʻSevillaʼ.
 SELECT * FROM distribuye d
    JOIN programa p USING(codigo);

  SELECT SUM(c53.cantidad) numprogramas FROM (
    SELECT * FROM distribuye d
    JOIN programa p USING(codigo)
    ) c53 JOIN comercio c USING(cif)
  WHERE c.ciudad='sevilla';

  -- 54 Calcular el número total de programas que han desarrollado los fabricantes cuyo país es ʻEstados Unidosʼ
     SELECT * FROM fabricante f
      JOIN desarrolla d USING(id_fab)
    WHERE f.pais='estados unidos';

-- 55 Visualiza el nombre de todos los clientes en mayúscula. En el resultado de la consulta debe aparecer también la longitud de la cadena nombre.
  SELECT UPPER(c.nombre), 
  CHAR_LENGTH(c.nombre) longitudname
  FROM cliente c;

-- 56 Con una consulta concatena los campos nombre y versión de la tabla PROGRAMA
  SELECT CONCAT(p.nombre,p.version) 
  FROM programa p;


    













    
