﻿-- practica 1
-- practica 1.1
  SELECT * 
  FROM emple e;

-- practica 1.2

  SELECT *
  FROM depart d;

  -- practica 1.3
    SELECT 
           e.apellido,
           e.oficio
        FROM emple e;

    -- practica 1.4

      SELECT d.dept_no,
             d.loc
        FROM depart d;

  -- practica 1.5

      SELECT *
        FROM depart d;

  -- practica 1.6

      SELECT COUNT(*) 
      FROM emple e;

  -- practica 1.7
   SELECT * 
  FROM emple e
  ORDER BY e.apellido ASC;

  -- practica 1.8
SELECT * 
  FROM emple e
  ORDER BY e.apellido DESC;

 -- practica 1.9

SELECT COUNT(*) 
  FROM depart d;

 -- practica 1.10
SELECT COUNT(*) 
FROM emple e;

SELECT COUNT(*) 
FROM depart d;

SELECT (SELECT COUNT(*) 
FROM emple e)+(SELECT COUNT(*) 
FROM depart d) ;

-- practica 1.11
SELECT *
  FROM emple e
  ORDER BY e.dept_no DESC;

-- practica 1.12
SELECT *
  FROM emple e
  ORDER BY e.dept_no DESC, e.oficio ASC;

-- practica 1.13

SELECT *
  FROM emple e
  ORDER BY e.dept_no DESC, e.apellido ASC;

-- practica 1.14
  SELECT e.emp_no
    FROM emple e
    WHERE e.salario>2000;

  -- practica 1.15
 SELECT e.emp_no,
        e.apellido
    FROM emple e
    WHERE e.salario<2000;

  -- practica 1.16
 SELECT *
    FROM emple e
    WHERE e.salario BETWEEN 1500 AND 2500;

  -- practica 1.17
SELECT * 
  FROM emple e
  WHERE e.oficio='analista';

  -- practica 1.18
SELECT * 
  FROM emple e
  WHERE e.oficio='analista' AND e.salario>2000;

-- practica 1.19
SELECT 
       e.apellido,
       e.oficio
  FROM emple e
  GROUP BY e.dept_no=20;

-- practica 1.20
SELECT * FROM emple e
  WHERE e.oficio='vendedor';

CREATE OR REPLACE VIEW c1e AS
SELECT * FROM emple e
  WHERE e.oficio='vendedor';

SELECT COUNT(*) FROM c1e c;

-- practica 1.21
SELECT * 
  FROM emple e
  WHERE e.apellido LIKE 'm%' OR 'n%' 
   ORDER BY e.apellido ASC;

--  practica 1.22
  SELECT * FROM emple e
    WHERE e.oficio='vendedor'
    ORDER BY e.apellido ASC;

--  practica 1.23
  SELECT
    e.apellido, 
    MAX(e.salario) 
  FROM emple e;

--  practica 1.24
SELECT * FROM emple e
  WHERE e.oficio='analista' AND  dept_no=10
ORDER BY apellido , oficio  ;

--  practica 1.25
  SELECT DISTINCT MONTHNAME(e.fecha_alt)
    FROM emple e;

-- practica 1.26
SELECT DISTINCT YEAR(e.fecha_alt)
    FROM emple e;

-- practica 1.27
SELECT DISTINCT DAYOFMONTH(e.fecha_alt)
    FROM emple e;

-- practica 1.28
SELECT e.apellido
  FROM emple e
  WHERE e.salario>2000 OR e.dept_no=20;

-- practica 1.29
  SELECT 
         e.apellido,
         d.dnombre
    FROM emple e
    JOIN depart d ON e.dept_no = d.dept_no;

-- practica 1.30
 SELECT 
         e.apellido,
         e.oficio,
         d.dnombre
    FROM emple e
    JOIN depart d ON e.dept_no = d.dept_no
      ORDER BY e.apellido DESC;


-- practica 1.31
  SELECT e.dept_no, COUNT(*) FROM emple e
    GROUP BY e.dept_no;

-- practica 1.32
CREATE OR REPLACE VIEW c1_32 AS
  SELECT e.dept_no, COUNT(*) cuenta FROM emple e
    GROUP BY e.dept_no;

  SELECT 
         d.dnombre,
         c.cuenta
    FROM depart d
    JOIN c1_32 c ON d.dept_no=c.dept_no;

-- practica 1.33
  SELECT e.apellido FROM emple e
    ORDER BY e.oficio AND e.apellido;

  -- practica 1.34
    SELECT e.apellido FROM emple e
      WHERE e.apellido LIKE 'a%';
    
  -- practica 1.35
   SELECT e.apellido FROM emple e
      WHERE e.apellido LIKE 'm%' OR e.apellido LIKE 'a%' ; 

 -- practica 1.36
   SELECT * FROM emple e
      WHERE e.apellido NOT LIKE '%z';  
  
  -- practica 1.37
    SELECT * FROM emple e
      WHERE e.apellido LIKE 'a%' AND e.oficio LIKE '%e%' 
    ORDER BY e.oficio DESC, e.salario DESC;


