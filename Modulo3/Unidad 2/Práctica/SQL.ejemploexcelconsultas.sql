﻿ALTER TABLE productos ADD COLUMN IF NOT EXISTS descuento1 float;
                                                                                                                               
SELECT p.importe_base*(1-10/100) descuento1 FROM productos p;

SELECT * FROM productos p
  WHERE p.rubro='verduras';

-- con if
UPDATE productos p
  SET p.descuento1=IF (p.rubro='verduras',p.importe_base*0.1,0);

UPDATE productos p JOIN (SELECT * FROM productos p
  WHERE p.rubro='verduras') c USING(producto) 
  SET p.descuento1=p.importe_base*(1-10/100);

ALTER TABLE productos ADD COLUMN IF NOT EXISTS descuento2 float;

SELECT * FROM productos p
  WHERE p.`u/medida`='atado';

UPDATE productos p JOIN (SELECT * FROM productos p
  WHERE p.`u/medida`='atado') c USING (producto)
SET p.descuento2= p.importe_base*0.20;

 UPDATE productos p JOIN (SELECT * FROM productos p
  WHERE p.`u/medida`<>'atado') c USING (producto)
SET p.descuento2= p.importe_base*0.05;

-- 6 
  ALTER TABLE productos ADD COLUMN IF NOT EXISTS descuento3 float;

  SELECT * FROM productos p
    WHERE p.rubro='frutas' AND p.importe_base>=15;

  UPDATE productos p JOIN(
   SELECT * FROM productos p
    WHERE p.rubro='frutas' AND p.importe_base>=15) c USING(producto)
    SET p.descuento3=p.importe_base*0.20;

  -- con if 
    UPDATE productos p
      SET p.descuento3=p.importe_base*IF (p.rubro='frutas' AND p.importe_base>=15, 0.2,0);

-- 7
  ALTER TABLE productos ADD COLUMN IF NOT EXISTS descuento4 float;

  SELECT * FROM productos p
    WHERE p.granja='primavera' OR  p.granja='litoral';

  UPDATE productos p JOIN (
 SELECT * FROM productos p
    WHERE p.granja='primavera' OR  p.granja='litoral') c USING(producto)
    SET p.descuento4=p.importe_base*0.50;


 UPDATE productos p JOIN (
 SELECT * FROM productos p
    WHERE p.granja<>'primavera' OR  p.granja<>'litoral') c USING(producto)
    SET p.descuento4=p.importe_base*0.25;

-- 8 
  ALTER TABLE productos ADD COLUMN IF NOT EXISTS aumento1 float;

-- sin join
  UPDATE productos p
    SET p.aumento1=p.importe_base*0.10
  WHERE  p.rubro= ('frutas' OR  p.rubro='verduras')
  AND
     p.granja=('la garota' OR p.granja='la pocha');

-- con not

    UPDATE productos p
    SET p.aumento1=0
  WHERE 
      NOT (
      (p.rubro= ('frutas' OR  p.rubro='verduras')
  AND
     p.granja=('la garota' OR p.granja='la pocha')));

    -- con if
      UPDATE productos p
        SET  p.aumento1=p.importe_base*
        IF ((p.rubro= ('frutas' OR  p.rubro='verduras')
               AND
            p.granja=('la garota' OR p.granja='la pocha')), 0.1,0);

-- 9 
  ALTER TABLE productos ADD COLUMN IF NOT EXISTS presentacion float;

  -- con varios update

  UPDATE productos p JOIN (
    SELECT * FROM productos p 
    WHERE p.`u/medida`='atado' ) c  USING (`u/medida`)
    SET  p.presentacion=1;
    

  UPDATE productos p JOIN (
    SELECT * FROM productos p 
    WHERE p.`u/medida`='unidad' ) c  USING (`u/medida`)
    SET  p.presentacion=2;

    UPDATE productos p JOIN (
    SELECT * FROM productos p 
    WHERE p.`u/medida`='kilo' ) c  USING (`u/medida`)
    SET  p.presentacion=3;

    -- con if

  UPDATE productos p
    SET p.presentacion=IF(p.`u/medida`='atado',1,IF(p.`u/medida`='unidad',2,3));

  -- con case 


-- 10


   
 ALTER TABLE productos ADD COLUMN IF NOT EXISTS categoria char;
SELECT * FROM productos p
  WHERE p.importe_base<=10;

UPDATE productos p JOIN(
  SELECT * FROM productos p
  WHERE p.importe_base<=10) c USING(importe_base)
  SET p.categoria= 'a';

UPDATE productos p JOIN(
  SELECT * FROM productos p
  WHERE p.importe_base BETWEEN 10 AND 20) c USING(importe_base)
  SET p.categoria= 'b';

UPDATE productos p JOIN(
  SELECT * FROM productos p
  WHERE p.importe_base>=20) c USING(importe_base)
  SET p.categoria= 'c';



-- 11

 ALTER TABLE productos ADD COLUMN IF NOT EXISTS aumento2 char;

SELECT * FROM productos p
  WHERE p.rubro='frutas' AND p.granja='litoral';
-- 11-a

UPDATE productos p JOIN(
 SELECT * FROM productos p
  WHERE p.rubro='frutas' AND p.granja='litoral' ) c USING (producto)
  SET p.aumento2=p.importe_base*(1+10/100);
-- 11-b

UPDATE productos p JOIN(
 SELECT * FROM productos p
  WHERE p.rubro='verduras' AND p.granja='el ceibal' ) c USING (producto)
  SET p.aumento2=p.importe_base*(1+15/100);
-- 11-c

UPDATE productos p JOIN(
 SELECT * FROM productos p
  WHERE p.rubro='semillas' AND p.granja='el canuto' ) c USING (producto)
  SET p.aumento2=p.importe_base*(1+20/100);

-- 12 




         
           


