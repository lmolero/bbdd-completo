﻿-- practica 5_1
  SELECT 
         m.nombre_m
    FROM composicion c
    JOIN miembro m USING (dni);

-- practica 5_2
  SELECT DISTINCT
         f.direccion
    FROM composicion c
    JOIN federacion f USING (nombre);

-- practica 5_3
  SELECT 
        c.nombre
    FROM composicion c
  WHERE c.cargo='Information Tec';

  SELECT DISTINCT
    f.nombre
    FROM federacion f
    LEFT JOIN (
      SELECT 
        c.nombre
    FROM composicion c
  WHERE c.cargo='Information Tec')c1 USING (nombre)
  WHERE c1.nombre IS NULL;

-- practica 5_4










 -- practica 5_5

(SELECT 
        c.nombre
    FROM composicion c
  WHERE c.cargo='Information Tec')
  NATURAL JOIN 
(SELECT 
        c.nombre
    FROM composicion c
  WHERE c.cargo='Operations');
