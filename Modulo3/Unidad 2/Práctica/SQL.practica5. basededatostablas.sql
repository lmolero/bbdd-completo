﻿DROP DATABASE IF EXISTS empresa;
CREATE DATABASE IF NOT EXISTS practica5;
USE practica5;

DROP TABLE IF EXISTS federacion;

  CREATE TABLE  IF NOT EXISTS  federacion(
    nombre varchar(15),
    direccion varchar (15),
    telefono varchar (10),
    PRIMARY KEY (nombre)
  );

DROP TABLE IF EXISTS miembro;

  CREATE TABLE  IF NOT EXISTS  miembro(
    dni int AUTO_INCREMENT,
    nombre_m varchar(15),
    titulacion varchar (15),
    PRIMARY KEY (dni)
    );

DROP TABLE IF EXISTS composicion;

  CREATE TABLE  IF NOT EXISTS  composicion(
    nombre varchar(15),
    dni  int,
    cargo varchar (15),
    fecha_inicio date,
    CONSTRAINT fkcomposicionfederacion FOREIGN KEY(nombre)
    REFERENCES federacion(nombre)  ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkcomposicionmiembro FOREIGN KEY(  dni )
    REFERENCES miembro(dni) ON DELETE CASCADE ON UPDATE CASCADE
    );





