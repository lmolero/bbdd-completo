﻿CREATE OR REPLACE TABLE numeros(
  valor int );

INSERT INTO numeros VALUES(1),(100),(220),(400),(1000);

SELECT * FROM numeros n;
-- quiero un procedimiento que me permita indicar:
  -- cuantos numeros pares hay dentro de la tabla

   DELIMITER //
DROP PROCEDURE IF EXISTS pares//
CREATE PROCEDURE pares()
  COMMENT 'cuantos numeros pares hay dentro de la tabla numeros'
  BEGIN
  -- esta variable me ayuda para saber si estoy al final de la tabla cuando leo con cursor
  DECLARE final int DEFAULT 0;
  -- variable para utilizar conel cursor
  DECLARE valorleido int;
  -- se coloca para contar  los numeros pares que tengo en la tabla
  DECLARE contadorpares int DEFAULT 0;
  -- declaro el cursor
DECLARE c CURSOR FOR 
  SELECT n.valor FROM numeros n;

  -- control de excepciones
  -- manejar las excepciones de final de tabla
  DECLARE CONTINUE HANDLER FOR NOT FOUND 
    SET final=1;

 OPEN c;
-- leo el primer registro
 FETCH c INTO valorleido;
  WHILE (final=0) DO
    IF (MOD(valorleido,2)=0) THEN
      SET contadorpares=contadorpares+1;
    END IF;

     SELECT valorleido,final, contadorpares;
    FETCH c INTO valorleido;
  END WHILE;
   
CLOSE c;
 SELECT contadorpares;
  END //
DELIMITER;
TRUNCATE numeros;
CALL pares();


-- encontrar en la tabla numeros los registros que sean mayores que 5
  -- y almacenar esos registros en otra tabla esos numeros

   DELIMITER //
DROP PROCEDURE IF EXISTS  mayoresa5;
CREATE PROCEDURE mayoresa5()
 BEGIN

DECLARE resultadofinal int DEFAULT 0;
  DECLARE lectura int;
  DECLARE Cursor1 CURSOR FOR 
  SELECT n.valor FROM numeros n;

   DECLARE CONTINUE HANDLER FOR NOT FOUND 
    SET resultadofinal=1;
  CREATE OR REPLACE TABLE nummayores(
  mayora5 int);

  OPEN cursor1;
FETCH cursor1 INTO lectura;
  WHILE (resultadofinal=0) DO
  IF (lectura>5) THEN 
  
  INSERT INTO nummayores(mayora5) VALUES(lectura);
 END IF;

  FETCH cursor1 INTO lectura;

  END WHILE;

CLOSE cursor1;

  END
  //
  DELIMITER;

CALL mayoresa5();
SELECT * FROM nummayores n;

CREATE OR REPLACE TABLE notas(
  id int AUTO_INCREMENT,
  nota int,
  repeticiones int,
  valor varchar(50),
PRIMARY KEY (id)
);

INSERT INTO notas(valor) VALUES
  (5),(5),(3),(7),(9),(9),(10),(5),(1);

-- en repeticiones: numeros de veces que se repite esa nota hasta el valor leido
 -- valor: nota con texto
 -- 0.5:suspenso
 -- 5,6:suficiente
  -- 6.7: bien
  -- 7.9: notable
  -- 9.10:sobresaliente


  DELIMITER //
DROP PROCEDURE IF EXISTS  p_notas;
CREATE PROCEDURE p_notas()
 BEGIN

DECLARE vfinal  int DEFAULT 0;
DECLARE vleido int; 
DECLARE textonota varchar(50);
  DECLARE repeticionesde0 int DEFAULT 0;
  DECLARE repeticionesde1 int DEFAULT 0;
  DECLARE repeticionesde2 int DEFAULT 0;
  DECLARE repeticionesde3 int DEFAULT 0;
  DECLARE repeticionesde4 int DEFAULT 0;
  DECLARE repeticionesde5 int DEFAULT 0;
  DECLARE repeticionesde6 int DEFAULT 0;
  DECLARE repeticionesde7 int DEFAULT 0;
  DECLARE repeticionesde8 int DEFAULT 0;
  DECLARE repeticionesde9 int DEFAULT 0;
  DECLARE repeticiones int;
DECLARE registro int;
DECLARE c2 CURSOR FOR 

  SELECT id,valor FROM notas n;

   DECLARE CONTINUE HANDLER FOR NOT FOUND 
    SET vfinal=1;

  OPEN c2;

-- leemos la primera nota
FETCH c2 INTO registro,vleido;
  WHILE (vfinal=0) DO
-- calculo el texto de la nota
SET textonota=notatexto(vleido);

-- calcular las repeticiones
CASE vleido
WHEN 0 THEN
   SET repeticiones=repeticionesde0;
  SET repeticionesde0=repeticionesde0+1;
WHEN 1 THEN
   SET repeticiones=repeticionesde1;
  SET repeticionesde1=repeticionesde1+1;
WHEN 2 THEN
   SET repeticiones=repeticionesde2;
  SET repeticionesde2=repeticionesde2+1;
WHEN 3 THEN
   SET repeticiones=repeticionesde3;
  SET repeticionesde3=repeticionesde3+1;
WHEN 4 THEN
   SET repeticiones=repeticionesde4;
  SET repeticionesde4=repeticionesde4+1;
WHEN 5 THEN
   SET repeticiones=repeticionesde5;
  SET repeticionesde5=repeticionesde5+1;
WHEN 6 THEN
   SET repeticiones=repeticionesde6;
  SET repeticionesde6=repeticionesde6+1;
WHEN 7 THEN
   SET repeticiones=repeticionesde7;
  SET repeticionesde7=repeticionesde7+1;
WHEN 8 THEN
   SET repeticiones=repeticionesde8;
  SET repeticionesde8=repeticionesde8+1;
ELSE 
   SET repeticiones=repeticionesde9;
  SET repeticionesde9=repeticionesde9+1;
  END CASE;

SELECT vleido,textonota,repeticiones;

UPDATE notas 
  SET 
  valor=

-- leemos la nota siguiente
FETCH c2 INTO registro,vleido;
END WHILE;

  CLOSE c2;

  END
  //
  DELIMITER;
 
CALL p_notas();

SELECT * FROM notas n;




DELIMITER//
CREATE OR REPLACE FUNCTION notatexto(valor int)
  RETURNS varchar(50)
  BEGIN
DECLARE resultado varchar(50);
  CASE 
    WHEN (valor<5 )THEN 
    SET resultado='suspenso';
     WHEN (valor<6) THEN
    SET resultado='suficiente';
    WHEN (valor<7) THEN 
    SET resultado ='bien';
    WHEN (valor<9) THEN 
    SET resultado='notable';
    ELSE 
    SET resultado='sobresaliente';
    END CASE;
    
      
  RETURN resultado;
  END 
  //
  DELIMITER;

SELECT notatexto(8);