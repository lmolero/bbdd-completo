﻿USE jardineria;
-- 1
DELIMITER //
DROP PROCEDURE IF EXISTS jardineria1;
CREATE PROCEDURE jardineria1 (namep varchar(50))
BEGIN
  SELECT
    *
  FROM cliente c
  WHERE c.pais = namep;

END
//

DELIMITER ;

CALL jardineria1('spain');

-- 2

DELIMITER//

CREATE OR REPLACE FUNCTION fj2 (nomPais varchar(50))
RETURNS int
BEGIN
  DECLARE r int;

  SELECT
    COUNT(*) INTO r
  FROM cliente c
  WHERE c.pais = nomPais;


  RETURN r;

END//
DELIMITER;

SELECT
  fj2('spain');


-- 3
DELIMITER//

CREATE OR REPLACE FUNCTION fj2 (nomPais varchar(50))
RETURNS int
BEGIN


  DECLARE final int DEFAULT 0;
  DECLARE valorl varchar(20);
  DECLARE contador int DEFAULT 0;
  DECLARE c1 CURSOR FOR
  SELECT
    c.pais
  FROM cliente c;

  DECLARE CONTINUE HANDLER FOR NOT FOUND
  SET final = 1;


  OPEN c1;
  FETCH c1 INTO valorl;
  WHILE (final = 0) DO
    IF (nomPais = valorl) THEN
      SET contador = contador + 1;
    END IF;

    FETCH c1 INTO valorl;

  END WHILE;

  CLOSE c1;

  RETURN contador;

END//
DELIMITER;

-- 4 


DELIMITER // 
CREATE OR REPLACE FUNCTION fj4 (cantidad smallint)
RETURNS smallint
BEGIN

  DECLARE v smallint;

  SELECT
    COUNT(*) INTO v
  FROM producto p
  WHERE p.cantidad_en_stock > cantidad;

  RETURN v;

END
//
DELIMITER;
SELECT
  fj4(5);
-- 5
DELIMITER//

CREATE OR REPLACE FUNCTION fj4_concursor (introducecantidad int)
RETURNS int
BEGIN

  DECLARE contador int DEFAULT 0;
  DECLARE valorleido int DEFAULT 0;
  DECLARE final int DEFAULT 0;


  DECLARE cursorfj2 CURSOR FOR
  SELECT
    p.codigo_producto
  FROM producto p
  WHERE p.cantidad_en_stock > valorleido;

  DECLARE CONTINUE HANDLER FOR NOT FOUND
  SET final = 1;

  OPEN cursorfj2;
  FETCH cursorfj2 INTO valorleido;

  WHILE (final = 0) DO
    SET contador = contador + 1;

    FETCH cursorfj2 INTO valorleido;

  END WHILE;
  CLOSE cursorfj2;

  RETURN contador;

END//
DELIMITER;

SELECT
  fj4_concursor(10);

-- 6
DELIMITER //
DROP PROCEDURE IF EXISTS jardineria6;
CREATE PROCEDURE jardineria6 (formapago varchar(50))
BEGIN

  SELECT
    MAX(p.total) AS pagoMaxv
  FROM pago p
  WHERE p.forma_pago = formapago;


END
//

DELIMITER;
CALL jardineria6('PAYPAL');

-- 7

DELIMITER //
DROP PROCEDURE IF EXISTS jardineria7;
CREATE PROCEDURE jardineria7 (formapago varchar(50))
BEGIN

  SELECT
    MAX(p.total) AS pagoMaxv
  FROM pago p
  WHERE p.forma_pago = formapago;

  SELECT
    MIN(p.total) AS pagoMinv
  FROM pago p
  WHERE p.forma_pago = formapago;

  SELECT
    AVG(p.total) AS pagoAVGv
  FROM pago p
  WHERE p.forma_pago = formapago;

  SELECT
    SUM(p.total) AS pagoSUMAv
  FROM pago p
  WHERE p.forma_pago = formapago;

  SELECT
    COUNT(p.total) AS pagoCUENTAv
  FROM pago p
  WHERE p.forma_pago = formapago;


END
//

DELIMITER;

CALL jardineria7('paypal');


-- 8
DELIMITER//

CREATE OR REPLACE FUNCTION fj8 (nombref varchar(50))
RETURNS decimal
BEGIN
  DECLARE avg decimal;

  SET avg = (SELECT
      AVG(p.precio_venta)
    FROM producto p
    WHERE p.proveedor = nombref);


  RETURN avg;

END//
DELIMITER;

SELECT
  fj8('HiperGarden Tools');

-- 9
DELIMITER//

CREATE OR REPLACE FUNCTION fj9 (nombref varchar(50))
RETURNS decimal
BEGIN
  DECLARE avg decimal;

   IF ((SELECT count(*) FROM producto p
    WHERE p.proveedor=nombref )=0) THEN 
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El proveedor no existe';

    ELSE
 SET avg = (SELECT
      AVG(p.precio_venta)
    FROM producto p
    WHERE p.proveedor = nombref);

  END IF;


  RETURN avg;

END//
DELIMITER;

SELECT fj9('tech');


