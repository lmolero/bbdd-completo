﻿-- ejemplo 9
-- apartado 1.a
DROP DATABASE IF EXISTS ejemplo9;
CREATE DATABASE ejemplo9;

USE ejemplo9;

DELIMITER //
CREATE OR REPLACE PROCEDURE creartablaempleados (test int)
BEGIN

  CREATE OR REPLACE TABLE empleados (
    id int AUTO_INCREMENT,
    nombre varchar(20),
    fechanacimiento date,
    edad int,
    PRIMARY KEY (id)
  ) ENGINE MYISAM;

  IF (test = 1) THEN
    INSERT INTO empleados (nombre, fechanacimiento)
      VALUES ('pedro', '2000/1/1'),
      ('ana', '1980/10/1'),
      ('jose', '1990/12/4'),
      ('luis', '1985/4/9'),
      ('ernesto', '1975/4/4');

  END IF;

END//
DELIMITER;

CALL creartablaempleados(1);

SELECT
  *
FROM empleados e;


DELIMITER //
CREATE OR REPLACE PROCEDURE creartablaempresa (test int)
BEGIN

  CREATE OR REPLACE TABLE empresa (
    id int AUTO_INCREMENT,
    nombre varchar(20),
    direccion varchar(20),
    numeroempleados int,
    PRIMARY KEY (id)
  ) ENGINE MYISAM;

  IF (test = 1) THEN
    INSERT INTO empresa (nombre, direccion)
      VALUES ('empresa1', 'direccion1'),
      ('empresa2', 'direccion2'),
      ('empresa3', 'direccion3'),
      ('empresa4', 'direccion4'),
      ('empresa5', 'direccion5');

  END IF;

END//
DELIMITER;

DELIMITER //
CREATE OR REPLACE PROCEDURE creartablatrabajan (test int)
BEGIN

  CREATE OR REPLACE TABLE trabajan (
    id int AUTO_INCREMENT,
    empleado int,
    empresa int,
    fechaInicio date,
    fechaFin date,
    baja bool DEFAULT FALSE,
    PRIMARY KEY (id),
    CONSTRAINT trabajanuk UNIQUE KEY (empleado, empresa)
  ) ENGINE MYISAM;

  IF (test = 1) THEN
    INSERT INTO trabajan (empleado, empresa, fechaInicio, fechaFin)
      VALUES (1, 1, '2015/1/1', NULL);

  END IF;

END//
DELIMITER;

-- disparador que fecha de inicio no sea mayor a la de fin

DELIMITER //
CREATE OR REPLACE TRIGGER t1beforeinsert
BEFORE INSERT
ON trabajan
FOR EACH ROW
BEGIN

  DECLARE numero int;
  DECLARE nempresa int;

  IF (new.fechaInicio > new.fechaFin) THEN

    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la fecha no es válida';
  END IF;

   -- cuando meta un empleado en trabaja me verifique si existe, si no que me de un error
 -- luego, borramos la consulta y sustituimos por la funcion buscar empleado...
 
  IF (buscarempleado(new.empleado)= 0) THEN

    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'el trabajador no está registrado';
  END IF;

    -- cuando meta una empresa  me verifique si existe, si no que me de un error
 -- luego, borramos la consulta y sustituimos por la funcion buscar empresa...

  IF (buscarempresa(new.empresa) =0) THEN
 SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la empresa no está registrada';
  END IF;

  -- disparador  quiero que el campo baja si meto un dato en fecha de fin se ponga true sino false

  IF (new.fechaFin IS NOT NULL) THEN
    SET new.baja = TRUE;
  END IF;

END//
DELIMITER ;

-- disparadores para actualizar

DELIMITER //
CREATE OR REPLACE TRIGGER t1beforeupdate
BEFORE UPDATE
ON trabajan
FOR EACH ROW
BEGIN

  DECLARE numero int;
  DECLARE nempresa int;

  -- para modificar id de empleados y si no está en tabla empelados , de un error
  

  IF (buscarempleado(new.empleado) = 0) THEN
     SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'el trabajador no está registrado';
  END IF;

 -- para modificar id de empresa y si no está en tabla empresa , de un error

  IF (buscarempresa(new.empresa)=0) THEN
 SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la empresa no está registrada';
  END IF;

 -- para modificar fechaInicio pero, si fecha inicio mayor a fechaFin  de un error
  IF (new.fechaInicio > new.fechaFin) THEN

    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la fecha no es válida';
  END IF;

 -- para modificar en la columna baja que si se introduce una fechafin me coloque un 1 sino 0

  IF (new.fechaFin IS NOT NULL) THEN
    SET new.baja = TRUE;
  ELSE
    SET new.baja = FALSE;
  END IF;


END//
DELIMITER ;

-- crear una funcion que reciba como arg un id d empresa y me devuelva si la empresa existe
DELIMITER//

CREATE OR REPLACE FUNCTION buscarempresa (idempresa int)
RETURNS int
BEGIN
  DECLARE numero int;

set numero=(SELECT
    COUNT(*) 
  FROM empresa e
  WHERE e.id = idempresa);

  RETURN numero;

END//
DELIMITER;


 -- crear una funcion que reciba como arg un id de empleado y m devuelva si el empleado existe
  

  DELIMITER//

CREATE OR REPLACE FUNCTION buscarempleado(idempleado int)
RETURNS int
BEGIN
  DECLARE numero int;

set numero=(SELECT
    COUNT(*) 
  FROM empleados e
  WHERE e.id = idempleado);

  RETURN numero;

END//
DELIMITER;



 -- disparador para la tabla empleado
  DELIMITER //
  CREATE OR REPLACE TRIGGER Tbeforeinsert
  BEFORE INSERT
    ON empleados
    FOR EACH ROW
  BEGIN
 DECLARE edad date;

  SET new.edad= TIMESTAMPDIFF(year,new.fechanacimiento, CURDATE());
  
 
  END // 
  DELIMITER ;
-- correos empresa-empelado
 SHOW TABLES;
SELECT CONCAT(empleados.nombre,'@',empresa,'.com') FROM empleados JOIN trabajan ON empleados.id=empleado
  JOIN empresa ON empresa=empresa.id;

