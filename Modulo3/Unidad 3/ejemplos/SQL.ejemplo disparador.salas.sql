﻿USE tema3;

CREATE OR REPLACE TABLE entradas(
  id int AUTO_INCREMENT PRIMARY KEY,
  valor int DEFAULT 0,
  total int DEFAULT 0,
  codigo int DEFAULT 0
  );
INSERT INTO entradas (valor, codigo)
  VALUES(2,10);
SELECT * FROM  entradas e;




DELIMITER //
CREATE OR REPLACE TRIGGER TRIGGER1
BEFORE INSERT
  ON entradas
  FOR EACH ROW
BEGIN
-- quiero el total con el valor al cuadrado


SET new.total=POW(new.valor,2);

  
END // 
DELIMITER ;



DELIMITER //
CREATE OR REPLACE TRIGGER TRIGGER1
BEFORE INSERT
  ON entradas
  FOR EACH ROW
BEGIN
-- si el codigo es menor que 50 quiero el
-- total como valor al cuadrado
-- y si el valor es mayor o igual a 50 
-- quiero el total como valor al cubo

IF (new.codigo<50) THEN 
  SET new.total=POW(new.valor,2);
ELSE  
  SET new.total=POW(new.valor,3);
 
  END IF;

END // 
DELIMITER ;

CREATE TABLE IF NOT EXISTS salas(
  id int AUTO_INCREMENT PRIMARY KEY,
  butacas int,
  fecha date,
  edad int
  );

CREATE TABLE IF NOT EXISTS t2(
  id int AUTO_INCREMENT PRIMARY KEY,
  sala int,
  numero int
  );


-- creando dispaardor para la tabla salas antes de insertar
DELIMITER //
CREATE OR REPLACE TRIGGER TRIGGERsala
BEFORE INSERT
  ON salas
  FOR EACH ROW
BEGIN
  -- disparador para que me calcule la edad en salas

  SET new.edad= TIMESTAMPDIFF(year,new.fecha, NOW());



END // 
DELIMITER ;

SELECT * FROM salas s;

-- creando dispaardor para la tabla salas antes de update
DELIMITER //
CREATE OR REPLACE TRIGGER tsala
BEFORE UPDATE
  ON salas
  FOR EACH ROW
BEGIN
  -- disparador para que me calcule la edad en salas

  SET new.edad= TIMESTAMPDIFF(year,new.fecha, NOW());



END // 
DELIMITER ;

SELECT * FROM salas s;

UPDATE salas s SET s.fecha='2001/1/1';

DELIMITER //
CREATE OR REPLACE TRIGGER t2
BEFORE UPDATE
  ON salas
  FOR EACH ROW
BEGIN
  -- disparador quiero comprobar que la sala este en la tabla salas





END // 
DELIMITER ;