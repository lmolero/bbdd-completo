﻿USE ejemplo7;

-- 1.- Crear un disparador para que cuando inserte un registro en clientes me compruebe si la edad esta entre 18 y 70. En caso
-- de que no esté produzca una excepción con el mensaje “La edad no es válida”. ......

  DELIMITER //
  CREATE OR REPLACE TRIGGER t1beforeinsert
  BEFORE INSERT
    ON clientes
    FOR EACH ROW
  BEGIN
    
    IF (new.edad BETWEEN 18 AND 70 ) THEN 
    SET new.edad= 'la edad es valida';
    ELSE 
/*2.- Además, cada vez que un usuario introduzca un registro con una edad no valida debe grabar ese registro en una tabla
-- denominada errores.*/
INSERT INTO errores(edad) VALUES(new.edad);

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la edad no es valida';


      END IF;

  END // 
  DELIMITER ;

-- 3.- Crear un disparador para que cuando modifique un registro de la tabla clientes me compruebe si la edad esta entre 18 y
-- 70. En caso de que no esté produzca una excepción con el mensaje “La edad no es válida”

 DELIMITER //
 CREATE OR REPLACE TRIGGER t3beforeupdate
 BEFORE UPDATE 
   ON clientes
   FOR EACH ROW
 BEGIN
   
IF (new.edad BETWEEN 18 AND 70) THEN 
  SET new.edad= 'la edad es valida';
    ELSE 
  SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la edad no es valida';
END IF;

 END // 
 DELIMITER ;

-- 4.- Crear disparadores en la tabla localidades que comprueben que cuando introduzcas o modifiques una localidad el código
-- de la provincia exista en la tabla provincias. En caso de que no exista que cree la provincia. El resto de campos de la tabla
-- provincias se colocarán a valor por defecto.

  DELIMITER //
  CREATE OR REPLACE TRIGGER t4beforeinsert
  BEFORE INSERT
    ON localidades
    FOR EACH ROW
  BEGIN

     DECLARE numero int;

    DECLARE CONTINUE HANDLER FOR SQLSTATE '45000'
      BEGIN 
 INSERT INTO provincias(CodPro) 
        VALUES (new.CodPro);
      END;

    SELECT COUNT(*) INTO numero FROM provincias p
      WHERE p.CodPro=new.CodPro;
    
 -- primera opcion
   IF (numero=0)THEN 
    SIGNAL SQLSTATE '45000';
      END IF ;




-- segunda opcion
/*IF(SELECT * FROM provincias p WHERE p.CodPro=new,CodPro) THEN 
  INSERT INTO provincias(CodPro)
    VALUES(new.CodPro);
  END IF;*/

  END // 
  DELIMITER ;


-- para actualizar y modificar
   DELIMITER //
  CREATE OR REPLACE TRIGGER t4beforeinsert
  BEFORE INSERT
    ON localidades
    FOR EACH ROW
  BEGIN

     DECLARE numero int;


    DECLARE CONTINUE HANDLER FOR SQLSTATE '45000'
      BEGIN 
 INSERT INTO provincias(CodPro) 
        VALUES (new.CodPro);
      END;

    SELECT COUNT(*) INTO numero FROM provincias p
      WHERE p.CodPro=new.CodPro;
    
 -- primera opcion
   IF (numero=0)THEN 
    SIGNAL SQLSTATE '45000';
      END IF ;




-- segunda opcion
/*IF(SELECT * FROM provincias p WHERE p.CodPro=new,CodPro) THEN 
  INSERT INTO provincias(CodPro)
    VALUES(new.CodPro);
  END IF;*/

  END // 
  DELIMITER ;




-- 5.- Modificar los disparadores de la tabla clientes para que cuando introduzcas o modifiques un cliente compruebe si la
-- localidad del cliente exista. En caso que no exista mostrar el mensaje de error “La localidad no existe”. Debe insertar ese registro
-- de error en la tabla errores.

 DELIMITER //
  CREATE OR REPLACE TRIGGER t1beforeinsert
  BEFORE INSERT
    ON clientes
    FOR EACH ROW
  BEGIN
    DECLARE numero int;
    
    IF (new.edad BETWEEN 18 AND 70 ) THEN 
    SET new.edad= 'la edad es válida';
    ELSE 


INSERT INTO errores(mensaje,fecha,hora) VALUES('la edad no es válida', CURDATE(), CURTIME());;

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la edad no es válida';
      END IF;

SELECT COUNT(*) INTO numero FROM localidades l
  WHERE NEW.CodLoc=l.CodLoc;


IF(numero=0) THEN 
INSERT INTO errores(mensaje,fecha,hora) VALUES('la localidad no existe', CURDATE(), CURTIME());
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='la localidad no existe';
     END IF;

  END // 
  DELIMITER ;

-- actualizando
  DELIMITER //
  CREATE OR REPLACE TRIGGER t1beforeinsert
  BEFORE UPDATE 
    ON clientes
    FOR EACH ROW
  BEGIN
    DECLARE numero int;
    
    IF (new.edad BETWEEN 18 AND 70 ) THEN 
    SET new.edad= 'la edad es válida';
    ELSE 


INSERT INTO errores(mensaje,fecha,hora) VALUES('la edad no es válida', CURDATE(), CURTIME());;

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la edad no es válida';
      END IF;

SELECT COUNT(*) INTO numero FROM localidades l
  WHERE NEW.CodLoc=l.CodLoc;


IF(numero=0) THEN 
INSERT INTO errores(mensaje,fecha,hora) VALUES('la localidad no existe', CURDATE(), CURTIME());
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='la localidad no existe';
     END IF;

  END // 
  DELIMITER ;


  -- 6.- Crear unos disparadores para la tabla provincias que cuando modifiques o insertes un registro te realice algunas
-- operaciones


    DELIMITER //
    CREATE OR REPLACE TRIGGER t6 
   BEFORE  INSERT
      ON provincias
      FOR EACH ROW
    BEGIN
   DECLARE numero int DEFAULT 0;
     
SET new.inicial=new.LEFT(new.NomPro,1);

SELECT COUNT(*) INTO numero  FROM localidades l
      WHERE new.CodPro=l.CodPro;

SET new.cantidad=numero;

    END // 
    DELIMITER ;

  -- 7.- Modificar los disparadores para la tabla localidades para que cuando insertes o modifiques un registro te recalcule el
-- campo cantidad de la tabla provincias

    DELIMITER //
    CREATE OR REPLACE TRIGGER t7
    AFTER INSERT
      ON localidades
      FOR EACH ROW
    BEGIN
 -- almaceno en tabla provincias     
UPDATE provincias p
  SET p.cantidad=(SELECT COUNT(*) FROM localidades l
                  WHERE l.CodPro=new.CodPro )
      WHERE p.CodPro=new.CodPro;

    END // 
    DELIMITER ;

-- para el update
 DELIMITER //
    CREATE OR REPLACE TRIGGER t7
    AFTER INSERT
      ON localidades
      FOR EACH ROW
    BEGIN
 -- almaceno en tabla provincias     
UPDATE provincias p
  SET p.cantidad=(SELECT COUNT(*) FROM localidades l
                  WHERE l.CodPro=new.CodPro )
      WHERE p.CodPro=new.CodPro;

    END // 
    DELIMITER ;