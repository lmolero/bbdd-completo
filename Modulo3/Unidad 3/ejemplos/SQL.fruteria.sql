﻿DROP DATABASE IF EXISTS fruteria;
CREATE DATABASE IF NOT EXISTS fruteria;

USE fruteria;

-- procedimientos 

 -- 1
DELIMITER //
DROP PROCEDURE IF EXISTS pfruteria1;
CREATE PROCEDURE pfruteria1(idproveedor int)
  COMMENT 'Realizar un procedimiento que reciba el codigo de un proveedor como parámetro de entrada
           y realice una consulta sobre la tabla productos para obtener resumen de productos por proveedor'
BEGIN
 
SELECT * FROM productos
  WHERE idproveedor=CodProveedor;

END
//
DELIMITER ;

CALL pfruteria1(1);

-- 2 
DELIMITER //
DROP PROCEDURE IF EXISTS pfruteria2;
CREATE PROCEDURE pfruteria2()
  COMMENT 'crear una tabla llamada resumen que tenga los siguientes campos:
            nombrefruta, mensaje'
BEGIN
 
CREATE OR REPLACE TABLE resumen(
  nombrefruta varchar(255) PRIMARY KEY,
  mensaje varchar(255)
  );

END
//
DELIMITER ;

CALL pfruteria2();

-- 3
DELIMITER //
DROP PROCEDURE IF EXISTS pfruteria3;
CREATE PROCEDURE pfruteria3(fruta varchar (255))
  COMMENT 'insertar en la tabla resumen un producto y me saca el mensaje de como está de stock la fruta'
BEGIN

INSERT INTO resumen(nombrefruta, mensaje) VALUES(fruta,ffruteria(fruta));

END
//
DELIMITER ;

CALL pfruteria3('mango');
SELECT * FROM resumen r;
 
-- 4
  DELIMITER //
DROP PROCEDURE IF EXISTS pfruteria4;
CREATE PROCEDURE pfruteria4()
  COMMENT 'crear una columna llamada precio en la tabla facturas'
BEGIN
 
ALTER TABLE facturas ADD COLUMN IF NOT EXISTS precio decimal(10,2) ;

END
//
DELIMITER ;

CALL pfruteria4();

-- funciones

/* 1 funtion . realizar una funcion que reciba como parametro de entrada tres frutas y devuelva:
            el importe mas alto de las frutas introducidas */

DELIMITER //

CREATE OR REPLACE FUNCTION  fmax(fruta1 varchar(255),fruta2 varchar(255), fruta3 varchar(255))
  RETURNS float
BEGIN
  

 DECLARE maximporte float;

 
  SET maximporte=( SELECT MAX(importe) FROM productos
    JOIN facturas ON codigo=codProducto
  WHERE descripcion=fruta1 OR descripcion=fruta2 OR descripcion=fruta3);


RETURN maximporte;
 
END
//
DELIMITER ;

SELECT fmax('platano','fresa','uvas');


/* 2 funtion . realizar una funcion que reciba como parametro de entrada tres frutas y devuelva:
            el importe minimo de las frutas introducidas */

  DELIMITER //

CREATE OR REPLACE FUNCTION  fmin (fruta1 varchar(255),fruta2 varchar(255), fruta3 varchar(255))
  RETURNS float

BEGIN
  
 DECLARE minimporte float;

SET minimporte=(SELECT MIN(importe) FROM productos
    JOIN facturas ON codigo=codProducto
  WHERE descripcion=fruta1 OR descripcion=fruta2 OR descripcion=fruta3);


RETURN minimporte;
 
END
//
DELIMITER ;

SELECT fmin('manzana','arandanos','fresa');

/* funtion 3. crear funcion que reciba una fruta  y debe indicar cuanto de esa fruta hay en stock, 
  si es 0 que muestre un mensaje 'pedir al proveedor'*/

DELIMITER //

CREATE OR REPLACE FUNCTION  ffruteria(fruta varchar(255))
  RETURNS varchar (255)
BEGIN
  
DECLARE stock int;
DECLARE texto varchar (255);

SET stock=(SELECT p.stock FROM productos p
  WHERE p.descripcion=fruta);

  IF (stock=0) THEN 
    SET texto='pedir producto al proveedor';
    ELSE 
    SET texto='suficientes productos en stock';

END IF ;

RETURN texto;
 
END
//
DELIMITER ;


SELECT ffruteria('platano');

-- disparadores

-- 1 Crear un disparador para que cuando inserte una fruta en la tabla factura me compruebe si la fruta está registrada en productos.
  -- En caso de que no esté produzca una excepción con el mensaje “la fruta no está registrada”

 DELIMITER //
 CREATE OR REPLACE TRIGGER t1
 BEFORE insert
   ON facturas
   FOR EACH ROW
 BEGIN
   
DECLARE numero int;

    DECLARE CONTINUE HANDLER FOR SQLSTATE '45000' 
      BEGIN 
 INSERT INTO productos(codigo) 
        VALUES (new.codProducto);
      END;

    SELECT COUNT(*) INTO numero FROM productos p
      WHERE p.codigo=new.codProducto;
    
   IF (numero=0)THEN 
    SIGNAL SQLSTATE '45000';
      END IF ;
 END // 
 DELIMITER ;

-- 2  Crear un disparador para la tabla productos que si eliminas un producto te elimine todos los productos del mismo código
-- en la tabla factura

  DELIMITER //
  CREATE OR REPLACE TRIGGER t2
  AFTER DELETE      
    ON productos 
    FOR EACH ROW
  BEGIN

  DELETE FROM facturas WHERE codProducto=old.codigo;
  
  END // 
  DELIMITER ;

-- 3.- Crear un disparador para la tabla factura para que cuando actualices un registro nuevo te calcule el total
-- automáticamente.

  DELIMITER //
  CREATE OR REPLACE TRIGGER t3
  BEFORE UPDATE 
    ON facturas
    FOR EACH ROW
  BEGIN

    SET new.importe=new.precio*new.cantidad;
  
  END // 
  DELIMITER ;




