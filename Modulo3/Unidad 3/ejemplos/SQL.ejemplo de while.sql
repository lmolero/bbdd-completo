﻿-- ejemplo con iterate

-- he cerado una tabla para probar el procedure

  CREATE OR REPLACE TABLE  prueba(
    id int AUTO_INCREMENT,
    numeros int DEFAULT 0,
    PRIMARY KEY (id)
    );

-- procedimiento alamcenado bucle1
-- argumentos: a1 entero, a2 entero
-- introducir datos en la tabla pruebas desd el argumento1
-- hasta el argumento2
-- no debe introducir ni el 8 ni el 2

DELIMITER $$
DROP PROCEDURE IF EXISTS ejcw $$
CREATE PROCEDURE ejcw (a1 int, a2 int)

BEGIN

  DECLARE contador int DEFAULT a1; -- variable contador

  -- comienzo del bucle etiqueta

 etiqueta: WHILE (contador<=a2) DO
    
    IF (contador=8 OR contador=12) THEN
       SET contador=contador+1;
      ITERATE etiqueta;
      END IF ;
-- meter el dato en la tabla
     INSERT INTO prueba VALUE (DEFAULT, contador);

   SET contador=contador+1;
   
  -- fin del bucle etiqueta
    end WHILE etiqueta;

END
$$

DELIMITER ;

CALL ejcw(5,20);
SELECT * FROM prueba p;

-- el mismo ejercicio con while

DELIMITER $$
DROP PROCEDURE IF EXISTS ejcw2 $$
CREATE PROCEDURE ejcw2 (a1 int, a2 int)

BEGIN

  DECLARE contador int DEFAULT a1; -- variable contador
  WHILE (contador<=a2) DO
    IF (contador<>8 AND contador<>12) THEN 
      INSERT INTO prueba VALUE(DEFAULT, contador);
END IF ;
    SET contador=contador+1;
    END WHILE;

END
$$

DELIMITER ;

CALL ejcw2(5,20);
SELECT * FROM prueba p;

-- funcion1
-- ejemplos con funciones
-- crear una funcion que le pasemos 3 numeros y te 
--  devuelva la suma de ellos
-- si los numeros no son mayores que 0 devuelve null

  DELIMITER// 
DROP FUNCTION IF EXISTS funcion1//
CREATE FUNCTION IF NOT EXISTS funcion1(num1 int,num2 int ,num3 int)
  RETURNS int 
  BEGIN
DECLARE suma int DEFAULT null;
  -- analizas los datos y preparas el valor a devolver
IF (num1>0 AND num2>0 AND num3>0) THEN
SET suma=num1+num2+num3;
  END IF;

-- al final realizo el return
  RETURN suma;
END //
DELIMITER;

SELECT funcion1(23,24,2);
