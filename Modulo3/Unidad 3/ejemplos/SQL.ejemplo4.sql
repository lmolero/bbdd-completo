﻿CREATE DATABASE IF NOT EXISTS ejemplo4;
USE ejemplo4;

-- 1
DELIMITER //
DROP PROCEDURE IF EXISTS p1;
CREATE PROCEDURE p1(texto varchar(50))
BEGIN

  CREATE TABLE IF NOT EXISTS tabla1(
    mensaje varchar (50));

  INSERT INTO tabla1(mensaje) VALUE(texto);
      END
//

DELIMITER ;


CALL p1('ésto es un ejemplo de procedimiento');

SELECT * FROM tabla1;

TRUNCATE tabla1;

-- 2

DELIMITER //
DROP  PROCEDURE IF EXISTS p2;
CREATE PROCEDURE p2()
BEGIN

DECLARE a int DEFAULT 1;
DECLARE b varchar(10);
DECLARE c DECIMAL(4,2) DEFAULT (10,48);

END
//

DELIMITER ;

-- 4

DELIMITER //
DROP  PROCEDURE IF EXISTS p3;
CREATE PROCEDURE p3(numero int)
BEGIN
DECLARE num int DEFAULT 0;
SET num=(numero);
SELECT num;

END
//

DELIMITER ;

CALL p3(2);

-- 5
DELIMITER //
DROP  PROCEDURE IF EXISTS p5;
CREATE PROCEDURE p5(parametro int)
BEGIN
DECLARE variable int DEFAULT 0;
SET variable=(parametro);
SELECT variable;

END
//

DELIMITER ;

CALL p5(50);


-- 6
DELIMITER //
DROP  PROCEDURE IF EXISTS p6;
CREATE PROCEDURE p6(n int)
BEGIN
   
  IF (n>0) THEN
    SELECT  'positivo';

   ELSEIF
    (n<0)  THEN
    SELECT 'negativo';

    ELSE 
  SELECT 'cero';

    END IF;
END
//

DELIMITER ;

CALL p5(0);
CALL p5(-8);
CALL p5(10);

-- 7
  DELIMITER//
DROP FUNCTION IF EXISTS pares;
  CREATE FUNCTION pares(num float)
 RETURNS varchar (20)
    BEGIN 
DECLARE n varchar (20) DEFAULT '';

 IF  (n=mod(num,2)=0) THEN
  SET n='true';
  ELSE 
  SET n='false';
  END IF;

    RETURN n ;

END; 
    //
DELIMITER;

SELECT pares(5);

-- 8

  DELIMITER//
DROP FUNCTION IF EXISTS hipotenusa;
  CREATE FUNCTION hipotenusa(a float, b float)
 RETURNS float
    BEGIN 
DECLARE c float DEFAULT 0;

 SET c=SQRT(POW(a,2)+POW(b,2));

    RETURN c ;

END; 
    //
DELIMITER;

SELECT hipotenusa(10,10);

-- 9
DELIMITER //
DROP  PROCEDURE IF EXISTS p9;
CREATE PROCEDURE p9(IN n int, OUT texto varchar(50))
BEGIN
    
IF (n>0) THEN
    SET texto="positivo";

   ELSEIF
    (n<0)  THEN
      SET texto="negativo";

    ELSE 
    SET texto="cero";

   END IF;
  
END
//

DELIMITER ;

CALL p9(10,@s);
SELECT @s;

-- 10
DELIMITER //
DROP  PROCEDURE IF EXISTS p10;
CREATE PROCEDURE p10(nota int)
BEGIN
    
CASE 
 WHEN (nota<5) THEN 
    SELECT 'insuficiente';

 WHEN (nota BETWEEN 5 AND 6) THEN 
    SELECT 'aprovado';

WHEN (nota BETWEEN 6 AND 7) THEN
    SELECT 'Bien';

WHEN (nota BETWEEN 7 AND 8) THEN
    SELECT 'Notable';
ELSE 
    SELECT 'sobresaliente';


END CASE;
END
//

DELIMITER ;

CALL p10(5);

-- 11
DELIMITER //
DROP  PROCEDURE IF EXISTS p11;
CREATE PROCEDURE p11(name varchar (50), mark int)
BEGIN
  
  CREATE OR REPLACE TABLE notas(
    id int AUTO_INCREMENT,
    nombre varchar (50),
    nota int,
    PRIMARY KEY (id));

  INSERT INTO notas(nombre,nota) VALUES(name, mark);

    

END
//

DELIMITER ;

CALL p11('ana',10);
SELECT * FROM notas;

-- 12

  DELIMITER//
DROP FUNCTION IF EXISTS diassemana //
  CREATE FUNCTION diassemana(dato float, dia varchar(20))
 RETURNS varchar(20)
    BEGIN 

    CASE 
    WHEN  (dato=1) THEN 
      SET dia='lunes';
    WHEN  (dato=2) THEN 
      SET dia='martes';
    WHEN  (dato=3) THEN 
      SET dia='miercoles';
     WHEN  (dato=4) THEN 
      SET dia='jueves';
    WHEN  (dato=5) THEN 
      SET dia='viernes';
    WHEN  (dato=6) THEN 
      SET dia='sabado';
     ELSE  
      SET dia='domingo';
END CASE;

    RETURN  dia;

END; 
    //
DELIMITER;

SELECT diassemana(5,@d);

-- 13
DELIMITER //
DROP  PROCEDURE IF EXISTS p13;
CREATE PROCEDURE p13(nomalumno varchar (50))
BEGIN
   DECLARE cantidad int DEFAULT 0;
  
  SELECT COUNT(*) FROM notas n
  WHERE n.nombre=nomalumno;

 
END
//

DELIMITER ;
CALL p13('adrian');

-- 14
DELIMITER //
DROP  PROCEDURE IF EXISTS p14;
CREATE PROCEDURE p14(nomalumno varchar (50))
BEGIN
  DECLARE cantidad int DEFAULT 0;
  DECLARE final int DEFAULT 0;
  DECLARE valorleido int ;
  DECLARE contador int DEFAULT 0;
  
  DECLARE cursor1 CURSOR FOR
    SELECT n.nombre FROM notas n;

   DECLARE CONTINUE HANDLER FOR NOT FOUND 
    SET final=1;

  OPEN cursor1;
FETCH cursor1 INTO valorleido;
    WHILE (final=0) DO
IF (valorleido=nomalumno) THEN 
  set contador=contador+1;
  END IF;

FETCH cursor1 INTO valorleido;
END WHILE;
  CLOSE cursor1;

  SELECT contador;
END
//

DELIMITER ;

CALL p14('ana');
SELECT * FROM notas n;

-- 16
DELIMITER //
DROP  PROCEDURE IF EXISTS p16;
CREATE PROCEDURE p16()
BEGIN
  
  CREATE OR REPLACE TABLE  usuarios(
    id int AUTO_INCREMENT,
    nombre varchar (50),
    PRIMARY KEY (id));

END
//

DELIMITER ;

CALL p16();

-- 17
DELIMITER //
DROP  PROCEDURE IF EXISTS p17;
CREATE PROCEDURE p17(nUsuario varchar (20))
BEGIN
  
  INSERT INTO usuarios(nombre) VALUES(nUsuario);
  

END
//

DELIMITER ;

CALL p17('ana');
CALL p17('Luis');
CALL p17('Lara');
CALL p17('Ceila');
CALL p17('Ramon');
CALL p17('Clara');
SELECT * FROM usuarios u;

-- 18

DELIMITER //
DROP  PROCEDURE IF EXISTS p18;
CREATE PROCEDURE p18()
BEGIN
  
  CALL p16();
  CALL p17();
  

END
//

DELIMITER ;


-- 19 
  DELIMITER//

  CREATE OR REPLACE FUNCTION f19(nombreUsuario varchar (50))
 RETURNS varchar (50)
    BEGIN 

DECLARE n varchar (50);
set n=nombreUsuario;
IF (n IN (SELECT u.nombre FROM usuarios u)) THEN
  set n= 'verdadero';
  ELSE
  set n= 'falso';
  END IF;

    RETURN n ;

END //
DELIMITER;

SELECT f19('ana');

SELECT * FROM usuarios u;

