﻿-- 2. Función área triangulo

   DELIMITER//

  CREATE OR REPLACE FUNCTION areatriangulo
  (base float, altura float )

    RETURNS float
    COMMENT'Realizar una función que reciba como argumentos:
            - Base de un triangulo
            - Altura de un triangulo
        Debe devolver el cálculo del área del triángulo.'
    
    BEGIN 
   DECLARE area float DEFAULT 0;
    SET area=(base*altura)/2;
RETURN area;


    END //
  DELIMITER;

SELECT areatriangulo(10,10);

-- 3 
  DELIMITER //

CREATE OR REPLACE FUNCTION  perimetrotriangulo
  (base float, lado2 float, lado3 float )
  RETURNS float 
  COMMENT 'Realizar una función que reciba como argumentos:
          - Base de un triangulo
          - Lado2 de un triangulo
          - Lado3 de un triangulo
          Debe devolver el cálculo del perímetro del triángulo.'
  BEGIN
DECLARE perimetro float DEFAULT 0;
  SET perimetro=(base+lado2+lado3);

  RETURN perimetro ;
 END //
DELIMITER;

SELECT perimetrotriangulo(10,40,15);

-- 4
   DELIMITER//
DROP PROCEDURE IF EXISTS  ptriangulo//
  CREATE PROCEDURE   ptriangulo
  (id1 float, id2 float)
COMMENT 'Realizar un procedimiento almacenado que cuando le llames como argumentos:
        - Id1: id inicial
        - Id2: id final
        Actualice el área y el perímetro de los triángulos (utilizando las funciones realizadas) que estén comprendidos entre los id
        pasados.'
    BEGIN 
 UPDATE triangulos t
  SET t.area=areatriangulo(t.base,t.altura)
    WHERE id BETWEEN id1 AND id2;
 UPDATE triangulos t
  SET t.perimetro=perimetrotriangulo(id1,id2,t.base)
     WHERE id BETWEEN id1 AND id2;


    END
//
  DELIMITER;

UPDATE triangulos t SET area=NULL;
 CALL ptriangulo(4,7);
SELECT * FROM triangulos t;

-- 5

 DELIMITER//

  CREATE OR REPLACE FUNCTION areacuadrado
  (lado float )

    RETURNS float
    COMMENT'Realizar una función que reciba como argumentos:
            - Lado de un cuadrado
            Debe devolver el cálculo del área'
    
    BEGIN 
   DECLARE area float DEFAULT 0;
    SET area=POW(lado,2);
RETURN area;


    END //
  DELIMITER;

SELECT areacuadrado(5);

-- 6 
DELIMITER//

  CREATE OR REPLACE FUNCTION perimetrocuadrado
  ( lado float )

    RETURNS float
    COMMENT'Realizar una función que reciba como argumentos:
            - Lado de un cuadrado
            Debe devolver el cálculo del perímetro'
    
    BEGIN 
  DECLARE perimetro float DEFAULT 0;
    SET perimetro=4*(lado);


RETURN perimetro;


    END //
  DELIMITER;

SELECT perimetrocuadrado(4);

-- 7 
   DELIMITER//
DROP PROCEDURE IF EXISTS  pcuadrado//
  CREATE PROCEDURE   pcuadrado
  (id1 float, id2 float)
COMMENT 'Realizar un procedimiento almacenado que cuando le llames como argumentos:
        - Id1: id inicial
        - Id2: id final
        Actualice el área y el perímetro de los cuadrados (utilizando las funciones realizadas) que estén comprendidos entre los id pasados'
    BEGIN 
 UPDATE cuadrados c
  SET c.area=areacuadrado(c.lado)
    WHERE c.id BETWEEN id1 AND id2;

UPDATE cuadrados c
  SET c.perimetro=perimetrocuadrado(c.lado)
    WHERE id BETWEEN id1 AND id2;


    END
//
  DELIMITER;

CALL pcuadrado(3,10);
SELECT * FROM cuadrados c;


-- 8 
 DELIMITER//

  CREATE OR REPLACE FUNCTION areacirculo
  (radio float )

    RETURNS float
    COMMENT'Realizar una función que reciba como argumentos:
            - radio
            Debe devolver el cálculo del área'
    
    BEGIN 
   DECLARE area float DEFAULT 0;
    SET area=PI()*POW(radio,2);
RETURN area;


    END //
  DELIMITER;

SELECT areacirculo(5);

-- 9
 DELIMITER//

  CREATE OR REPLACE FUNCTION perimetrocirculo
  (radio float )

    RETURNS float
    COMMENT'Realizar una función que reciba como argumentos:
          - radio
            Debe devolver el cálculo del perímetro'
    
    BEGIN 
   DECLARE perimetro float DEFAULT 0;
    SET perimetro=2*PI()*(radio);
RETURN perimetro;


    END //
  DELIMITER;

SELECT perimetrocirculo(5);

-- 10 
   DELIMITER//
DROP PROCEDURE IF EXISTS  pcirculo//
  CREATE PROCEDURE  pcirculo
  (id1 float, id2 float, letra char(1))

COMMENT 'Realizar un procedimiento almacenado que cuando le llames como argumentos:
          - Id1: id inicial
          - Id2: id final
          - Tipo: puede ser a, b o null.
        Actualice el área y el perímetro de los círculos (utilizando las funciones realizadas) que estén comprendidos entre los id
        pasados y que sea del tipo pasado. Si el tipo es null cogerá todos los tipos.'

    BEGIN 
 UPDATE circulo c
  SET c.area=areacirculo(c.radio)
    WHERE c.id BETWEEN id1 AND id2 AND c.tipo=letra;

UPDATE circulo c
  SET c.perimetro=perimetrocirculo(c.radio)
    WHERE id BETWEEN id1 AND id2 AND c.tipo=letra;


    END
//
  DELIMITER;

UPDATE circulo c set area=0 , c.perimetro=0;

CALL pcirculo(7,10,'a');
SELECT * FROM circulo c;



-- 11
DELIMITER//

  CREATE OR REPLACE FUNCTION media
  (nota1 float, nota2 float,nota3 float, nota4 float )

    RETURNS float
    COMMENT'Realizar una función que reciba como argumentos 4 notas.
            Debe devolver el cálculo de la nota media.'
    
    BEGIN 
   DECLARE media float DEFAULT 0;
    SET media=(nota1+nota2+nota3+nota4)/4;
RETURN media;


    END //
  DELIMITER;

SELECT media(4,8,9,5);

-- 12 
DELIMITER//

  CREATE OR REPLACE FUNCTION minima
  (nota1 float, nota2 float,nota3 float, nota4 float )

    RETURNS float
    COMMENT'Realizar una función que reciba como argumentos 4 notas.
            Debe devolver la nota mínima.'
    
    BEGIN 
   DECLARE minima float DEFAULT 0;
    SET minima= LEAST(nota1,nota2,nota3,nota4);
RETURN minima;


    END //
  DELIMITER;

SELECT minima(4,8,10,6);

-- 13 
DELIMITER//

  CREATE OR REPLACE FUNCTION maximo
  (nota1 float, nota2 float,nota3 float, nota4 float )

    RETURNS float
    COMMENT'Realizar una función que reciba como argumentos 4 notas.
            Debe devolver la nota maxima.'
    
    BEGIN 
   DECLARE maximo float DEFAULT 0;
    SET maximo= GREATEST(nota1,nota2,nota3,nota4);
RETURN maximo;


    END //
  DELIMITER;

SELECT maximo(4,8,10,6);

-- 14
DELIMITER//

  CREATE OR REPLACE FUNCTION moda
  (nota1 float, nota2 float,nota3 float, nota4 float )

    RETURNS float
    COMMENT'Realizar una función que reciba como argumentos 4 notas.
            Debe devolver la nota que más se repite'
    
    BEGIN 
   DECLARE moda float DEFAULT 0;

    CREATE OR REPLACE TEMPORARY TABLE t(
      notas float
      );
    INSERT INTO t VALUES(nota1),(nota2),(nota3),(nota4);
    SELECT notas INTO moda 
    FROM t GROUP BY notas ORDER BY COUNT(*) DESC LIMIT 1;
 RETURN moda;
    

    END //
  DELIMITER;

SELECT moda(1,1,1,2);

-- 15

   DELIMITER//
DROP PROCEDURE IF EXISTS  palumnos//
  CREATE PROCEDURE  palumnos
  (id1 float,
   id2 float)

COMMENT 'Realizar un procedimiento almacenado que cuando le llames como argumentos:
          - Id1: id inicial
          - Id2: id final
          - Tipo: puede ser a, b o null.
        Actualice el área y el perímetro de los círculos (utilizando las funciones realizadas) que estén comprendidos entre los id
        pasados y que sea del tipo pasado. Si el tipo es null cogerá todos los tipos.'

    BEGIN 
  -- actualizo campos de la tabla alumnos

UPDATE alumnos a 
  SET a.media=media(a.nota1,a.nota2,a.nota3,a.nota4)
    WHERE a.id BETWEEN id1 AND id2; 

    UPDATE alumnos a 
  SET a.min=minima(a.nota1,a.nota2,a.nota3,a.nota4)
    WHERE a.id BETWEEN id1 AND id2; 

    UPDATE alumnos a 
  SET a.max=maximo(a.nota1,a.nota2,a.nota3,a.nota4)
    WHERE a.id BETWEEN id1 AND id2; 

  
   UPDATE alumnos a 
  SET a.moda=moda(a.nota1,a.nota2,a.nota3,a.nota4)
    WHERE a.id BETWEEN id1 AND id2; 

    -- actualizo campos de la tabla grupo

    UPDATE grupos g
  SET g.media= (SELECT AVG(media) FROM alumnos WHERE grupo=1
      AND id BETWEEN id1 AND id2) WHERE id=1; 

    UPDATE grupos g
  SET g.media= (SELECT AVG(media) FROM alumnos WHERE grupo=2
      AND id BETWEEN id1 AND id2) WHERE id=2; 


    END
//
  DELIMITER;

-- probando el procedure

UPDATE alumnos a set a.max=0 , a.media=0,a.min=0, a.moda=0;

UPDATE grupos g SET g.media=0;

CALL palumnos(5,10);
SELECT * FROM alumnos a;

SELECT * FROM grupos g;

-- 15 procedimiento almacenado de conversion

  DELIMITER//
DROP PROCEDURE IF EXISTS conversion//
  CREATE PROCEDURE  conversion
  (idinicio float)

COMMENT 'Realizar un procedimiento almacenado que le pasas como argumento un id y te calcula la conversión de unidades de
        todos los registros que estén detrás de ese id.
        Si está escrito los cm calculara m, km y pulgadas. Si está escrito en m calculara cm, km y pulgadas y así consecutivamente.'

    BEGIN 
UPDATE conversion c
set
  m=cm/100,
  km=cm/100000,
  pulgadas=cm/0.393701
    WHERE cm IS NOT NULL AND c.id>idinicio;

    UPDATE conversion c
      set
      c.cm=m*100,
      c.km=m/1000,
      c.pulgadas=cm/0.393701
    WHERE c.m IS NOT NULL  AND c.cm IS NULL  AND c.id>idinicio;

 UPDATE conversion c
      set
      c.cm=c.km*100000,
      c.km=c.km*1000,
      c.pulgadas=cm/0.393701
    WHERE c.km IS NOT NULL  AND c.cm IS NULL  AND c.id>idinicio;


UPDATE conversion c
      set
      c.cm=c.pulgadas*2.54,
      c.km=c.pulgadas*254,
      c.pulgadas=c.pulgadas*254000
    WHERE c.km IS NOT NULL  AND c.cm IS NULL  AND c.id>idinicio;

    END//
  DELIMITER;

