﻿-- 1 


DELIMITER $$
DROP PROCEDURE IF EXISTS ej1 $$
CREATE PROCEDURE ej1(texto varchar(50),caracter char)
BEGIN

    DECLARE valor int;

    SET valor=LOCATE(caracter,texto);

    IF( valor= 0 ) THEN
       SELECT 'no está';
        ELSE 
        SELECT 'está';

    END IF;  
      END
$$

DELIMITER ;

CALL ej1('juan','l');
CALL ej1('maria','a');
CALL 

-- 2
DELIMITER $$
DROP PROCEDURE IF EXISTS ej2;
CREATE OR REPLACE PROCEDURE ej2(texto varchar(25), caracter char)
  BEGIN
SELECT SUBSTRING_INDEX(texto, caracter, 1) caracteresanteriores;
  END
$$

DELIMITER ;

CALL ej2 ('segundo ejemplo', 'j');
CALL ej2('francisco','i');


  -- 3
  DELIMITER $$
DROP PROCEDURE IF EXISTS ej3 $$
CREATE PROCEDURE ej3(num1 int,num2 int, num3 int,OUT num4 int, OUT num5 int)
BEGIN
  SELECT num1,num2,num3,num4,num5;
  SET num4=GREATEST(num1,num2,num3);
  SET num5=LEAST(num1,num2,num3);

SELECT num4;
  SELECT num5;


END
$$

DELIMITER ;

  CALL ej3(15,14,19,@num4,@num5);


  -- 4 que muestre cuantos numeros1 y numeros2 son mayores que 50
 DELIMITER $$
 DROP PROCEDURE IF EXISTS ej4;
CREATE OR REPLACE PROCEDURE ej4()
  BEGIN
SELECT COUNT(numero1)num1 FROM hoja1 h WHERE numero1>50 
  UNION  
 SELECT COUNT(numero2)num2 FROM hoja1 h WHERE numero2 >50;
END
$$

DELIMITER ;


CALL ej4();

SELECT * FROM hoja1 h;


  -- 5
 DELIMITER $$
  DROP PROCEDURE IF EXISTS ej5;
CREATE OR REPLACE PROCEDURE ej5()
  BEGIN
UPDATE hoja1 h JOIN (SELECT id, numero1+numero2 num1 FROM hoja1 h) c1 USING(id)
  SET suma=c1.num1;
  UPDATE hoja1 h JOIN (SELECT id, numero1-numero2 num2 FROM  hoja1 h) c2 USING (id)
    SET resta=c2.num2;  
 END
$$

DELIMITER ;


CALL ej5();
SELECT * FROM  hoja1 h;


  -- 6 
 DELIMITER $$
DROP PROCEDURE IF EXISTS ej6;
CREATE OR REPLACE PROCEDURE ej6()
  BEGIN
  UPDATE hoja1 h 
  SET suma=NULL, resta=NULL;
  UPDATE hoja1 h 
  SET suma=IF(numero1>numero2,numero1+numero2,0);
  UPDATE hoja1 h 
  SET resta=IF(numero1>numero2,numero1-numero2,numero2-numero1);
  END
$$

DELIMITER ;


CALL ej6();
SELECT * FROM  hoja1 h;

-- 7 
   DELIMITER $$
  DROP PROCEDURE IF EXISTS ej7;
CREATE OR REPLACE PROCEDURE ej7()
  BEGIN
  UPDATE hoja1 h 
  SET junto=CONCAT(texto1,' ',texto2);
END
$$

DELIMITER ;

CALL ej7();
SELECT * FROM hoja1 h;

-- 8 
   DELIMITER $$
  DROP PROCEDURE IF EXISTS ej8;
  CREATE OR REPLACE PROCEDURE ej8()
    BEGIN
      UPDATE hoja1 h 
    SET junto=NULL;
      UPDATE hoja1 h 
        SET junto=
        IF (rango='A', CONCAT(texto1,'-',' ',texto2), IF(rango='B', CONCAT(texto1,'+',' ',texto2), texto1));
    END
$$

DELIMITER ;

CALL ej8();
SELECT * FROM hoja1 h;

