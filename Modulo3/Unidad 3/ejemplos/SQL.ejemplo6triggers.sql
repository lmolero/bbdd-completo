﻿USE ejemplo6;

-- 1.- Crear un disparador para la tabla ventas para que cuando metas un registro nuevo te calcule el total automáticamente... 2

  DELIMITER //
  CREATE OR REPLACE TRIGGER t1insert
  BEFORE INSERT
    ON ventas
    FOR EACH ROW
  BEGIN

   
    SET new.total=new.precio*new.unidades;
  
  END // 
  DELIMITER ;



-- 2.- Crear un disparador para la tabla ventas para que cuando inserte un registro me sume el total a la tabla productos (en el
-- campo cantidad).

  DELIMITER //
  CREATE OR REPLACE TRIGGER t2insert
  AFTER INSERT
    ON ventas 
    FOR EACH ROW
  BEGIN
    

  UPDATE  productos p
     SET p.cantidad=p.cantidad+new.total
    WHERE p.producto=NEW.producto;
 
  END // 
  DELIMITER ;

-- 3.- Crear un disparador para la tabla ventas para que cuando actualices un registro nuevo te calcule el total
-- automáticamente.

  DELIMITER //
  CREATE OR REPLACE TRIGGER t2update
  BEFORE UPDATE 
    ON ventas
    FOR EACH ROW
  BEGIN

   
    SET new.total=new.precio*new.unidades;
  
  END // 
  DELIMITER ;

-- 4.- Crear un disparador para la tabla ventas para que cuando actualice un registro me sume el total a la tabla productos (en
-- el campo cantidad)

  DELIMITER //
  CREATE OR REPLACE TRIGGER t2update
  AFTER UPDATE 
    ON ventas 
    FOR EACH ROW
  BEGIN
    

  UPDATE  productos p
     SET p.cantidad=p.cantidad+(new.total-old.total)
    WHERE p.producto=NEW.producto;
 
  END // 
  DELIMITER ;

-- 5.- Crear un disparador para la tabla productos que si cambia el código del producto te sume todos los totales de ese
-- producto de la tabla ventas

DELIMITER //
CREATE OR REPLACE TRIGGER t5
BEFORE UPDATE 
  ON productos
  FOR EACH ROW
BEGIN
  
  SET new.cantidad=(SELECT SUM(total) FROM ventas
                     WHERE NEW.producto=producto);
END // 
DELIMITER ;

-- 6.- Crear un disparador para la tabla productos que si eliminas un producto te elimine todos los productos del mismo código
-- en la tabla ventas

  DELIMITER //
  CREATE OR REPLACE TRIGGER t6
  AFTER DELETE      
    ON productos 
    FOR EACH ROW
  BEGIN

  DELETE FROM ventas WHERE producto=old.producto;
  
  END // 
  DELIMITER ;

-- 7.- Crear un disparador para la tabla ventas que si eliminas un registro te reste el total del campo cantidad de la tabla
-- productos (en el campo cantidad)

  DELIMITER //
  CREATE OR REPLACE TRIGGER t7
  AFTER DELETE     
    ON ventas
    FOR EACH ROW
  BEGIN
    
UPDATE productos
  SET cantidad=cantidad-old.total
    WHERE producto=old.producto;

  END // 
  DELIMITER ;

--  8.- Modificar el disparador 3 para que modifique la tabla productos actualizando el valor del campo cantidad en funcion del
-- total. 

 DELIMITER //
  CREATE OR REPLACE TRIGGER t2update
  AFTER UPDATE 
    ON productos
    FOR EACH ROW
  BEGIN

   
    SET new.total=new.precio*new.unidades;
  
  END // 
  DELIMITER ;