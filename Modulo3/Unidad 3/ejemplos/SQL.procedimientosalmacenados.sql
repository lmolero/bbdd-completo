﻿CREATE DATABASE IF NOT EXISTS procedimientosfunciones1;
USE procedimientosfunciones1;



DELIMITER $$
DROP PROCEDURE IF EXISTS ej1 $$
CREATE PROCEDURE ej1 ()
BEGIN
  SELECT NOW();
END
$$

DELIMITER ;

CALL ej1();

DELIMITER $$
DROP PROCEDURE IF EXISTS ej2 $$
CREATE PROCEDURE ej2 ()
BEGIN
  DECLARE v1 datetime; -- declarando
  SET v1=NOW(); -- asignando
END
$$
DELIMITER ;

CALL ej2();

-- suma dos numeros

DELIMITER $$
DROP PROCEDURE IF EXISTS ej3 $$
CREATE PROCEDURE ej3 (arg1 int, arg2 int)
BEGIN
  DECLARE suma int; -- declarando
  SET suma=arg1+arg2; -- asignando
  SELECT  suma;
END
$$
DELIMITER ;

CALL ej3(3,4);
CALL ej3(6,4);
CALL ej3(7,4);

-- resta dos numeros

DELIMITER $$
DROP PROCEDURE IF EXISTS ej4 $$
CREATE PROCEDURE ej4 (arg1 int, arg2 int)
BEGIN
  DECLARE resta int; -- declarando
  SET resta=arg1-arg2; -- asignando
  SELECT  resta;
END
$$
DELIMITER ;

CALL ej4(3,4);
CALL ej4(6,4);
CALL ej4(7,4);

-- crear una tabla, introducir valores en columnas y que sume las variables

DELIMITER $$
DROP PROCEDURE IF EXISTS ej5 $$
CREATE PROCEDURE ej5 (a1 int, a2 int)
BEGIN
-- declarar las variables luego del begin
  DECLARE suma int DEFAULT 0;

-- craer tabla
    
CREATE TABLE IF NOT EXISTS datos(
  id int AUTO_INCREMENT,
  dato1 int,
  dato2 int,
  suma int,
  PRIMARY KEY(id)
  );

  --  calcular la suma
  SET suma=a1+a2;

  -- introducir los datos en la tabla
  INSERT INTO datos VALUES
    (DEFAULT, a1,a2,suma);

  END
$$
DELIMITER ;

CALL ej5(12,5);
SELECT * FROM datos d;

CALL ej5(12,15);
 -- crear una tabla llamada ej6(id,d1,d2,suma,producto) introducir los datos en la tabla

DELIMITER $$
DROP PROCEDURE IF EXISTS ej6 $$
CREATE PROCEDURE ej6 (d1 int, d2 int)
BEGIN

  DECLARE suma int DEFAULT 0;
  DECLARE  producto int DEFAULT 0;
    
CREATE TABLE IF NOT EXISTS datos2(
  id int AUTO_INCREMENT,
  dato1 int,
  dato2 int,
  suma int,
  producto int,
  PRIMARY KEY(id)
  );

  SET suma=d1+d2;
  SET producto=d1*d2;

  INSERT INTO datos2 VALUES
    (DEFAULT, d1,d2,suma,producto);

  END
$$
DELIMITER ;

CALL ej6(12,5);
SELECT * FROM datos2 d;

-- crear una tabla 

DELIMITER $$
DROP PROCEDURE IF EXISTS ej33 $$
CREATE PROCEDURE ej33 (dato1 int, dato2 int)
BEGIN

   DECLARE resultado int DEFAULT 0;
  CREATE TABLE IF NOT EXISTS tablaej33(
  id int AUTO_INCREMENT,
  dato1 int,
  dato2 int,
  resultado int,
  PRIMARY KEY(id)
  );
 END
$$
DELIMITER ;

-- ej33a
DELIMITER $$
DROP PROCEDURE IF EXISTS ej33a $$
CREATE PROCEDURE ej33a (dato1 int, dato2 int)
BEGIN
  DECLARE elevadocuadrado int; 
  SET elevadocuadrado=POW(dato1,dato2);

   INSERT INTO tablaej33 VALUES
    (DEFAULT, dato1,dato2,elevadocuadrado);

END
$$
DELIMITER ;


CALL ej33();
CALL ej33a(1,2);
SELECT * FROM tablaej33 t;

-- crear raiz cuadrada
-- ej33b
DELIMITER $$
DROP PROCEDURE IF EXISTS ej33b $$
CREATE PROCEDURE ej33b (dato1 int)
BEGIN
  DECLARE raiz int; 
  SET raiz=SQRT(dato1);

   INSERT INTO tablaej33 VALUES
    (DEFAULT, dato1,DEFAULT, raiz); -- default se coloca para indicar que end ato 2 es un campo vacio, sino se coloca da error

END
$$
DELIMITER ;

CALL ej33b(9);
SELECT * FROM  tablaej33 t;          


-- ej34.. crear tabla
  DELIMITER $$
DROP PROCEDURE IF EXISTS ej34 $$
CREATE PROCEDURE ej34 ()
BEGIN
DROP TABLE IF EXISTS ej34;
  CREATE TABLE IF NOT EXISTS tablaej34(
  id int AUTO_INCREMENT,
  texto varchar(50),
  longitud int,
  caracteres varchar(50),
  PRIMARY KEY(id)
  );
 END
$$
DELIMITER ;

CALL ej34();


-- ej34b: insertar texto

DELIMITER $$
DROP PROCEDURE IF EXISTS ej34b $$
CREATE PROCEDURE ej34b(texto varchar (50))
BEGIN 
    INSERT INTO tablaej34(texto) VALUES
    (texto);

END
$$
DELIMITER ;

CALL ej34b('Ana');
CALL ej34b('Jose');
CALL ej34b('Raul');
CALL ej34b('alejandro');
CALL ej34b('santiago');

SELECT * FROM tablaej34 t;

-- ej34a: colocar la longitud del texto

DELIMITER $$
DROP PROCEDURE IF EXISTS ej34a $$
CREATE PROCEDURE ej34a ()
BEGIN

UPDATE tablaej34 t
set longitud=CHAR_LENGTH(texto);

END
$$
DELIMITER ;

CALL ej34a();
SELECT * FROM tablaej34 t;

-- ej34c
  DELIMITER $$
DROP PROCEDURE IF EXISTS ej34c $$
CREATE PROCEDURE ej34c (caracteres varchar(50),registro int) -- ARGUMENTOS
BEGIN

UPDATE tablaej34 t
 JOIN  (SELECT * FROM tablaej34 t LIMIT registro,1000) c1 
  USING (id)
set t.caracteres=LEFT(t.texto,caracteres);

END
$$
DELIMITER ;


CALL ej34c(2,0);
SELECT * FROM tablaej34 t;

-- EJ35(NUMERO)

DELIMITER $$
DROP PROCEDURE IF EXISTS ej35 $$
CREATE PROCEDURE ej35 (numero int)
BEGIN

  IF (numero>10) THEN 
      SELECT 'grande';
    ELSE 
      SELECT 'pequeño';
  END IF;

END
$$
DELIMITER ;

CALL ej35(12);
CALL ej35(2);

-- tabla temporal 4
DELIMITER $$
DROP PROCEDURE IF EXISTS ej4 $$
CREATE PROCEDURE ej4(num1 int,num2 int)

BEGIN

CREATE OR REPLACE TEMPORARY TABLE ej4( -- se le coloca create or replace para que no m de error o colocar drop table if exists ej4;
  id int AUTO_INCREMENT,
  numero int,
  PRIMARY KEY (id));


  INSERT INTO ej4(numero) VALUES 
  (num1),(num2);

  SELECT 
         e.numero FROM ej4 e
  ORDER BY numero DESC;

END
$$
DELIMITER ;

CALL ej4(12,34);

-- ej5

DELIMITER $$
DROP PROCEDURE IF EXISTS ej5 $$
CREATE PROCEDURE ej5(arg1 int,OUT arg2 int,INOUT arg3 int)

BEGIN
SELECT arg1,arg2,arg3;
SET rg1=arg1+50;
SET arg2=arg2+100;
SET arg1=arg3+500;
END
$$
DELIMITER ;


SET @3=10;
CALL ej5(1,@2,@3);









