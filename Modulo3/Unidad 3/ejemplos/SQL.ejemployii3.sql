﻿DROP DATABASE IF EXISTS ejemplo3yii;
CREATE DATABASE IF NOT EXISTS ejemplo3yii;

USE ejemplo3yii;


CREATE OR REPLACE TABLE naciones(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar (20),
  continente varchar (20),
  vintorias int
  ) ;

CREATE OR REPLACE TABLE tenistas(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(20),
  altura int,
  peso int,
  correo varchar (20),
  fechanacimiento date,
  fechabaja date,
  activo bool,
  nacion int,
 CONSTRAINT fknombrenacion FOREIGN KEY (nacion)
REFERENCES naciones(id) ON DELETE CASCADE ON UPDATE CASCADE
  );


