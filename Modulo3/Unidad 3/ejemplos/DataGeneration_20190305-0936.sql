﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product Home Page: http://www.devart.com/dbforge/mysql/studio
-- Script date 05/03/2019 9:39:08
-- Target server version: 5.5.5-10.1.36-MariaDB
-- Target connection string: User Id=root;Host=127.0.0.1;Character Set=utf8
--



SET NAMES 'utf8';
USE fruteria;
--
-- Delete data from the table 'facturas'
--
TRUNCATE TABLE facturas;
--
-- Delete data from the table 'productos'
--
DELETE FROM productos;
--
-- Delete data from the table 'proveedores'
--
DELETE FROM proveedores;

--
-- Inserting data into table proveedores
--
INSERT INTO proveedores(id, nombre, direccion, cp) VALUES
(1, 'FRUCA MARKETING SL', 'CALLEJON DE LA NOCHE TRISTE NO.23', '21546'),
(2, 'FRUTAS TONO SL', 'AV. INDEPENDENCIA NO. 500', '32514'),
(3, 'Indasol S.a.t.\r\n', 'AV. 20 DE NOVIEMBRE NO.1024', '32587'),
(4, 'FRUTAS TONO SL', 'AV. 20 DE NOVIEMBRE NO.1024', '32547'),
(5, 'Agronativa Sl', 'CALLE AGUSTIN LARA NO. 69-B', '78942'),
(6, 'Indasol S.a.t.\r\n', 'AV. INDEPENDENCIA NO. 241', '36987'),
(7, 'FRUTAS TONO SL', 'AV. INDEPENDENCIA NO. 779', '6845'),
(8, 'FRUTAS TONO SL', 'CALLE AGUSTIN LARA NO. 69-B', '36987'),
(9, 'Indasol S.a.t.\r\n', 'AV. INDEPENDENCIA NO. 779', '78945'),
(10, 'CULTIVAR SA', 'AV. INDEPENDENCIA NO. 500', '12549'),
(11, '', 'CALLE AGUSTIN LARA NO. 69-B', '32514'),
(12, 'FRUTAS TONO SL', 'CALLE AGUSTIN LARA NO. 69-B', '68370'),
(13, 'Vicasol Sca', 'LAZARO CARDENAS NO.195', '45962'),
(14, 'FRUCA MARKETING SL', 'AV 5 DE MAYO 659', '65489'),
(15, 'Dai Fruits Sl', 'AV. INDEPENDENCIA NO. 500', '78942'),
(16, 'Agronativa Sl', 'AV. INDEPENDENCIA NO. 241', '32154'),
(17, 'Indasol S.a.t.\r\n', 'CALLEJON DE LA NOCHE TRISTE NO.23', '65489'),
(18, 'Dai Fruits Sl', 'AV 5 DE MAYO 659', '36987'),
(19, 'Dai Fruits Sl', 'CALLE AGUSTIN LARA NO. 69-B', '65489'),
(20, 'Indasol S.a.t.\r\n', 'AV. 20 DE NOVIEMBRE NO.1024', '45962');

--
-- Inserting data into table productos
--
INSERT INTO productos(codigo, descripcion, stock, precio, CodProveedor) VALUES
(1, 'fresas', 140, 3, 2),
(2, 'uvas', 246, 8, 13),
(3, 'platano', 31, 7, 11),
(4, 'melocoton', 239, 7, 11),
(5, 'frambuesas', 106, 12, 12),
(6, 'arandanos', 35, 3, 9),
(7, 'manzana', 62, 6, 8),
(8, 'kiwi', 77, 11, 10),
(9, 'frambuesas', 141, 14, 3),
(10, 'naranja', 228, 1, 2),
(11, 'mandarina', 188, 10, 18),
(12, 'melocoton', 41, 5, 1),
(13, 'manzana', 34, 4, 4),
(14, 'naranja', 108, 8, 16),
(15, 'melon', 123, 5, 1),
(16, 'uvas', 133, 13, 17),
(17, 'uvas', 173, 9, 10),
(18, 'melon', 140, 15, 2),
(19, 'uvas', 77, 10, 18),
(20, 'manzana', 237, 1, 11);

--
-- Inserting data into table facturas
--
INSERT INTO facturas(numero, codProducto, cantidad, importe) VALUES
(1, 19, 208, 9636),
(2, 14, 267, 7747),
(3, 6, 94, 8146),
(4, 12, 65, 5563),
(5, 13, 219, 8587),
(6, 19, 148, 6749),
(7, 1, 227, 9659),
(8, 10, 63, 7726),
(9, 6, 122, 7315),
(10, 16, 260, 7961),
(11, 13, 71, 4913),
(12, 10, 279, 5763),
(13, 19, 138, 7630),
(14, 4, 207, 9434),
(15, 4, 274, 8271),
(16, 5, 219, 1658),
(17, 10, 76, 7187),
(18, 16, 197, 4890),
(19, 6, 208, 1300),
(20, 16, 54, 3645);