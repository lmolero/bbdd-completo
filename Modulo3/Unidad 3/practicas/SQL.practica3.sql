﻿DROP DATABASE IF EXISTS Viviendas;
CREATE DATABASE IF NOT EXISTS Viviendas;

USE Viviendas;
-- 2

DELIMITER //
DROP PROCEDURE IF EXISTS n1//
CREATE PROCEDURE n1(numero int)
  COMMENT 'crear procedimiento al cual pasas un numero'
  BEGIN
 DECLARE esta int DEFAULT 0;

  SELECT COUNT(*) INTO esta FROM zonaurbana z WHERE z.NombreZona=numero;

  IF(esta=1) THEN 
    SELECT 'esta dentro de la tabla';
  ELSE
    SELECT 'no esta dentro de la tabla';
  END IF;
 
  END //
DELIMITER;

CALL n1(3);
SELECT * FROM zonaurbana z;

-- 3

DELIMITER //
DROP PROCEDURE IF EXISTS p2//
CREATE PROCEDURE p2(nombrez int, catego char(20))
  COMMENT 'crear procedimiento llamarlo p2, que introduzca valores en la tabla zonaurbana'
  BEGIN

INSERT INTO zonaurbana VALUES(nombrez,catego);

  END //
DELIMITER;

CALL p2(1,1);
SELECT * FROM zonaurbana z;

-- 4
  DELIMITER//
DROP FUNCTION IF EXISTS f3;
  CREATE FUNCTION f3()
 RETURNS int
    BEGIN 
DECLARE cuenta int;
SET cuenta=(SELECT COUNT(*)  FROM zonaurbana z);
 
    RETURN cuenta;

END; 
    //
DELIMITER;

SELECT f3();

-- 5
DELIMITER //
DROP PROCEDURE IF EXISTS procedimiento5;
CREATE PROCEDURE procedimiento5 (cuenta int)
  COMMENT 'crear procedimiento que utilice el valor devuelto por la funcion f3,
            inserta un registro en casaparticular siempre que haya algun registro en la tabla zonaurbana'
  BEGIN

  DECLARE resultado int DEFAULT 0;
  SET resultado = (
    SELECT
      COUNT(*)
    FROM zonaurbana
    WHERE nombrezona = cuenta);
  IF resultado > 0 THEN
    SELECT
      'Existe el registro' registro;
  ELSE
    SELECT
      'No existe el registro' registro;
  END IF;
END
//

DELIMITER ;


CALL procedimiento5(5);
SELECT * FROM casaparticular c;


-- procedimiento ramon
 DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio5//
CREATE PROCEDURE ejercicio5 (nombreZona int, numero_entrada int, metros int)
BEGIN
  DECLARE numero int DEFAULT 0;

  SET numero = f3();

  -- si no hay zonas no puedo hacer nada
  IF numero > 0 THEN
    SELECT COUNT(*) INTO numero FROM 
      zonaurbana z 
      WHERE z.NombreZona=nombreZona;
    -- la zona urbana existe??
    IF (numero=1) THEN
      INSERT INTO casaParticular
        VALUE (nombreZona, numero_entrada, metros);
    ELSE
      -- mensaje a pantalla
      -- SELECT "no existe esa zona urbana";

      -- mensaje a canal de errores
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'no existe esa zona urbana';
    END IF;
  ELSE 
    -- mensaje a pantalla
    -- SELECT "no hay zonas urbanas";
     -- mensaje a canal de errores
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'no hay zonas urbanas';
  END IF;
END//
DELIMITER ;

CALL ejercicio5(625, 6, 120);
SELECT * FROM casaparticular c;
SELECT * FROM zonaurbana z;
DELETE FROM zonaurbana;
CALL p2('ccc');

 /* Ejercicio 6 
Crear un vista que muestre todos los campos de zona urbana 
junto con los campos numero y m2 de la tabla casa particular 
de los registros asociados
  */


CREATE OR REPLACE VIEW v1 AS
SELECT
  `zu`.`NombreZona` AS `NombreZona`,
  `zu`.`Categoria` AS `Categoria`,
  `c`.`Numero` AS `Numero`,
  `c`.`M2` AS `M2`
FROM (`zonaurbana` `zu`
  JOIN `casaparticular` `c`
    ON ((`zu`.`NombreZona` = `c`.`NombreZona`)));

/**
  Ejercicio 7
  Crear un procedimiento, llamarlo p6, 
  para que introduzca de forma automatica 100 registros 
  en la tabla ZonaUrbana. 
  Para crear valores aleatorios podeis utilizar la 
  funcion predefinida llamada RAND() 
  (esta devuelve un numero entre 0 y 1). 
  Para redondear un numero utilizar la funcion ROUND(numero).
**/
DELIMITER //
CREATE OR REPLACE PROCEDURE p6()
  BEGIN
    DECLARE temp int DEFAULT 0;
    DECLARE error int DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR 1062 SET error=1;
      
    
    WHILE (temp<3) DO
        -- intento introducir el registro
        INSERT INTO ZonaUrbana(NombreZona) 
          VALUES (ROUND(RAND()*10));
        -- ha entrado??
        IF(error=0) THEN 
          SET temp=temp+1;
        ELSE 
          set error=0;
         END IF;
    END WHILE;

  END //
DELIMITER ;

CALL p6();
SELECT * FROM ZonaUrbana;
DELETE FROM zonaurbana;



/* Ejercicio 8. Procedimiento que introduzca 100 registros en CasaParticular.
                Crear funcion que seleccione un valor del campo NombreZona de la tabla ZonaUrbana */

DELIMITER //
CREATE OR REPLACE FUNCTION f4()
  RETURNS int
  COMMENT 'Esta función selecciona un valor aleatorio del campo NombreZona de la tabla ZonaUrbana'
  BEGIN
    DECLARE nRegistros int;
    DECLARE valor int;
      set nRegistros=(SELECT COUNT(*) FROM ZonaUrbana);
      set valor=(RAND()*nRegistros);
  RETURN valor;
  END //
DELIMITER ;

SELECT f4();


/* Ejercicio 9: Crear un procedimiento que utilizando un cursor te muestre los registros del 1 al 10 */
DELIMITER //
CREATE OR REPLACE PROCEDURE p8()
BEGIN
  DECLARE v1 int;
  DECLARE incr int DEFAULT 0;
  DECLARE v2 char(2);
  DECLARE c1 CURSOR FOR

  SELECT z.NombreZona,
         z.Categoría FROM ZonaUrbana z;

  OPEN c1;

  WHILE (incr < 10) DO
    FETCH c1 INTO v1, v2;
    SELECT v1 NombreZona, v2 Categoria;

    SET incr = incr + 1;
  END WHILE;
  CLOSE c1;
END //
DELIMITER ;

CALL p8();

/* Ejercicio 10: Introducir datos utilizando un script en la tabla bloque casas */
SELECT * FROM BloqueCasas;

INSERT INTO bloquecasas VALUES
('C/ Principal', 4, 5, 20),
('C/ Principal', 7, 4, 66),
('C/ Principal', 9, 9, 30),
('C/ Principal', 16, 5, 85),
('C/ Principal', 33, 5, 32),
('C/ Principal', 45, 2, 42),
('C/ Principal', 67, 7, 51),
('C/ Principal', 83, 6, 66),
('C/ Principal', 90, 7, 81),
('C/ Principal', 99, 9, 83);

SELECT * FROM BloqueCasas;

/* Ejercicio 11: Crear un procedimiento, llamarlo p10, donde podamos introducir datos de forma automática
                 en la tabla piso. Tener en cuenta que los campos numero y calle tenéis que sacarlos de la
                 tabla bloque casas. Introducir 23 registros. */
DELIMITER //
CREATE OR REPLACE PROCEDURE p10()
BEGIN
  DECLARE calle int;
  DECLARE piso int;
  DECLARE numpisos int;
  DECLARE incr int DEFAULT 0;
  DECLARE v1 int;
  DECLARE n int;
  DECLARE error int DEFAULT 0;

  DECLARE CONTINUE HANDLER FOR 1062
    SET error=1;

    SET v1=(SELECT COUNT(*) FROM BloqueCasas bc);

    

  WHILE (incr<23) DO
    SET n=(ROUND(RAND()*v1));

    SELECT
      bc.Calle,
      bc.Número,
      bc.Npisos INTO calle, piso, numpisos
    FROM BloqueCasas bc LIMIT n,1;

    INSERT INTO Piso
      VALUES (calle, piso, ROUND(RAND()*numpisos)+1, ROUND(RAND()*6)+50, ROUND(RAND()*400)+200);
     IF (error=1) THEN 
      SET error=0;
     ELSE 
      SET incr=incr+1; 
     END IF;
  END WHILE;

END //
DELIMITER ;

CALL p10();

SELECT * FROM BloqueCasas bc;
SELECT * FROM piso p;
DELETE FROM piso;

/* Ejercicio 12: Crear un procedimiento al cual le pasas una fecha en el primer argumento y
                 que te retorna tres argumentos (lógicamente de salida) con el día,
                 mes y año de la fecha generada */
DELIMITER //
CREATE OR REPLACE PROCEDURE p11(IN fecha date, OUT dia int, OUT mes int, OUT anho int)
BEGIN
  SET dia=DAY(fecha);
  SET mes=MONTH(fecha);
  SET anho=YEAR(fecha);

  SELECT dia;
  SELECT mes;
  SELECT anho;

END //
DELIMITER ;


CALL p11('2017-11-10',@dia,@mes,@anho);

/* Ejercicio 13: Crear una función a la cual le pasas una fecha y te devuelve el numero de años
                 que tiene esa persona teniendo en cuenta la fecha actual */
DELIMITER //
CREATE OR REPLACE FUNCTION f5(fecha date)
RETURNS int
BEGIN
  DECLARE anhos int DEFAULT 0;

  SET anhos=YEAR(NOW())-YEAR(fecha);

  IF (MONTH(NOW())<MONTH(fecha)) THEN
    SET anhos=anhos-1;
  ELSEIF (MONTH(NOW())=MONTH(fecha)) THEN
    IF (DAY(NOW())<DAY(fecha)) THEN
      SET anhos=anhos-1;
    END IF;
  END IF;
  RETURN anhos;
END //
DELIMITER ;

SELECT f5('2017/10/11');

/* Ejercicio 14: Crear una función a la cual le pasas un nombre y te devuelve
                 el nombre sin espacios en blanco en la derecha y en la izquierda
                 (utilizar las funciones RTRIM y LTRIM) */
DELIMITER //
CREATE OR REPLACE FUNCTION f6(nombre varchar(30))
RETURNS varchar(30)
BEGIN
  DECLARE juntar varchar(30);

  set juntar=RTRIM(LTRIM(nombre));

  RETURN juntar;
END //
DELIMITER ;

SELECT f6('   Pepe');









