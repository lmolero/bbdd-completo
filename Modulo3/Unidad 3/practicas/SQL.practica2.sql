﻿-- 1- Explicar el funcionamiento del siguiente procedimiento y después ejecutarle

  DELIMITER //
DROP PROCEDURE IF EXISTS f1//
CREATE PROCEDURE f1()
  COMMENT 'crear procedimiento y después ejecutarle'
  BEGIN
CREATE TABLE tabla1(
  nombre varchar(10) DEFAULT NULL,
  edad int(3) DEFAULT 0,
  fecha_nacimiento date DEFAULT NULL
  );
  END //
DELIMITER;

CALL f1();

-- 2
  DELIMITER //
DROP PROCEDURE IF EXISTS creartabla//
CREATE PROCEDURE creartabla()
  COMMENT 'Modificar el procedimiento anterior para crear una tabla con los siguientes campos:
              Dni: decimal(8)
              Nombre: varchar(30)
              Edad: integer
              Apellidos: varchar(50)
              Fecha_nacimiento: date
              Llamar al procedimiento crear_tabla.'
  BEGIN

CREATE TABLE tabla2(
 dni  varchar (8) DEFAULT NULL,
 nombre  varchar(30),
 edad  integer,
 apellidos varchar(50),
fecha_nacimiento date,
PRIMARY KEY(dni)
  );
  END //
DELIMITER;

CALL creartabla();

-- 3
  DELIMITER//
DROP PROCEDURE IF EXISTS f2//
CREATE PROCEDURE f2()
  BEGIN
  DECLARE v1 int;
  SET v1=(SELECT COUNT(*) FROM tabla1);
  IF (v1=0 ) THEN
  SELECT 'la tabla está vacia' salida;
  ELSE
  SELECT 'la tabla no está vacia' salida;
  END IF;
  END //
DELIMITER;

CALL f2();

/* 4 Modificar el procedimiento anterior para que si le pasamos un 0 te evalua la tabla denominada tabla1, si es un 1
evalúa la tabla denominada personas. Si le pasa cualquier otro valor entonces debe indicar valor incorrecto.*/

 
-- 5

  DELIMITER//
DROP PROCEDURE IF EXISTS f3//
CREATE PROCEDURE f3()
  COMMENT '- Explicar el funcionamiento del siguiente procedimiento y después ejecutarle '
  BEGIN
  DECLARE v1 int;


  SET v1=(SELECT COUNT(*) FROM tabla1);
  IF (v1=0 ) THEN
  SELECT 'la tabla está vacia' salida;
  INSERT INTO tabla1 VALUES(DEFAULT,DEFAULT,DEFAULT);
  ELSEIF (v1 BETWEEN 10 AND 100) THEN 
  SELECT * FROM tabla1 t;
  ELSE
  SELECT * FROM tabla1 t LIMIT 1,100;
  END IF;
  END //
DELIMITER;

CALL f3();
SELECT * FROM tabla1 t;

-- 6
   DELIMITER//
DROP FUNCTION IF EXISTS funcion_p2;
  CREATE FUNCTION funcion_p2()
 RETURNS int
    COMMENT 'Transformar el procedimiento anterior en una función'
    BEGIN 

DECLARE f2 float;
   

  SET f2=(SELECT COUNT(*) FROM tabla1);

    RETURN f2;
END; 
    //
DELIMITER;

SELECT funcion_p2();

-- 7
    DELIMITER//
DROP PROCEDURE IF EXISTS crear_claves//
CREATE PROCEDURE crear_claves()
  COMMENT 'Realizar un procedimiento, denominado crear_claves, que genere las siguientes claves y restricciones en la tabla
           personas:
            a. Clave principal : dni
            b. Clave única: nombre y apellidos
            c. Indexado con duplicados: fecha_nacimiento.'
  BEGIN
  
ALTER TABLE tabla2 ADD UNIQUE KEY (nombre,apellidos);
ALTER TABLE tabla2 ADD INDEX (fecha_nacimiento);


  END //
DELIMITER;

CALL crear_claves();

-- 8
   DELIMITER//
DROP PROCEDURE IF EXISTS funcionedad_p2//
CREATE PROCEDURE funcionedad_p2()
  COMMENT 'Crear un procedimiento que actualice todos los campos edad calculándolos en función de la fecha de nacimiento.'
  BEGIN
  
  UPDATE tabla2 t
    SET t.edad=TIMESTAMPDIFF(year,t.fecha_nacimiento,CURDATE());

  END //
DELIMITER;

CALL funcionedad_p2();

SELECT * FROM tabla2 t;
DELETE FROM tabla2;

INSERT INTO tabla2 VALUES('9514561','juan',0,'Parra','1980/01/01');

-- 9

  DELIMITER  //
DROP PROCEDURE IF EXISTS vista_p2;
CREATE PROCEDURE vista_p2()
  COMMENT 'Crear una vista que nos permita sacar todas las personas menores de 18 años. Sacar nombre, apellidos y edad.
           Grabar los registros en una tabla denominada menores. El resto de registros grabarlos en una tabla denominada
            no_menores. Esto realizarlo en un guion denominado los_menores.sql.'
  BEGIN
 CREATE OR REPLACE VIEW personasmenores AS 
  SELECT t.nombre, t.apellidos, t.edad
     FROM tabla2 t
       WHERE t.edad<18;  

CREATE OR REPLACE TABLE menores(
  nombre   varchar(30),
  apellidos varchar (50),
  edad integer
  );


CREATE OR REPLACE TABLE no_menores(
  nombre   varchar(30),
  apellidos varchar (50),
  edad integer
  );

INSERT INTO menores SELECT * FROM personasmenores ;
INSERT INTO no_menores SELECT t.nombre,t.apellidos,t.edad FROM tabla2 t 
 LEFT JOIN personasmenores p USING (nombre,apellidos)
WHERE t.edad IS NULL;

  END
 //
DELIMITER;

CALL vista_p2();

-- 10

 DELIMITER  //
DROP PROCEDURE IF EXISTS Ncompletos_p2;
CREATE PROCEDURE Ncompletos_p2()
  COMMENT 'Crear una vista denominada nombres con los nombres y apellidos de todas las personas. Esta vista debe tener un
          solo campo denomina nombre_completo con el siguiente formato
          apellidos, nombre.'
  BEGIN
 CREATE OR REPLACE VIEW Ncompleto AS
 SELECT concat_ws(' ', apellidos, nombre) as nombre_completo FROM tabla2 t;

  END
 //
DELIMITER;

-- 11


-- rellenar tabla del 1 al 1000.
  DELIMITER //
DROP PROCEDURE IF EXISTS crear_numero 
CREATE PROCEDURE crear_numero ()

BEGIN

  CREATE OR REPLACE TABLE tnumeros(
    id int AUTO_INCREMENT,
    numeros int DEFAULT 0,
    PRIMARY KEY (id)
    );

  DECLARE contador int DEFAULT 1;

 WHILE (contador<=1000) DO
     SET contador=contador+1;
 INSERT INTO tnumeros VALUE(DEFAULT, numeros);
  SET contador=contador+1;
    END WHILE;
END
//
DELIMITER ;

CALL 

DELIMITER //

CREATE PROCEDURE IF NOT  EXISTS  c_alfabeto ()
COMMENT 'Procedimiento para el listado del abecedario letra a letra'
BEGIN
  DECLARE contador int DEFAULT 0;
  DECLARE alfabeto varchar(50) DEFAULT 'abcdefghijklmnñopqrstuvwxyz';
  DECLARE registros int DEFAULT 0;
  SET registros = LENGTH(alfabeto);

  CREATE TABLE IF NOT EXISTS abecedario (
    letras char(1)
  );
  REPEAT
    SET contador = contador + 1;
    INSERT INTO abecedario
    SET letras = SUBSTRING(alfabeto, contador, 1);
  UNTIL contador = registros
  END REPEAT;
END
//
DELIMITER ;

