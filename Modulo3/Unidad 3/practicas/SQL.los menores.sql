﻿-- 9

  DELIMITER  //
DROP PROCEDURE IF EXISTS vista_p2;
CREATE PROCEDURE vista_p2()
  COMMENT 'Crear una vista que nos permita sacar todas las personas menores de 18 años. Sacar nombre, apellidos y edad.
           Grabar los registros en una tabla denominada menores. El resto de registros grabarlos en una tabla denominada
            no_menores. Esto realizarlo en un guion denominado los_menores.sql.'
  BEGIN
 CREATE OR REPLACE VIEW personasmenores AS 
  SELECT t.nombre, t.apellidos, t.edad
     FROM tabla2 t
       WHERE t.edad<18;  

CREATE OR REPLACE TABLE menores(
  nombre   varchar(30),
  apellidos varchar (50),
  edad integer
  );


CREATE OR REPLACE TABLE no_menores(
  nombre   varchar(30),
  apellidos varchar (50),
  edad integer
  );

INSERT INTO menores SELECT * FROM personasmenores ;
INSERT INTO no_menores SELECT t.nombre,t.apellidos,t.edad FROM tabla2 t 
 LEFT JOIN personasmenores p USING (nombre,apellidos)
WHERE t.edad IS NULL;

  END
 //
DELIMITER;

CALL vista_p2();
