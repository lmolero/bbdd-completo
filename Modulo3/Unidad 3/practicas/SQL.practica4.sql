﻿DROP DATABASE IF EXISTS practica4;
CREATE DATABASE practica4;

USE practica4;

/* 1  Crea un procedimiento que reciba una cadena que puede contener letras y números y sustituya los números por *. El
argumento debe ser único y debe almacenar en el, el resultado*/

  DELIMITER //
DROP PROCEDURE IF EXISTS p1;
CREATE PROCEDURE p1 (texto varchar(25))
BEGIN
 
DECLARE a int DEFAULT 1;
DECLARE b char (1);
DECLARE textosalida varchar (25) DEFAULT texto;

  WHILE (a<=CHAR_LENGTH(texto)) DO
SET b=SUBSTRING(texto,a,1);
IF b IN ('1','2','3','4','5','6','7','8','9','0') THEN 
  SET textosalida=INSERT(textosalida,a,1,'*');
  END IF;
 SET a=a+1;
END WHILE;

SET texto=textosalida;

END
//

DELIMITER ;

SET @texto='125abcdf1250';
CALL p1(@texto);
SELECT @texto;

/* 2 Crea una función que recibe como argumento una cadena que puede contener letras y número y debe devolver la
cadena con el primer carácter en mayúsculas y el resto en minúsculas*/

  DELIMITER//

CREATE OR REPLACE FUNCTION funcion_p4_2(texto varchar(50))
RETURNS varchar (50)
BEGIN
  DECLARE textoentrada varchar(50);

SET textoentrada= CONCAT(UPPER(LEFT(texto, 1)), LOWER(SUBSTRING(texto, 2)));
 

  RETURN textoentrada;

END//
DELIMITER;

SELECT funcion_p4_2('compañía anonima');

/* 3. Desarrollar una función que devuelva el número de años completos que hay entre dos fechas que se pasan como
parámetros. Utiliza la función DATEDIFF.*/

DELIMITER //
CREATE OR REPLACE FUNCTION funcion_p4_3(fecha1 date,fecha2 date)
  RETURNS int
  BEGIN
 DECLARE años int ;

  SET años=FLOOR(DATEDIFF(fecha1,fecha2)/365) ;

RETURN años;
  END//
DELIMITER;

SELECT funcion_p4_3('2019/06/02','2001/02/02');

/*4. Realiza el ejercicio anterior pero utilizando la función TIMESTAMPDIFF*/

DELIMITER //
CREATE OR REPLACE FUNCTION funcion_p4_4(fecha1 date, fecha2 date)

RETURNS int
BEGIN 
DECLARE años int;
SET años=TIMESTAMPDIFF(YEAR,fecha1,fecha2);

RETURN años;
  END //
DELIMITER;

SELECT funcion_p4_4('2019/06/02','2001/02/02');

/* 5 Crear un procedimiento que genere una tabla llamada números (un solo campo de tipo int) la rellene de 50 números y
después devuelva su suma, media, valor máximo y valor mínimo.*/

  DELIMITER //
DROP PROCEDURE IF EXISTS p5;
CREATE PROCEDURE p5()
BEGIN

  DECLARE contador int DEFAULT 0;
 DECLARE suma int;
  DECLARE media int;
  DECLARE maxi int;
  DECLARE mini int;

DROP TABLE IF EXISTS numeros;
 
  CREATE  TABLE IF NOT EXISTS numeros(
  num int PRIMARY KEY
  );
 
   
WHILE (contador<=50) do
    INSERT INTO numeros(num) VALUES(contador);
   SET contador=contador+1 ; 
  END WHILE ;

  SET suma=(SELECT SUM(n.num) FROM numeros n);
    SET media=(SELECT avg(n.num) FROM numeros n);
    SET maxi=(SELECT max(n.num) FROM numeros n);
    SET mini=(SELECT min(n.num) FROM numeros n);
 
 SELECT suma;
  SELECT media;
  SELECT maxi;
  SELECT mini;



END
//

DELIMITER ;

TRUNCATE TABLE numeros;
CALL p5();

SELECT * FROM numeros n;

/* 6- Crear una función que reciba 5 números y devuelva su valor máximo, mínimo, media y suma  */

DELIMITER//
DROP FUNCTION IF EXISTS f6;
  CREATE FUNCTION f6(n1 int,n2 int, n3 int, n4 int, n5 int)
 RETURNS  varchar (50)
    BEGIN 
DECLARE maxi int;
DECLARE mini int;
DECLARE media int;
DECLARE suma int;
DECLARE resultado varchar (50);

  SET suma=(n1+n2+n3+n4+n5);
    SET media=(n1+n2+n3+n4+n5)/5;
    SET maxi=GREATEST(n1,n2,n3,n4,n5);
    SET mini=LEAST(n1,n2,n3,n4,n5);
 
SET resultado =  CONCAT(suma,' ', media, ' ', maxi,' ', mini);
  RETURN resultado;
     

END; 
    //
DELIMITER;

SELECT f6(5,8,7,3,9);


/* 7- Realizar un procedimiento que genere una tabla con dos campos (nota de tipo int, resultado de tipo varchar). Debe
rellenar esa tabla con 10 números entre 0 y 10 (con el procedimiento).
 */

DELIMITER //
DROP PROCEDURE IF EXISTS p7;
CREATE PROCEDURE  p7()
 BEGIN
   DECLARE contador int DEFAULT 0;
  DECLARE valor int;
-- creo la tabla

 CREATE OR REPLACE TABLE tablap7(
  nota int ,
  resultado varchar (25)
  );
-- creo los numeros aleatorios para las notas

 REPEAT
    SET valor=(SELECT ROUND(RAND()*10));

SET contador=contador+1;
INSERT INTO tablap7(nota) VALUES(valor);
    UNTIL contador=10

END REPEAT;

 
  END //
DELIMITER;

CALL p7();

SELECT * FROM tablap7 t;

-- 8- Realizar un procedimiento almacenado almacene en el campo resultado el siguiente valor en función de nota
-- a. Nota entre 0 y 4: Suspenso
-- b. Nota 5: Suficiente
-- c. Nota 6: Bien
-- d. Nota entre 7 y 8: Notable
-- e. Nota entre 9 y 10: Sobresaliente

  DELIMITER //

CREATE OR REPLACE  PROCEDURE  p8()
 BEGIN

DECLARE resultadonota varchar (20);

  UPDATE tablap7 t
  SET t.resultado=  
      CASE
  WHEN t.nota BETWEEN 0 AND 4 THEN 'suspenso'
    WHEN t.nota=5 THEN 'suficiente'
    WHEN t.nota=6 THEN 'bien'
    WHEN t.nota BETWEEN 7 AND 8 THEN 'notable'
   ELSE 'sobresaliente'
    END;
   
 
  END //
DELIMITER;

CALL p8(); 
  SELECT * FROM tablap7 t;


-- 9- Crear un procedimiento almacenado que genere estas dos tablas
  DELIMITER //
DROP PROCEDURE IF EXISTS p9;
CREATE PROCEDURE  p9()
 BEGIN
  
CREATE OR REPLACE TABLE comisiones(
  vendedor int,
  comision float
  );

CREATE OR REPLACE TABLE ventas(
  vendedor int,
  producto int,
  importe float
  );

 
  END //
DELIMITER;

CALL p9();

-- 10- Analizar el siguiente procedimiento almacenado

  DELIMITER //
CREATE OR REPLACE  PROCEDURE p10( IN mivendedor int)
  BEGIN
   DECLARE micomision int DEFAULT 0;
  DECLARE suma int;
  DECLARE existe bool;

  SELECT 
  IFNULL (SUM(v.importe),0) INTO suma 
  from ventas v WHERE v.producto=1 AND v.vendedor=mivendedor;
  SET micomision=micomision+(suma *0.15);

  SELECT 
  IFNULL (SUM(v.importe),0) INTO suma 
  FROM ventas v
  WHERE v.producto=2 AND v.vendedor=mivendedor;
  SET micomision=micomision+(suma *0.1);

  SELECT
  IFNULL (SUM(v.importe),0) INTO suma 
  FROM ventas v
  WHERE v.producto=3 AND v.vendedor=mivendedor;
  SET micomision=micomision+(suma *0.2); 

 /* 12- Modificar el procedimiento almacenado para que en caso de que sea un producto de tipo 4 le coloquemos una
 comisión del 30% de sus ventas sobre ese producto. */

  SELECT
  IFNULL (SUM(v.importe),0) INTO suma 
  FROM ventas v
  WHERE v.producto=4 AND v.vendedor=mivendedor;
  SET micomision=micomision+(suma *0.3); 

  SELECT
 COUNT(1)>0 INTO existe FROM comisiones c 
  WHERE c.vendedor=mivendedor;


  IF existe THEN 
  UPDATE comisiones c
  SET c.comision=c.comision+ micomision
  WHERE c.vendedor=mivendedor;
  ELSE 
  INSERT INTO comisiones (vendedor, comision)
  VALUES(mivendedor, micomision);

  END IF;

  END //
  DELIMITER;

 CALL p10(2);
CALL p10(1);
CALL p10(3);
CALL p10(4);

-- 13- Introducir una serie de productos y comprobar que funciona correctamente este procedimiento

INSERT INTO ventas VALUES('1', '2', '5'), ('2', '3', '15'), ('2', '4', '15'), ('2', '3', '3'), ('1', '3', '20'),
('3', '3', '10'), ('4', '4', '15');

TRUNCATE TABLE comisiones;

SELECT * FROM comisiones c;
SELECT * FROM ventas v;



