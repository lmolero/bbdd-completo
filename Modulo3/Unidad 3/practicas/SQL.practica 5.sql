﻿/* Practica 5 */

  DROP DATABASE IF EXISTS practica5;
  CREATE DATABASE IF NOT EXISTS practica5;
  USE practica5;

-- 1- Crear un procedimiento almacenado que genere una tabla con los campos
-- a. Id : entero, clave principal
-- b. Nombre: texto

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p1()
  BEGIN

-- 2- Colocar en el procedimiento anterior un control de errores de tal forma que si la tabla ya existe en vez de crearla
-- la vacía
DECLARE CONTINUE HANDLER FOR 1050

   BEGIN
TRUNCATE nombre;
  END;

    CREATE OR REPLACE TABLE nombre(
      id int  PRIMARY KEY,
      nombre varchar(20)
      );
  
  END //
 DELIMITER;

CALL p1();

-- Crea una función que la pasamos dos argumentos (id, nombre). La función debe introducir el dato en la tabla en
-- caso de que el id no exista. Si el Id existe entonces lo que realizara es actualizarle con el nombre que pasemos.
-- Realizar el ejercicio sin control de errores

DELIMITER //
  CREATE  FUNCTION f1 (vid int, vnombre varchar (20) )
    RETURNS int 
  BEGIN

 IF (id<>vid )THEN 
INSERT INTO nombre(id,nombre) VALUES(vid,vnombre);
ELSE 
 UPDATE nombre n
  SET n.nombre= vnombre
  WHERE n.id=vid;
END IF;
    RETURN 0;

  END //
    DELIMITER;

SELECT * FROM nombre n;
  
-- 4- Realizar el ejercicio anterior pero utilizando el control de errores. (Crear un función nueva)


  DELIMITER //
  CREATE OR REPLACE FUNCTION f2handler(vid int, vnombre varchar (20) )
    RETURNS int 
  BEGIN
 
DECLARE CONTINUE HANDLER FOR 1062

 IF (id=vid )THEN 
UPDATE nombre n
  SET n.nombre= vnombre
  WHERE n.id=vid ;
ELSE 
INSERT INTO nombre(id,nombre) VALUES(vid,vnombre); 
END IF;
    RETURN 0;

  END  //

    DELIMITER;

-- 5- Crear un script que añada un campo a la tabla denominado fecha (tipo DATE)

  ALTER TABLE nombre 
          ADD COLUMN fecha date;

  ALTER TABLE nombre ADD COLUMN diasemana varchar (20);

  SELECT * FROM nombre n;

  -- 6- Crear una función a la cual le pasas un fecha y te devuelve el día de la semana en castellano al que pertenece. Por
-- ejemplo si le paso 05/05/2015 me indicara que es martes. Probarla con la tabla creada. Utilizar la instrucción CASE

     DELIMITER //
CREATE OR REPLACE FUNCTION fechatexto(fecha date) 
    RETURNS varchar(20)
  BEGIN

  DECLARE textodia varchar(20);
  DECLARE dia int DEFAULT 0;

    SELECT DAYOFWEEK(fecha) INTO dia ;

     CASE 

    WHEN dia=1 THEN set textodia='Domingo';
    WHEN dia=2 THEN set textodia='Lunes';
    WHEN dia=3 THEN set textodia='Martes';
    WHEN dia=4 THEN set textodia='Miércoles';
    WHEN dia=5 THEN set textodia='Jueves';
    WHEN dia=6 THEN set textodia='Viernes';
     ELSE set textodia='Sabado';
      END CASE;

    RETURN textodia;

  END  //

    DELIMITER; 

SELECT fechatexto(fecha) FROM nombre n;

-- 7- Modificar la función anterior para que cuando el dia de la semana sea de lunes a viernes (incluidos) lance una
-- señal que me grabe en una nueva tabla denominada fechas la fecha y el texto laboral. Si el dia de la semana es
-- sábado o domingo me lance otra señal que me grabe en la tabla fechas la fecha y el texto festivo

   CREATE OR REPLACE TABLE fechas(
    fecha date,
    texto varchar(20)
    );

    DELIMITER //
CREATE OR REPLACE FUNCTION f7(fechaintroducida date) 
    RETURNS varchar(20)
  BEGIN
DECLARE dia varchar(20);
  SET dia=DAYOFWEEK(fechaintroducida);

    IF ( dia BETWEEN 2 AND 5) THEN 
     INSERT INTO fechas(fecha,texto) VALUES(fechaintroducida,'laboral');
    ELSE 
    INSERT INTO fechas(fecha,texto) VALUES(fechaintroducida,'Festivo');
END IF;
    RETURN dia;

  END  //

    DELIMITER; 
      
 SELECT * FROM fechas f;
