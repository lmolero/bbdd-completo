﻿-- 1. Llamar al procedimiento ejercicio1 y probarle con varios lados
  DROP DATABASE IF EXISTS practica1apoyo;
  CREATE DATABASE IF NOT EXISTS practica1apoyo;
  
  USE practica1apoyo;

DELIMITER //
DROP PROCEDURE IF EXISTS  practica1apoyo.ejercicio1 //
CREATE PROCEDURE practica1apoyo.ejercicio1 (
IN lado float)
BEGIN
DECLARE area float;
  DECLARE superficie float;

  SET area=POW(lado,2);
  SET superficie=lado*4;

  CREATE TABLE IF NOT EXISTS ejercicio1
    (
    numero int AUTO_INCREMENT,
    lado float,
    area float,
    superficie float,
    PRIMARY KEY (numero)
    );
  INSERT INTO ejercicio1 VALUES(DEFAULT, lado,area,superficie);
END //
DELIMITER;

CALL practica1apoyo.ejercicio1(20);
SELECT * FROM ejercicio1 e;

-- 2
  DELIMITER //
DROP PROCEDURE IF EXISTS  practica1apoyo.ejercicio2 //
CREATE PROCEDURE practica1apoyo.ejercicio2 (
IN area float)
  COMMENT 'En el procedimiento ejercicio2 quiero que lo modifiquéis para que calcule el volumen de un cubo.'
BEGIN

  DECLARE volumen float;

  SET volumen=POW(area,3);
 

  CREATE TABLE IF NOT EXISTS ejercicio2
    (
    numero int AUTO_INCREMENT,
    area float,
    voumen float,
   
    PRIMARY KEY (numero)
    );
  INSERT INTO ejercicio2 VALUES(DEFAULT, area,volumen);
END //
DELIMITER;

CALL practica1apoyo.ejercicio2(20);
SELECT * FROM ejercicio2 e;

-- 3
DELIMITER //
DROP PROCEDURE IF EXISTS  practica1apoyo.ejercicio3 //
CREATE PROCEDURE practica1apoyo.ejercicio3 (
IN superficiecara float,IN area float)
  COMMENT 'Modificar el siguiente procedimiento para que calcule la superficie total y el volumen de un cubo.
.'
BEGIN

  DECLARE superficietotal float;
  DECLARE volumen float;

  SET superficietotal=POW(superficiecara,2)*6;
  SET volumen=POW(area,3);
 
DROP TABLE IF EXISTS ejercicio3;
  CREATE TABLE IF NOT EXISTS ejercicio3
    (
    numero int AUTO_INCREMENT,
   superficiecara float,
    area float,
    voumen float,
    superficietotal float,
   
    PRIMARY KEY (numero)
    );
  INSERT INTO ejercicio3 VALUES(DEFAULT, superficiecara,area,volumen,superficietotal);
END //
DELIMITER;

CALL practica1apoyo.ejercicio3(20,8);
SELECT * FROM ejercicio3 e;

-- 4
  DELIMITER//
DROP FUNCTION IF EXISTS volumen;
  CREATE FUNCTION volumen(lado float)
 RETURNS float
    COMMENT 'Mediante la siguiente función calculamos el volumen. Llamar la función y probarla.'
    BEGIN 
DECLARE volumen float;
 SET volumen=POW(lado,3);
    RETURN volumen;

END; 
    //
DELIMITER;

SELECT volumen(5);

-- 5 

 DELIMITER//
DROP FUNCTION IF EXISTS areatotalcubo;
  CREATE FUNCTION areatotalcubo(superficiecara float)
 RETURNS float
    COMMENT 'La siguiente función debe calcular el área total de un cubo'
    BEGIN 
DECLARE areatotal float;
 SET areatotal=POW(superficiecara,2)*6;
    RETURN areatotal;

END; 
    //

DELIMITER;
SELECT areatotalcubo(5);

-- 6
  DELIMITER //
DROP PROCEDURE IF EXISTS practica1apoyo.ejercicio6;
CREATE PROCEDURE   practica1apoyo.ejercicio6(IN lado float)
  COMMENT'En el siguiente procedimiento almacenado vamos a utilizar las funciones anteriores. Llamar al procedimiento y probarlo'
BEGIN
  DECLARE volumencampo float;
  DECLARE areat float;
   SET volumencampo=volumen(lado);
   SET areat=areatotalcubo(lado);

  CREATE OR REPLACE tABLE ejercicio6 (
  id int AUTO_INCREMENT,
  ladoc float,
  volumen float,
  areatotal float,
  PRIMARY KEY(id)
  );
  INSERT INTO ejercicio6 VALUES(DEFAULT, lado, volumencampo,areat);

  END //
DELIMITER ;

CALL practica1apoyo.ejercicio6(5);
SELECT * FROM ejercicio6 e;

-- 7 

 DELIMITER//
DROP FUNCTION IF EXISTS volumenCilindro;
  CREATE FUNCTION volumencilindro(radio float, altura float)
 RETURNS float
    COMMENT 'La siguiente función calcula el volumen de un cilindro. La función recibe como argumentos el radio y la altura. Llamarla
             ejercicio7. '
    BEGIN 
DECLARE vCilindro float;
 SET  vCilindro=PI()*POW(radio,2)*(altura);

    RETURN vCilindro;

END; 
    //

DELIMITER;

SELECT volumencilindro(10,15);

-- 8 

  DELIMITER//
DROP FUNCTION IF EXISTS AlateralCilindro;
  CREATE FUNCTION AlateralCilindro(radio float, altura float)
 RETURNS float
    COMMENT 'Crear una función que calcule el área lateral de un cilindro. La función recibe como argumentos el radio y la altura.  '
    BEGIN 
DECLARE al float;
 SET  al=2*PI()*(radio)*(altura);

    RETURN al;

END; 
    //

DELIMITER;

SELECT AlateralCilindro(12,15);

-- 9

 DELIMITER//
DROP FUNCTION IF EXISTS Atotalcilindro;
  CREATE FUNCTION Atotalcilindro(radio float, altura float)
 RETURNS float
    COMMENT 'Crear una función que calcule el área total de un cilindro. La función recibe como argumentos el radio y la altura. '
    BEGIN 
DECLARE atc float;
 SET  atc=(2*PI()*(radio)*(altura))+(2*PI()*POW(radio,2));

    RETURN atc;

END; 
    //

DELIMITER;

SELECT Atotalcilindro(8,10);

-- 10

 DELIMITER //
DROP PROCEDURE IF EXISTS practica1apoyo.ejercicio10;
CREATE PROCEDURE   practica1apoyo.ejercicio10(IN radio float, IN altura float)
  COMMENT'Modificar el procedimiento siguiente para que utilice las funciones de los ejercicios anteriores para calcular el volumen,
          el área lateral y el área total de un cilindro'
BEGIN
  DECLARE vcilindro float;
  DECLARE alcilindro float;
  DECLARE atcilindro float;
   SET vcilindro=volumencilindro(radio,altura);
   SET alcilindro=alateralcilindro(radio,altura);
  SET atcilindro=Atotalcilindro(radio,altura);

  CREATE OR REPLACE tABLE ejercicio10(
  id int AUTO_INCREMENT,
  vCilindro float,
 alCilindro float,
  atCilindro float,
  PRIMARY KEY(id)
  );
  INSERT INTO ejercicio10 VALUES(DEFAULT, vcilindro, alcilindro,atcilindro);

  END //
DELIMITER ;

CALL practica1apoyo.ejercicio10(10,20);
SELECT * FROM ejercicio10 e;


 