﻿-- 1. Crear el siguiente procedimiento almacenado

  DELIMITER//
  CREATE PROCEDURE ejemploprocedimiento1
    (IN argumento1 integer)
    BEGIN 
    DECLARE variable1 char (10);
    IF (argumento1=17) THEN
    SET variable1='pajaros';
    ELSE SET variable1='leones';
    END IF;
    CREATE TABLE tabla1(
    nombre varchar(20)
    );
    INSERT INTO tabla1 VALUES (variable1);
    END
//
  DELIMITER;

CALL ejemploprocedimiento1(17);
SELECT * FROM tabla1 t;

-- ejemplo2

 DELIMITER//
  CREATE PROCEDURE ejemploprocedimiento2
    (IN argumento1 integer)
    BEGIN 
    DECLARE variable1 char (10);
    IF (argumento1=17) THEN
    SET variable1='pajaros';
    ELSE SET variable1='leones';
    END IF;
    CREATE TABLE IF NOT EXISTS tabla1(
    nombre varchar(20)
    );
    INSERT INTO tabla1 VALUES (variable1);
    END
//
  DELIMITER;

CALL ejemploprocedimiento2(10);
SELECT * FROM tabla1 t;

-- ejemplo3

 DELIMITER//
  CREATE PROCEDURE ejemplo_procedimiento3
    (IN argumento1 integer)
    BEGIN 
   SELECT 'hola mundo';

    INSERT INTO tabla1 VALUES('hola mundo');
    INSERT INTO tabla1 VALUES('hola mundo');
    INSERT INTO tabla1 VALUES('hola mundo');
    INSERT INTO tabla1 VALUES('hola mundo');
    INSERT INTO tabla1 VALUES('hola mundo');
    INSERT INTO tabla1 VALUES('hola mundo');
    INSERT INTO tabla1 VALUES('hola mundo');
    INSERT INTO tabla1 VALUES('hola mundo');
    INSERT INTO tabla1 VALUES('hola mundo');
    INSERT INTO tabla1 VALUES('hola mundo');
    END
//
  DELIMITER;

CALL ejemplo_procedimiento3('hola mundo');
SELECT * FROM tabla1 t;


-- ejemplo4
   DELIMITER//
  CREATE PROCEDURE ejemplo_procedimiento4
    (IN argumento1 integer)
    BEGIN 
  DECLARE contador int DEFAULT 0;
    REPEAT 
      SET contador=contador+1;
      INSERT INTO tabla1 VALUES('hola mundo');
      UNTIL (contador>=10)
      END REPEAT ;
    END
//
  DELIMITER;

CALL ejemplo_procedimiento4('hola mundo');
SELECT * FROM tabla1 t;


-- ejemplo 5

   DELIMITER//
DROP PROCEDURE IF EXISTS ejemplo_procedimiento5//
  CREATE PROCEDURE ejemplo_procedimiento5
  (IN nveces int)
    BEGIN 
  DECLARE contador int DEFAULT 0;
    REPEAT 
      SET contador=contador+1;
      INSERT INTO tabla1 VALUES ('hola mundo');
      UNTIL (contador>=nveces)
      END REPEAT ;
    END
//
  DELIMITER;
CALL ejemplo_procedimiento5(2);
SELECT * FROM tabla1 t;


