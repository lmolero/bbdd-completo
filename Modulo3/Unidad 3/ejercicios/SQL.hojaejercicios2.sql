﻿-- hoja de ejercicios 2

  -- Realizar un procedimiento almacenado que visualice todas las personas cuyo salario sea superior a un valor que se pasara como parámetro de entrada.

   DELIMITER//
DROP PROCEDURE IF EXISTS ejercicio2_1//
  CREATE PROCEDURE ejercicio2_1
  (IN nveces int)
    BEGIN 
  SELECT * FROM personas p
    WHERE p.salario>nveces;
    END
//
  DELIMITER;

CALL ejercicio2_1(1000);

-- - Realizar un procedimiento almacenado que visualice todas las personas cuyo salario este entre dos valores que se pasaran como parámetros de entrada.

   DELIMITER//
DROP PROCEDURE IF EXISTS ejercicio2_2//
  CREATE PROCEDURE ejercicio2_2
  (IN nvalor int, IN mvalor int)
    BEGIN 
  SELECT * FROM personas p
    WHERE p.salario BETWEEN nvalor AND mvalor;
    END
//
  DELIMITER;
CALL ejercicio2_2(1000,2000);

/* 5 Realizar un procedimiento almacenado que indique cuantos médicos trabajan en un hospital cuyo código se pase
como parámetro de entrada. Además al procedimiento se le pasara un argumento donde se debe almacenar el
número de plazas de ese hospital*/

   DELIMITER//
DROP PROCEDURE IF EXISTS ejercicio2_3//
  CREATE PROCEDURE ejercicio2_3
  (IN valor int, OUT plazas int)
    BEGIN 
  
    SELECT COUNT(*) AS n FROM medicos m WHERE m.cod_hospital=valor GROUP BY m.cod_hospital;
  
    SELECT num_plazas INTO plazas FROM hospitales WHERE cod_hospital=valor;


    END
//
  DELIMITER;


CALL ejercicio2_3(4,@s);
SELECT @s;


/*6 Crear una función que calcule el volumen de una esfera cuyo radio de tipo FLOAT se pasara como parámetro. Realizar
después una consulta para calcular el volumen de una esfera de radio 5.*/

DELIMITER//
DROP FUNCTION IF EXISTS volumen;
  CREATE FUNCTION volumen(radio float)
 RETURNS float
    BEGIN 
DECLARE v float;
 SET v=(4/3)*PI()*POW(radio,3);
    RETURN v;

END; 
    //
DELIMITER;

SELECT volumen(5);

/* 7- Crear un procedimiento almacenado que cree una tabla denominada esferas. Esta tabla tendrá dos campos de tipo
float. Estos campos son radio y volumen. El procedimiento debe crear la tabla aunque esta exista.*/


   DELIMITER//
DROP PROCEDURE IF EXISTS ejercicio2_7//
  CREATE PROCEDURE  ejercicio2_7
  ()
    BEGIN 
  DROP TABLE IF EXISTS esferas;
    CREATE TABLE IF NOT EXISTS esferas(
      id int AUTO_INCREMENT,
      radio float,
      volumen float,
      PRIMARY KEY(id)
    );
   
    END
//
  DELIMITER;

CALL ejercicio2_7();

/* 8 - Crear un procedimiento almacenado que me permita introducir 100 radios de forma automática en la tabla anterior.
Estos radios deben estar entre dos valores pasados al procedimiento como argumentos de entrada */

DELIMITER $$
DROP PROCEDURE IF EXISTS ejercicio2_8 $$
CREATE PROCEDURE ejercicio2_8(a1 int, a2 int)

BEGIN

  DECLARE contador int DEFAULT a1;
  REPEAT  
    SET contador=contador+1 ;
    INSERT INTO esferas(radio) VALUES(contador);
  UNTIL contador>=a2
  END REPEAT;
  

END
$$

DELIMITER ;

CALL ejercicio2_8(1000,1100);
SELECT * FROM esferas e;


/*9- Crear un procedimiento almacenado que me permita calcular los volúmenes de todos los registros de la tabla esferas
creada anteriormente. Debéis utilizar la función creada para este propósito  */


DELIMITER $$
DROP PROCEDURE IF EXISTS ejercicio2_9 $$
CREATE PROCEDURE ejercicio2_9()

BEGIN
  UPDATE esferas e SET e.volumen=volumen(e.radio);

END
$$

DELIMITER ;
CALL ejercicio2_9();

SELECT * FROM esferas;



/*  10- Crear un procedimiento que reciba una palabra como argumento y la devuelva en ese mismo argumento escrita al
revés.   */

    DELIMITER//
DROP PROCEDURE IF EXISTS ejercicio2_10//
  CREATE PROCEDURE ejercicio2_10
  (IN texto varchar)
    BEGIN 
DECLARE cadena varchar;
  DECLARE caracter varchar;
  DECLARE i int ;
    DECLARE longitud IN DEFAULT CHAR_LENGTH(texto);
    SET texto=SUBSTRING(cadena,longitud,1);
    WHILE i<=longitud DO 
      SET caracter=SUBSTRING(cadena,longitud,1);
      SET cadena=CONCAT(cadena,caracter);
      set i=i+1;
      END WHILE;

SET texto=cadena
    END
//
  DELIMITER;
set @cad='martini';
CALL ejercicio2_10('martini');
SELECT @cad;

/* 12- Realizar una función que calcule el factorial de un número. */