﻿DROP DATABASE IF EXISTS agenciaseguros;

CREATE DATABASE IF NOT EXISTS Agenciaseguros;

USE Agenciaseguros;

DROP TABLE IF EXISTS clientes;
 CREATE TABLE  IF NOT EXISTS clientes(
dni char(9),
nombre varchar (15),
direccion varchar (15),
telefono varchar (15),
 PRIMARY KEY (dni)
    );

DROP TABLE IF EXISTS agentes;
 CREATE TABLE  IF NOT EXISTS agentes(
dni char(9),
nombre varchar (15),
direccion varchar (15),
telefono varchar (15),
 PRIMARY KEY (dni)
    );


DROP TABLE IF EXISTS seguros;
 CREATE TABLE  IF NOT EXISTS seguros(
fechadecontrato date,
codtiposeguro varchar (15),
 PRIMARY KEY (codtiposeguro),
UNIQUE KEY  (fechadecontrato)
);

DROP TABLE IF EXISTS segurovida;
 CREATE TABLE  IF NOT EXISTS segurovida(
codsegurov varchar(15),
fechadecontrato date,
dniagente char(9),
dnicliente char(9),
beneficiario varchar(15),
 PRIMARY KEY (codsegurov, fechadecontrato,dniagente,dnicliente),
  CONSTRAINT FKsegurosegurovida FOREIGN KEY (codsegurov)
    REFERENCES seguros(codtiposeguro)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKsegurossegurovida FOREIGN KEY (fechadecontrato)
    REFERENCES seguros(fechadecontrato) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT FKagentesegurovida FOREIGN KEY (dniagente)
    REFERENCES agentes(dni)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKclientessegurovida FOREIGN KEY (dnicliente)
    REFERENCES clientes(dni) ON DELETE CASCADE ON UPDATE CASCADE
    );

DROP TABLE IF EXISTS segurohogar;
 CREATE TABLE  IF NOT EXISTS segurohogar(
codseguroh varchar(15),
fechadecontrato date,
dniagente char(9),
dnicliente char(9),
direccion varchar(15),
 PRIMARY KEY (codseguroh, fechadecontrato,dniagente,dnicliente),
  CONSTRAINT FKsegurosegurohogar FOREIGN KEY (codseguroh)
    REFERENCES seguros(codtiposeguro)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKsegurossegurohogar FOREIGN KEY (fechadecontrato)
    REFERENCES seguros(fechadecontrato) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT FKagentesegurohogar FOREIGN KEY (dniagente)
    REFERENCES agentes(dni)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKclientessegurohogar FOREIGN KEY (dnicliente)
    REFERENCES clientes(dni) ON DELETE CASCADE ON UPDATE CASCADE
    );

DROP TABLE IF EXISTS seguroautomovil;
 CREATE TABLE  IF NOT EXISTS  seguroautomovil(
codseguroa varchar(15),
fechadecontrato date,
  matriculas varchar(15),
  recargos varchar(15),
  descuento varchar(15),
dniagente char(9),
dnicliente char(9),
direccion varchar(15),
 PRIMARY KEY (codseguroa, fechadecontrato,dniagente,dnicliente, matriculas),
  CONSTRAINT FKsegurosegurovehiculo FOREIGN KEY (codseguroa)
    REFERENCES seguros(codtiposeguro)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKsegurossegurovehiculo FOREIGN KEY (fechadecontrato)
    REFERENCES seguros(fechadecontrato) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT FKagentesegurovehiculo FOREIGN KEY (dniagente)
    REFERENCES agentes(dni)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKclientessegurovehiculo FOREIGN KEY (dnicliente)
    REFERENCES clientes(dni) ON DELETE CASCADE ON UPDATE CASCADE
    );

DROP TABLE IF EXISTS contratan;
 CREATE TABLE  IF NOT EXISTS  contratan(
codtiposeguro varchar (15),
dnicliente char(9),
PRIMARY KEY (codtiposeguro,dnicliente),
CONSTRAINT FKcontratanseguro FOREIGN KEY (codtiposeguro)
    REFERENCES seguros(codtiposeguro)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKcontratancliente FOREIGN KEY (dnicliente)
    REFERENCES clientes(dni) ON DELETE CASCADE ON UPDATE CASCADE
    );

DROP TABLE IF EXISTS venden;
 CREATE TABLE  IF NOT EXISTS  venden(
codtiposeguro varchar (15),
dniagente char(9),
  fecha date,
PRIMARY KEY (codtiposeguro,dniagente),
CONSTRAINT FKvendenseguros FOREIGN KEY (codtiposeguro)
    REFERENCES seguros(codtiposeguro)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKvendenagentes FOREIGN KEY (dniagente)
    REFERENCES agentes(dni) ON DELETE CASCADE ON UPDATE CASCADE
    );