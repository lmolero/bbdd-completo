﻿-- Hoja 7- unidad 1- modulo 3- ejercicio 3
  DROP DATABASE IF EXISTS turistagencia;

CREATE DATABASE IF NOT EXISTS turistagencia;

USE turistagencia;

DROP TABLE IF EXISTS turista;
CREATE TABLE IF NOT EXISTS turista(
Nturista varchar(15),
  Nombre varchar(15),
  apellidos varchar(15),
  telefono numeric,
  PRIMARY KEY (Nturista)
);


DROP TABLE IF EXISTS vuelo;
CREATE TABLE IF NOT EXISTS vuelo(
Nvuelo varchar(15),
  nºturista varchar(15),
  nºtotal varchar(15),
  destino varchar(15),
  origen varchar(15),
  hora time,
  fecha date,
  PRIMARY KEY (Nvuelo)
);


DROP TABLE IF EXISTS toma;

CREATE TABLE IF NOT EXISTS toma(
Nturista varchar(15),
nvuelo varchar(15),
 clase varchar(15),
  PRIMARY KEY (Nturista,nvuelo),
  CONSTRAINT fktomaturista FOREIGN KEY(Nturista)
    REFERENCES turista(Nturista)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fktomavuelo FOREIGN KEY (nvuelo)
    REFERENCES vuelo(nvuelo)  ON DELETE CASCADE ON UPDATE CASCADE
    );


DROP TABLE IF EXISTS hotel;
CREATE TABLE IF NOT EXISTS hotel(
codhotel varchar(15),
  Nombre varchar(15),
  direccion varchar(15),
  ciudad varchar(15),
  telefono numeric,
  plazas varchar(15),
  PRIMARY KEY (codhotel)
);

DROP TABLE IF EXISTS reserva;
CREATE TABLE IF NOT EXISTS reserva(
  nºturista varchar(15),
  nºhotel varchar(15),
 fecha_e date,
  fecha_s date,
  pension varchar(15),
  PRIMARY KEY (nºturista,nºhotel),
CONSTRAINT fkreservaturista FOREIGN KEY(nºturista)
    REFERENCES turista(Nturista)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkreservahotel FOREIGN KEY (nºhotel)
    REFERENCES hotel(codhotel)  ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS agencia;
CREATE TABLE IF NOT EXISTS agencia(
codagencia varchar(15),
 direccion varchar(15),
  telefono numeric,
  PRIMARY KEY (codagencia)
);

DROP TABLE IF EXISTS contrata;
CREATE TABLE IF NOT EXISTS contrata(
  nºturista varchar(15),
  nºagencia varchar(15),
 direccion varchar(15),
 telefono numeric,
  PRIMARY KEY (nºturista,nºagencia),
CONSTRAINT fkcontrataturista FOREIGN KEY(nºturista)
    REFERENCES turista(Nturista)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkcontratagencia FOREIGN KEY (nºagencia)
    REFERENCES agencia(codagencia)  ON DELETE CASCADE ON UPDATE CASCADE
  );