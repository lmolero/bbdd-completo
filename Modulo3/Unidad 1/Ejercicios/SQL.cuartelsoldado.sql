﻿-- Hoja 7- unidad 1- modulo 3- ejercicio 2

  DROP DATABASE IF EXISTS cuartelsoldados;

CREATE DATABASE IF NOT EXISTS cuartelsoldados;

USE cuartelsoldados;

DROP TABLE IF EXISTS compañia;
 CREATE TABLE  IF NOT EXISTS compañia(
    numerocompañia char(9),
  actividad varchar(15),
    PRIMARY KEY (numerocompañia)
    );

  INSERT INTO compañia VALUES 
    ('numerocompañia1'),
    ('numerocompañia2');

  DROP TABLE IF EXISTS soldado;

  CREATE TABLE IF NOT EXISTS soldado(
    codSoldado varchar(15),
    nombre varchar(15),
    apellidos varchar(15),
    grado varchar(15),
    PRIMARY KEY (codSoldado)
  );

  DROP TABLE IF EXISTS pertenece;
  CREATE TABLE IF NOT EXISTS pertenece(
    compañia char(9),
    soldado varchar(15),
    PRIMARY KEY (compañia,soldado),
  CONSTRAINT fkpertenececompañia FOREIGN KEY (compañia)
    REFERENCES compañia(numerocompañia)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkpertenecesoldado FOREIGN KEY (soldado)
    REFERENCES soldado(codSoldado)  ON DELETE CASCADE ON UPDATE CASCADE
    );

  DROP TABLE IF EXISTS servicio;

  CREATE TABLE if NOT EXISTS servicio(
    codservicio varchar(15),
    descripcion varchar(15),
    PRIMARY KEY (codservicio)
    );

  DROP TABLE IF EXISTS realiza;
  CREATE TABLE IF NOT EXISTS realiza(
  soldado varchar(15),
    servicio varchar(15),
    fecha date,
    PRIMARY KEY(soldado,servicio),
    CONSTRAINT fkrealizasoldado FOREIGN KEY (soldado)
    REFERENCES soldado(codSoldado) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkrealizaservicio FOREIGN KEY (servicio) 
 REFERENCES servicio(codservicio) ON DELETE CASCADE ON UPDATE CASCADE
  );

  DROP TABLE IF EXISTS cuartel;
  CREATE TABLE IF NOT EXISTS cuartel(
  codcuartel varchar(15),
    nombre varchar(15),
    direccion varchar(15),
    PRIMARY KEY(codcuartel)
  );

  DROP TABLE IF EXISTS esta;
  CREATE TABLE IF NOT EXISTS esta(
  soldado varchar(15),
    cuartel varchar(15),
    PRIMARY KEY(soldado,cuartel),
    CONSTRAINT fkestasoldado FOREIGN KEY (soldado)
    REFERENCES soldado(codSoldado) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkestacuartel FOREIGN KEY (cuartel) 
 REFERENCES cuartel(codcuartel) ON DELETE CASCADE ON UPDATE CASCADE
  );


 DROP TABLE IF EXISTS cuerpo;
  CREATE TABLE IF NOT EXISTS cuerpo(
  codcuerpo varchar(15),
   denominacion varchar(15),
    PRIMARY KEY(codcuerpo)
  );

    DROP TABLE IF EXISTS pertenececuerpo;
  CREATE TABLE IF NOT EXISTS pertenececuerpo(
  soldado varchar(15),
   cuerpo varchar(15),
    PRIMARY KEY(soldado,cuerpo),
    CONSTRAINT fkpertenececsoldado FOREIGN KEY (soldado)
    REFERENCES soldado(codSoldado) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkperteneccuerpo FOREIGN KEY (cuerpo) 
 REFERENCES cuerpo(codcuerpo) ON DELETE CASCADE ON UPDATE CASCADE
  );

