﻿DROP DATABASE IF EXISTS practica3yii;
CREATE DATABASE IF NOT EXISTS practica3yii;

USE practica3yii;

DROP TABLE IF EXISTS delegacion;
CREATE TABLE IF NOT EXISTS delegacion(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(25),
  poblacion varchar(25),
  direccion varchar(25)
  );

DROP TABLE IF EXISTS trabajadores;
CREATE TABLE IF NOT EXISTS trabajadores(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(25),
  apellidos varchar(25),
  fechanacimiento date,
  foto varchar(20),
 delegacion int,
 
CONSTRAINT FKtrabajadoresdelegacion FOREIGN KEY (delegacion)
REFERENCES delegacion (id) ON DELETE CASCADE ON UPDATE CASCADE

  );

