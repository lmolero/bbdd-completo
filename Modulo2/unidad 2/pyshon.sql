﻿SELECT * FROM aemet;

ALTER TABLE aemet ADD f_prediccion date;

UPDATE aemet SET f_prediccion=date(insercion);

ALTER TABLE aemet DROP index fecha;
ALTER TABLE aemet ADD UNIQUE KEY (fecha,f_prediccion);


DELETE FROM aemet WHERE date(insercion)=date(NOW());


SELECT * FROM aemet;
UPDATE aemet SET f_prediccion=date(insercion);

SELECT MIN(lluvia) FROM aemet
WHERE date(insercion)=date(NOW());

SELECT  MAX(date(insercion)) FROM aemet;

SELECT MIN(lluvia)FROM aemet
  WHERE date(insercion)=(SELECT  MAX(date(insercion)) FROM aemet);

SELECT fecha, lluvia FROM aemet
WHERE lluvia=(
  SELECT MIN(lluvia)FROM aemet
  WHERE date(insercion)=(SELECT  MAX(date(insercion)) FROM aemet)) 
  AND date(insercion)=(SELECT MAX(date(insercion)) FROM aemet);

ALTER TABLE aemet ADD COLUMN periodo varchar(15);
SELECT * FROM aemet a;


EXPLAIN aemet;
-- obtener el ultimo dia que he metido datos

SELECT MAX(f_prediccion) FROM aemet a;

-- seleccionar las predicciones del ultimo dia

SELECT * FROM aemet a
WHERE date(a.insercion)=(SELECT MAX(f_prediccion) FROM aemet a);

SELECT * FROM aemet a
 WHERE DATE(a.insercion)=(SELECT MAX(DATE(insercion)) FROM aemet a1) AND a.periodo!='00-06';

SELECT MIN(a.lluvia) FROM aemet a
WHERE DATE(a.insercion)=(SELECT MAX(date(insercion)) FROM aemet a1 ) 
AND a.periodo!='00-06'
 AND a.lluvia=(SELECT MIN(a1.lluvia) FROM aemet a1 WHERE DATE(a1.insercion)= SELECT MAX(date(insercion)) FROM aemet);









