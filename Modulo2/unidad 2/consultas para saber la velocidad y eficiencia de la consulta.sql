﻿
SELECT * FROM provincias p;

EXPLAIN provincias;

SELECT MAX(p.poblacion) FROM provincias p;

EXPLAIN
  SELECT * FROM provincias p
  WHERE p.poblacion=(SELECT MAX(p.poblacion) FROM provincias p);

EXPLAIN
SELECT * FROM provincias p ORDER BY p.poblacion DESC LIMIT 1;

ALTER TABLE provincias ADD  INDEX(poblacion);

SELECT BENCHMARK(2,SLEEP(2));

DELIMITER //
DROP FUNCTION madrid;
CREATE FUNCTION madrid()
  RETURNS varchar (31) 
  BEGIN
RETURN 
  (SELECT p.provincia FROM provincias p
  WHERE p.poblacion=(SELECT MAX(p.poblacion) FROM provincias p));
  END
  //
DELIMITER;

SELECT madrid();
SELECT BENCHMARK(1e4,madrid());


DELIMITER //
CREATE FUNCTION madrid2()
  RETURNS varchar (31) 
  BEGIN
RETURN 
  (SELECT p.provincia FROM provincias p ORDER BY p.poblacion DESC LIMIT 1);
  END
  //
DELIMITER;


SELECT madrid2();
SELECT BENCHMARK(1e4,madrid2());

SELECT * FROM information_schema.TABLES t
WHERE t.TABLE_SCHEMA='nba';


 USE nba;
SELECT * FROM partidos;
EXPLAIN partidos;

-- partido en el que el equipo local ha marcado mas puntos
  SELECT DISTINCT codigo ,MAX( puntos_local) FROM partidos
  GROUP BY codigo;


 
  SELECT MIN(puntos_local)FROM partidos;

   EXPLAIN
  SELECT * FROM partidos WHERE puntos_local=( SELECT MIN(puntos_local)FROM partidos)
  AND puntos_visitante=(SELECT MIN(puntos_visitante)FROM partidos) AND temporada='06/07';

EXPLAIN
  SELECT * FROM partidos WHERE temporada='06/07'
    ORDER BY puntos_local,puntos_visitante
    LIMIT 1;

-- con where
DELIMITER //
DROP FUNCTION nba1;
CREATE FUNCTION nba1()
  RETURNS varchar(30)
  BEGIN 
RETURN ( SELECT codigo FROM partidos WHERE puntos_local=( SELECT MIN(puntos_local)FROM partidos)
  AND puntos_visitante=(SELECT MIN(puntos_visitante)FROM partidos) AND temporada='06/07');

  END 
  // 
DELIMITER;

SELECT nba1();

SELECT BENCHMARK(1e2,nba1());

-- con limit 
DELIMITER //
-- DROP FUNCTION nba2;
CREATE FUNCTION nba2()
  RETURNS varchar(30)
  BEGIN 
RETURN (SELECT codigo FROM partidos WHERE temporada='06/07'
    ORDER BY puntos_local,puntos_visitante
    LIMIT 1);

  END 
  // 
DELIMITER;

SELECT nba2();

SELECT BENCHMARK(1e2,nba2());

SELECT MAX(puntos_local) FROM partidos;

SELECT puntos_local FROM partidos ORDER BY  puntos_local DESC LIMIT 1;

CREATE FUNCTION nba3()
  RETURNS int
  BEGIN 
  RETURN
  (SELECT puntos_local FROM partidos ORDER BY  puntos_local DESC LIMIT 1);

  END ;

   SELECT nba3();
SELECT BENCHMARK(1e3,nba3());


CREATE FUNCTION nba4()
  RETURNS int
  BEGIN 
  RETURN
  (SELECT MAX(puntos_local) AS maximo  FROM partidos);

  END ;

  SELECT nba4();
SELECT BENCHMARK(1e3,nba4());

-- codigo de partidos jugados en NY
explain
  SELECT codigo FROM partidos
  JOIN  equipos ON equipo_local=nombre    WHERE ciudad='New york';


  explain
 SELECT codigo FROM partidos
  JOIN  (SELECT nombre FROM equipos WHERE ciudad='new york') equipos ON equipo_local=nombre;

  DELIMITER //
  DROP FUNCTION IF EXISTS nba5;
  CREATE FUNCTION nba5()
    RETURNS int
    BEGIN 
RETURN (SELECT COUNT(*) FROM partidos
  JOIN  equipos ON equipo_local=nombre    WHERE ciudad='New york');

    END
    //
  DELIMITER;

DELIMITER //
 DROP FUNCTION IF EXISTS nba6;
 CREATE FUNCTION nba6()
    RETURNS int
    BEGIN 
RETURN ( SELECT COUNT(*) FROM  FROM partidos
  JOIN  (SELECT nombre FROM equipos WHERE ciudad='new york') equipos ON equipo_local=nombre);

    END;
   //
  DELIMITER;

