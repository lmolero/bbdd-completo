﻿SELECT CURRENT_USER();


SELECT * FROM mysql.user u;
DROP USER alpe;

CREATE USER 'alpe'@'%' IDENTIFIED BY 'hola';

/*GRANT ALL ON alpe_db2018.* TO alpe;*/ -- este comando es para dar acceso

REVOKE ALL ON *.* from alpe; -- este comando es para restringir acceso
FLUSH PRIVILEGES;-- esto es para que accione el comando grant o revoke

DROP USER alpe;

SHOW TABLES;
SELECT CURRENT_USER();

SELECT * FROM information_schema.USER_PRIVILEGES up;

SELECT * FROM information_schema.SCHEMA_PRIVILEGES;

CREATE  DATABASE alpe_db2018;
USE alpe_db2018;

CREATE TABLE visitas(
nombre varchar(31)
  );

SELECT * FROM visitas v;

-- para usar otra base de datos de un compañero, hago una nueva conexion con usuario y contraseña del compañro y su ip.
  -- luego inserto los datos en su tabla

-- visitando a Imanol

  INSERT INTO visitas VALUE('Lisany');

  SELECT * FROM visitas v;

 -- visitando a pablo

 INSERT INTO visitas VALUE('Lisany');

  SELECT * FROM visitas v;

-- visitando a kevin

   INSERT INTO visitas VALUE('Lisany');

  SELECT * FROM visitas v;

  -- visitando a luis

     INSERT INTO visitas VALUE('Lisany');

  SELECT * FROM visitas v;


 -- visitando a sergio

     INSERT INTO visitas VALUE('Lisany');

  SELECT * FROM visitas v;

SELECT * FROM mysql.user; --  para ver acda usuario

SELECT * FROM information_schema.USER_PRIVILEGES up; -- para ver los privilegios que tiene cada usuario

