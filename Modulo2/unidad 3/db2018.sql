﻿DROP DATABASE IF EXISTS  db2018;
CREATE DATABASE db2018;

USE db2018;

CREATE OR REPLACE TABLE datos(
  id_dato int AUTO_INCREMENT,
  datos varchar (127),
  PRIMARY KEY (id_dato)
  );

SELECT * FROM datos d;

INSERT INTO datos (datos)
  VALUES(NOW()); 

ALTER TABLE datos ADD COLUMN ip varchar(15);

SELECT d.ip ,COUNT(DISTINCT ip) FROM datos d
GROUP BY d.ip;


CREATE OR REPLACE TABLE usuarios(
  id_usuario int AUTO_INCREMENT,
  ip varchar (15),
  nombre varchar(31),
  PRIMARY KEY (id_usuario)
  );

SELECT * FROM usuarios u;

ALTER TABLE datos ADD INDEX (ip);


--  ip que más me ha visitado diferente a la mia

SELECT * FROM (
    SELECT ip,COUNT(*) n FROM datos
      WHERE ip IS NOT NULL
      AND ip<>'172.31.128.110'
      GROUP BY 1
     HAVING COUNT(*)=(
        SELECT MAX(n) FROM (
           SELECT ip,COUNT(*) n FROM datos
            WHERE ip IS NOT NULL
            AND ip<>'172.31.128.110'
          GROUP BY 1
        ) c1
    )  
  ) c2 JOIN usuarios USING(ip);

SELECT * FROM (
    SELECT ip,COUNT(*) n FROM datos
      WHERE ip IS NOT NULL
      AND ip NOT IN (
        SELECT ip FROM usuarios
          WHERE nombre='Lisany'
      )
      GROUP BY 1
     HAVING COUNT(*)=(
        SELECT MAX(n) FROM (
           SELECT ip,COUNT(*) n FROM datos
            WHERE ip IS NOT NULL
            AND ip NOT IN (
              SELECT ip FROM usuarios
                WHERE nombre='Lisany'
            )
          GROUP BY 1
        ) c1
    )  
  ) c2 JOIN usuarios USING(ip);

ALTER TABLE usuarios ADD COLUMN email varchar(127);

SELECT * FROM usuarios u;

-- introducir el email que registren los usuarios segun su ip


    UPDATE usuarios u
    SET u.email='lisanymoleromachado@gmail.com'
    WHERE u.ip='172.31.128.110';

    SELECT * FROM usuarios u;

    ALTER TABLE datos CHANGE datos datos  datetime;


-- las personas que me han visitado en los ultimos 3 minutos
SELECT DISTINCT  ip FROM datos d
WHERE d.datos>NOW()-INTERVAL 3 MINUTE;

SELECT * FROM usuarios u
  WHERE ip NOT IN (SELECT DISTINCT  ip FROM datos d
WHERE d.datos>NOW()-INTERVAL 3 MINUTE);

ALTER TABLE usuarios ADD ip_ok bool;

SELECT COUNT(*) FROM usuarios WHERE ip_ok IS NOT NULL;

-- creando usuario y contraseña

SELECT * FROM mysql.user u;

CREATE USER 'alpe'@'%' IDENTIFIED BY 'hola';

-- establecemos nueva conexion con la ip de david

SELECT CURRENT_USER();

SHOW TABLES;
SELECT CURRENT_USER();

SELECT * FROM information_schema.USER_PRIVILEGES up;

SELECT * FROM information_schema.SCHEMA_PRIVILEGES;

USE alpe_db2018;

INSERT INTO visitas(nombre) VALUES('Lisany');





