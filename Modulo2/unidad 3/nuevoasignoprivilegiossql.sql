﻿SELECT CURRENT_USER;

SELECT * FROM mysql.user;

CREATE USER 'alpe'@'%' IDENTIFIED BY 'hola';
DROP USER alpe;
DROP USER ''@'localhost';

SELECT * FROM information_schema.USER_PRIVILEGES up
WHERE up.GRANTEE LIKE '%alpe%';

SELECT * FROM information_schema.SCHEMA_PRIVILEGES sp
  WHERE sp.GRANTEE LIKE '%alpe%';

SELECT * FROM information_schema.TABLE_PRIVILEGES tp;

SELECT * FROM information_schema.COLUMN_PRIVILEGES cp;

GRANT INSERT ON alpe_db2018.* TO alpe@`%`;
FLUSH PRIVILEGES;
GRANT SELECT ON alpe_db2018.* TO alpe@`%`;


ALTER TABLE visitas ADD COLUMN ip int;

GRANT UPDATE(ip)ON alpe_db2018.visitas TO 'alpe'@'%';
FLUSH PRIVILEGES;

GRANT SELECT,UPDATE,DELETE ON alpe_db2018.visitas TO alpe@`%`;
FLUSH PRIVILEGES;

SELECT * FROM information_schema.USER_PRIVILEGES WHERE GRANTEE LIKE '%alpe%';
SELECT * FROM information_schema.SCHEMA_PRIVILEGES WHERE GRANTEE LIKE '%alpe%';
SELECT * FROM information_schema.TABLE_PRIVILEGES tp WHERE GRANTEE LIKE '%alpe%';
SELECT * FROM information_schema.COLUMN_PRIVILEGES cp WHERE GRANTEE LIKE '%alpe%';


GRANT ALL ON alpe_db2018.* TO alpe;
FLUSH PRIVILEGES;
REVOKE INSERT ON alpe_db218.* FROM 'alpe'@'%';


DROP USER alpe;
CREATE USER alpe@'%' IDENTIFIED BY 'hola';
GRANT ALL ON alpe_db2018.* TO alpe;
REVOKE INSERT ON alpe_db2018.* FROM alpe;